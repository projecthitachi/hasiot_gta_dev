﻿using Kendo.Mvc;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace has.iot.Components
{
    public class Constant
    {
        public static MqttClient MqttClient = null;
        public static MongoClient client = new MongoClient(WebConfigurationManager.AppSettings["mongodb_server"].ToString());
        public static string ExportDirectory = "/Components/Export/AirCond/";
        public static FilterDescriptor KendoChangeComposite(IEnumerable<IFilterDescriptor> filters)
        {
            FilterDescriptor filt = new FilterDescriptor();
            foreach (var filter in filters)
            {
                if (filter is CompositeFilterDescriptor)
                    KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                else
                    filt = ((FilterDescriptor)filter);
            }
            return filt;
        }
        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text  
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it  
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
    }
}