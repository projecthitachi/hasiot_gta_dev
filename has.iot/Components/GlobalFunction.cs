﻿using has.iot.Models;
using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.AspNet.SignalR;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using has.iot.Components;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MongoDB.Driver;
using Kendo.Mvc;
using System.Web.Routing;
using MongoDB.Bson;

namespace has.iot.Components
{
    public class IsLoggedInAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string controllerName = ((ControllerBase)filterContext.Controller).ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = ((ControllerBase)filterContext.Controller).ControllerContext.RouteData.Values["action"].ToString();

            if ((session != null && session["userdata"] == null) && (controllerName != "Systems" && actionName != "Login"))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Systems",
                    action = "Login",
                    area = ""
                }));
            }
            base.OnActionExecuting(filterContext);
        }
    }

    public class GlobalFunction
    {
        public static class Constants
        {
            //public const string DateFormat = "MM/dd/yyyy";
            //public const string DateFormatGrid = "{0:MM/dd/yyyy}";
            public const string DateFormat = "MM-dd-yyyy";
            public const string DateFormatGrid = "{0:MM-dd-yyyy}";
            public static string Admin = "Administrator";
        }

        public static void TrimNull<T>(T poRecord)
        {
            var stringProperties = poRecord.GetType().GetProperties().Where(p => p.PropertyType == typeof(string));
            foreach (var stringProperty in stringProperties)
            {
                string currentValue = (string)stringProperty.GetValue(poRecord, null);
                if (currentValue == null)
                {
                    currentValue = "";
                }
                stringProperty.SetValue(poRecord, currentValue.Trim(), null);
            }
        }
        
        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static string mqttServer = null;
        public static MqttClient MqttClient = null;
        public static void MqttListener()
        {
            mqttServer = WebConfigurationManager.AppSettings["server_mqtt"].ToString();
            MqttClient = new MqttClient(mqttServer);
            //MqttClient.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            MqttClient.Connect(clientId);
            
            //MqttClient.Subscribe(new string[] { "#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });

            //void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
            //{
            //    var message = System.Text.Encoding.Default.GetString(e.Message);
            //    if (IsValidJson(message))
            //    {
            //        string[] topic = e.Topic.Split('/');
            //        string hostDevice = topic[0];
            //        hostDevice = hostDevice.Substring(0, hostDevice.Length - 2);
            //        // scan assembly
            //        ICollection<IPlugin> plugins = GenericPluginLoader<IPlugin>.LoadPlugins("\\Components\\Plugins\\pools");
            //        foreach (var item in plugins)
            //        {
            //            if (!GlobalFunction._Plugins.ContainsKey(item.Name.Replace(" ", String.Empty)))
            //            {
            //                GlobalFunction._Plugins.Add(item.Name.Replace(" ", String.Empty), item);
            //            }
            //        }
            //        if (GlobalFunction._Plugins.ContainsKey(hostDevice + "Contract"))
            //        {
            //            GlobalFunction._Plugins[hostDevice + "Contract"].Get(e);
            //        }
            //    }
            //}
        }    
        public static long GeneralTablesGetIDByType(string text, string type)
        {
            Models.GeneralTablesQuery oQuery = new Models.GeneralTablesQuery();
            Models.GeneralTables oData = oQuery.GeneralTablesGetIDByType(text, type).SingleOrDefault();
            if (oData != null)
                return oData.RecordID;
            else
                return 0;
        }
        public static FilterDescriptor KendoChangeComposite(IEnumerable<IFilterDescriptor> filters)
        {
            FilterDescriptor filt = new FilterDescriptor();
            foreach (var filter in filters)
            {
                if (filter is CompositeFilterDescriptor)
                    KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                else
                    filt = ((FilterDescriptor)filter);
            }
            return filt;
        }

        public static UsersData GetLoginData(string userdata)
        {
            var database = Constant.client.GetDatabase("IoT");
            var Collectusers = database.GetCollection<UsersView_REC>("Users");
            var collection = database.GetCollection<Persons>("Person");

            JObject obj = JObject.Parse(userdata);
            ObjectId _id = ObjectId.Parse(obj["_id"].ToString());
            string uuid = obj["uuid"].ToString();

            UsersView_REC res = Collectusers.Find(x => x._id == _id).SingleOrDefault();
            Persons resPerson = collection.Find(x => x.personID == uuid).SingleOrDefault();

            string resJson = JsonConvert.SerializeObject(res);
            obj = JObject.Parse(resJson);
            obj.Property("_id").Remove();

            string objString = JsonConvert.SerializeObject(obj);


            UsersData voData = JsonConvert.DeserializeObject<UsersData>(objString);
            voData._id = _id;
            voData.person = resPerson;


            return voData;
        }
    }
}