﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace has.iot.Components
{
    public class RealTimeHub : Hub
    {
        public void Send(string name, string massage, string type)
        {
            switch (type)
            {
                case "Dashboard":
                    Clients.All.Dashboard(name, massage);
                    break;
                case "Device":
                    Clients.All.Device(name, massage);
                    break;
                case "ButtonSwitch":
                    Clients.All.ButtonSwitch(name, massage);
                    break;
                case "IotCategory":
                    Clients.All.IotCategory(name, massage);
                    break;
                case "RFIDImpinj":
                    Clients.All.RFIDImpinj(name, massage);
                    break;
                default:
                    Clients.All.broadcastMessage(name, massage);
                    break;
            }
        }
    }
}