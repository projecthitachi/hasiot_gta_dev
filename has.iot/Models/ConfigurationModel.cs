﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class ConfigurationModel
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public decimal OutDoorTemperature { get; set; }

        public bool OutDoorTemperatureStatus { get; set; }

        public decimal ConstantLux { get; set; }

        public bool ConstantLuxStatus { get; set; }

        public decimal SkinTemperature { get; set; }

        public bool SkinTemperatureStatus { get; set; }

        public decimal HeartRate { get; set; }

        public bool HeartRateStatus { get; set; }

    }
}