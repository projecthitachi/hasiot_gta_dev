﻿using has.iot.Components;
using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Models
{
    public class Location_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string LocationID { get; set; }
        public string LocationName { get; set; }

        public long ParentMenuID { get; set; }
        public string LocationType { get; set; }
        public string CameraPosition { get; set; }
        public string ControlTarget { get; set; }
        public string ObjectName { get; set; }

    }

    public class LocationDetail_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public long ParentID { get; set; }
        public string ReaderID { get; set; }
        public int AntennaNo { get; set; }
        public decimal MaxRSSI { get; set; }

        public string LocationID { get; set; }
        public string LocationName { get; set; }
        public string CameraPosition { get; set; }
        public string ControlTarget { get; set; }
        public string ObjectName { get; set; }

    }

    public class LocationModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public LocationModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t8030fieldselect"
        private string t8030fieldselect = @"
            t8030r001 as RecordID,
            t8030r002 as RecordTimestamp,
            t8030r003 as RecordStatus,
            t8030f001 as LocationID,
            t8030f002 as LocationName,
            t8030f003 as ParentMenuID,
            t8030f004 as LocationType,
            t8030f005 as CameraPosition,
            t8030f006 as ControlTarget,
            t8030f007 as ObjectName
        ";
        #endregion
        #region "t8031fieldselect"
        private string t8031fieldselect = @"
            t8031r001 as RecordID,
            t8031r002 as RecordTimestamp,
            t8031r003 as RecordStatus,
            t8031f001 as ParentID,
            t8031f002 as ReaderID,
            t8031f003 as AntennaNo,
            t8031f004 as MaxRSSI
        ";
        #endregion
        public DbRawSqlQuery<Location_REC> GetList(DatabaseContext oRemoteDB)
        {
            string sSQL = "select " + t8030fieldselect + " from t8030";
            var vQuery = oRemoteDB.Database.SqlQuery<Location_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Location_REC> GetLocationByFloor(DatabaseContext oRemoteDB)
        {
            string sSQL = "select " + t8030fieldselect + " from t8030 where t8030f004 = 'FLOOR'";
            var vQuery = oRemoteDB.Database.SqlQuery<Location_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Location_REC> GetListOrderByType(DatabaseContext oRemoteDB)
        {
            string sSQL = "select "+ t8030fieldselect + " from t8030 order by t8030f004, t8030f001 asc";
            var vQuery = oRemoteDB.Database.SqlQuery<Location_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<LocationDetail_REC> GetListDetail(DatabaseContext oRemoteDB, long ParentRecordID)
        {
            string sSQL = "select " + t8031fieldselect + ", t8030f001 as LocationID, t8030f002 as LocationName from t8031 LEFT JOIN t8030 on t8030r001 = t8031f001 where t8031f001 = " + ParentRecordID;
            var vQuery = oRemoteDB.Database.SqlQuery<LocationDetail_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<LocationDetail_REC> GetDetailByReaderAntenna(string sReaderID, string sAntennaID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "select "+ t8031fieldselect + ", t8030f001 as LocationID, t8030f002 as LocationName from t8031 left join t8030 on t8030r001 = t8031f001 where t8031f002 = '" + sReaderID+"' AND t8031f003 = "+sAntennaID;
            var vQuery = oRemoteDB.Database.SqlQuery<LocationDetail_REC>(sSQL);
            return vQuery;
        }
        public bool Insert(DatabaseContext oRemoteDB, Location_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t8030 " +
                    "      ( " +
                    "      t8030r002, t8030r003, " +
                    "      t8030f001, t8030f002 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt8030r002, @pt8030r003, " +
                    "      @pt8030f001, @pt8030f002 " +
                    "      )" +
                    ";" +
                    "SELECT @pt8030r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt8030r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt8030r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt8030r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt8030f001", poRecord.LocationID));
                oParameters.Add(new SqlParameter("@pt8030f002", poRecord.LocationName));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, Location_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t8030 " +
                "   SET " +
                "      t8030r002 = @pt8030r002, " +
                "      t8030f001 = @pt8030f001, " +
                "      t8030f002 = @pt8030f002 " +
                "   WHERE (t8030r001 = @pt8030r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt8030r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt8030r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt8030f001", poRecord.LocationID));
            oParameters.Add(new SqlParameter("@pt8030f002", poRecord.LocationName));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, Location_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t8030 " +
                "   WHERE (t8030f001 = @pt8030f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt8030f001", poRecord.LocationID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }

        public bool InsertDetail(DatabaseContext oRemoteDB, LocationDetail_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t8031 " +
                    "      ( " +
                    "      t8031r002, t8031r003, " +
                    "      t8031f001, t8031f002, t8031f003, t8031f004 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt8031r002, @pt8031r003, " +
                    "      @pt8031f001, @pt8031f002, @pt8031f003, @pt8031f004 " +
                    "      )" +
                    ";" +
                    "SELECT @pt8031r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt8031r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt8031r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt8031r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt8031f001", poRecord.ParentID));
                oParameters.Add(new SqlParameter("@pt8031f002", poRecord.ReaderID));
                oParameters.Add(new SqlParameter("@pt8031f003", poRecord.AntennaNo));
                oParameters.Add(new SqlParameter("@pt8031f004", poRecord.MaxRSSI));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateDetail(DatabaseContext oRemoteDB, LocationDetail_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t8031 " +
                "   SET " +
                "      t8031r002 = @pt8031r002, " +
                "      t8031r003 = @pt8031r003, " +
                "      t8031f001 = @pt8031f001, " +
                "      t8031f002 = @pt8031f002, " +
                "      t8031f003 = @pt8031f003, " +
                "      t8031f004 = @pt8031f004 " +
                "   WHERE (t8031r001 = @pt8031r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt8031r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt8031r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt8031r003", poRecord.RecordStatus));
            oParameters.Add(new SqlParameter("@pt8031f001", poRecord.ParentID));
            oParameters.Add(new SqlParameter("@pt8031f002", poRecord.ReaderID));
            oParameters.Add(new SqlParameter("@pt8031f003", poRecord.AntennaNo));
            oParameters.Add(new SqlParameter("@pt8031f004", poRecord.MaxRSSI));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool DeleteDetail(DatabaseContext oRemoteDB, LocationDetail_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t8031 " +
                "   WHERE (t8031r001 = @pt8031r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt8031r001", poRecord.RecordID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }

    }
}