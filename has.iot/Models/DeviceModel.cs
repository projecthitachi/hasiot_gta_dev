﻿using has.iot.Components;
using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class Device_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string MacAddress { get; set; }
        public int GatewayID { get; set; }
        public string PluginID { get; set; }
        public string BrandID { get; set; }
        public string LocationID { get; set; }
        public string LocationName { get; set; }
    }

    public class DeviceAndPlugin_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string MacAddress { get; set; }
        public int GatewayID { get; set; }
        public string PluginID { get; set; }
        public string BrandID { get; set; }
        public string LocationID { get; set; }
        // plugin prop
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public string Payload { get; set; }
        public string Protocol { get; set; }
        public string Category { get; set; }
        public string Version { get; set; }

        //device addon field
        public string Status { get; set; }
    }

    public class ThermalLog
    {
        [Display(Name = "ID")]
        public long RecordID { get; set; }
        [Display(Name = "Timestamp")]
        public DateTime RecordTimestamp { get; set; }
        [Display(Name = "Status")]
        public int RecordStatus { get; set; }
        //------------------------------
        [Display(Name = "Camera")]
        public int CameraIndex { get; set; }
        [Display(Name = "Rectangle")]
        public int RectangleIndex { get; set; }
        [Display(Name = "Temp Detected")]
        public Decimal MaxTemp { get; set; }
        [Display(Name = "Min Temp Detected")]
        public Decimal MinTemp { get; set; }
        [Display(Name = "Avg Temp Detected")]
        public Decimal AvgTemp { get; set; }
        public int Alarm { get; set; }
        public int Warning { get; set; }
    }

    public class SmartSwitchhLog_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public decimal Energy { get; set; }
        public decimal Current { get; set; }
        public decimal ActivePower { get; set; }
        public decimal ApparentPower { get; set; }
        public string ChartGauge { get; set; }
        public string ChartBar { get; set; }
        public string GadGet { get; set; }
        public string fullcharger { get; set; }

    }

    public class SmartSwitchSum_REC
    {
        public decimal Dayly { get; set; }
        public decimal DaylyAvg { get; set; }
        public decimal Monthly { get; set; }
        public decimal MonthlyAvg { get; set; }
    }
    public class ChartDonat_REC
    {
        public string Column { get; set; }
        public string GadgetID { get; set; }
        public string DeviceID { get; set; }
        public decimal Watt { get; set; }
    }

    public class ChartTopThree_REC
    {
        public string month { get; set; }
        public string date { get; set; }
        public decimal? device01 { get; set; }
        public decimal? device02 { get; set; }
        public decimal? device03 { get; set; }
        public decimal? device04 { get; set; }
        public decimal? device05 { get; set; }
        public decimal? saved01 { get; set; }
        public decimal? saved02 { get; set; }
        public decimal? saved03 { get; set; }
        public decimal? saved04 { get; set; }
        public decimal? saved05 { get; set; }
        public decimal? avg { get; set; }
    }

    public class DeviceModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public DeviceModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }

        public DbRawSqlQuery<Device_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = "" +
                "SELECT " +
                "t1.t8010f001 AS DeviceID, t1.t8010f002 AS DeviceName, " +
                "t1.t8010f003 AS IPAddress, t1.t8010f004 AS MacAddress, " +
                "t1.t8010f005 AS PluginID,t1.t8010f006 AS BrandID, " +
                "t1.t8010f007 AS GatewayID, t1.t8010f008 AS LocationID, t2.t8020f002 AS LocationName " +
                "FROM  t8010 AS t1 LEFT JOIN t8020 t2 ON t2.t8020f001 = t1.t8010f008  " +
                "WHERE t1.t8010r003 = 0 " +
                "ORDER BY t1.t8010f005 DESC " +
                ";"
                ;

            var vQuery = oRemoteDB.Database.SqlQuery<Device_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<DeviceAndPlugin_REC> GetListByPlugin(string plugin)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = "" +
                "SELECT " +
                "      t1.t8010r001 AS RecordID, t1.t8010r002 AS RecordTimestamp, t1.t8010r003 AS RecordStatus, " +
                "      t1.t8010f001 AS DeviceID, t1.t8010f002 AS DeviceName, t1.t8010f003 AS IPAddress, t1.t8010f004 AS MacAddress, t1.t8010f005 AS Brand,t1.t8010f006 AS Category, t2.t9000f001 AS Name, t2.t9000f002 AS Alias, t2.t9000f003 AS Description, t2.t9000f004 AS Version, t2.t9000f005 AS Payload, t2.t9000f006 AS Protocol " +
                "   FROM t8010 t1 JOIN t9000 t2 ON t2.t9000f001 = t1.t8010f006 " +
                "   WHERE t8010f006 = '" + plugin + "'" +
                "   AND t8010r003 = 0 " +
                "   ORDER BY t1.t8010r002 DESC " +
                ";"
                ;

            var vQuery = oRemoteDB.Database.SqlQuery<DeviceAndPlugin_REC>(sSQL);

            return vQuery;
        }

        public DbRawSqlQuery<DeviceAndPlugin_REC> GetOne(long RecordID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = "" +
                "SELECT " +
                "      t1.t8010r001 AS RecordID, t1.t8010r002 AS RecordTimestamp, t1.t8010r003 AS RecordStatus, " +
                "      t1.t8010f001 AS DeviceID, t1.t8010f002 AS DeviceName, t1.t8010f003 AS IPAddress, t1.t8010f004 AS MacAddress, t1.t8010f005 AS Brand,t1.t8010f006 AS Category, t2.t9000f001 AS Name, t2.t9000f002 AS Alias, t2.t9000f003 AS Description, t2.t9000f004 AS Version, t2.t9000f005 AS Payload, t2.t9000f006 AS Protocol " +
                "   FROM t8010 t1 JOIN t9000 t2 ON t2.t9000f001 = t1.t8010f006 " +
                "   WHERE t1.t8010r001 = " + RecordID +
                "   AND t8010r003 = 0 " +
                ";"
                ;

            var vQuery = oRemoteDB.Database.SqlQuery<DeviceAndPlugin_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<DeviceAndPlugin_REC> GetOneID(long DeviceID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = "" +
                "SELECT " +
                "t1.t8010r001 AS RecordID, t1.t8010r002 AS RecordTimestamp, t1.t8010r003 AS RecordStatus, " +
                "t1.t8010f001 AS DeviceID, t1.t8010f002 AS DeviceName, " +
                "t1.t8010f003 AS IPAddress, t1.t8010f004 AS MacAddress, " +
                "t1.t8010f005 AS PluginID,t1.t8010f006 AS BrandID, " +
                "t1.t8010f007 AS GatewayID, t1.t8010f008 AS LocationID " +
                "FROM t8010 t1 JOIN t9000 t2 ON t2.t9000f001 = t1.t8010f006 " +
                "WHERE t1.t8010f001 = '" + DeviceID + "'" +
                "AND t8010r003 = 0 " +
                "; ";

            var vQuery = oRemoteDB.Database.SqlQuery<DeviceAndPlugin_REC>(sSQL);

            return vQuery;
        }

        public bool Insert(DatabaseContext oRemoteDB, Device_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t8010 " +
                    "      ( " +
                    "      t8010r002, t8010r003, " +
                    "      t8010f001, t8010f002, t8010f003, t8010f004, t8010f005, t8010f006, t8010f007, t8010f008 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt8010r002, @pt8010r003, " +
                    "      @pt8010f001, @pt8010f002, @pt8010f003, @pt8010f004, @pt8010f005, @pt8010f006, @pt8010f007, @pt8010f008 " +
                    "      )" +
                    ";" +
                    "SELECT @pt8010r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt8010r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt8010r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt8010r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt8010f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt8010f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt8010f003", poRecord.IPAddress));
                oParameters.Add(new SqlParameter("@pt8010f004", poRecord.MacAddress));
                oParameters.Add(new SqlParameter("@pt8010f005", poRecord.PluginID));
                oParameters.Add(new SqlParameter("@pt8010f006", poRecord.BrandID));
                oParameters.Add(new SqlParameter("@pt8010f007", poRecord.GatewayID));
                oParameters.Add(new SqlParameter("@pt8010f008", poRecord.LocationID));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        public bool Update(DatabaseContext oRemoteDB, Device_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t8010 " +
                "   SET " +
                "      t8010f002 = @pt8010f002, " +
                "      t8010f003 = @pt8010f003, " +
                "      t8010f004 = @pt8010f004, " +
                "      t8010f005 = @pt8010f005, " +
                "      t8010f006 = @pt8010f006, " +
                "      t8010f007 = @pt8010f007, " +
                "      t8010f008 = @pt8010f008 " +
                "   WHERE (t8010f001 = @pt8010f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt8010f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt8010f002", poRecord.DeviceName));
            oParameters.Add(new SqlParameter("@pt8010f003", poRecord.IPAddress));
            oParameters.Add(new SqlParameter("@pt8010f004", poRecord.MacAddress));
            oParameters.Add(new SqlParameter("@pt8010f005", poRecord.PluginID));
            oParameters.Add(new SqlParameter("@pt8010f006", poRecord.BrandID));
            oParameters.Add(new SqlParameter("@pt8010f007", poRecord.GatewayID));
            oParameters.Add(new SqlParameter("@pt8010f008", poRecord.LocationID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, Device_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t8010 " +
                "   WHERE (t8010f001 = @pt8010f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt8010f001", poRecord.DeviceID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }

        public bool InsertThermalLog(DatabaseContext oRemoteDB, ThermalLog poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;


                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t9001 " +
                    "      ( " +
                    "      t9001r002, t9001r003, " +
                    "      t9001f001, t9001f002, t9001f003, t9001f004, t9001f005, t9001f006, t9001f007 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt9001r002, @pt9001r003, " +
                    "      @pt9001f001, @pt9001f002, @pt9001f003, @pt9001f004, @pt9001f005, @pt9001f006, @pt9001f007 " +
                    "      )" +
                    ";" +
                    "SELECT @pt9001r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt9001r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt9001r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt9001r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt9001f001", poRecord.CameraIndex));
                oParameters.Add(new SqlParameter("@pt9001f002", poRecord.RectangleIndex));
                oParameters.Add(new SqlParameter("@pt9001f003", poRecord.MaxTemp));
                oParameters.Add(new SqlParameter("@pt9001f004", poRecord.MinTemp));
                oParameters.Add(new SqlParameter("@pt9001f005", poRecord.AvgTemp));
                oParameters.Add(new SqlParameter("@pt9001f006", poRecord.Alarm));
                oParameters.Add(new SqlParameter("@pt9001f007", poRecord.Warning));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public DbRawSqlQuery<ThermalLog> GetThermalLog()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "" +
                "Select " +
                "t9001r001 as RecordID," +
                "t9001r002 as RecordTimestamp," +
                "t9001r003 as RecordStatus," +
                "t9001f001 as CameraIndex," +
                "t9001f002 as RectangleIndex," +
                "t9001f003 as MaxTemp," +
                "t9001f004 as MinTemp," +
                "t9001f005 as AvgTemp," +
                "t9001f006 as Alarm," +
                "t9001f007 as Warning " +
                "From t9001";
            var vQuery = oRemoteDB.Database.SqlQuery<ThermalLog>(sSQL);
            return vQuery;
        }
    }

    public class SmartSwitchModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public SmartSwitchModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }

        public DbRawSqlQuery<SmartSwitchSum_REC> GetDaylyToday(string DeviceID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = "SELECT " +
            " (SELECT ISNULL(CAST(SUM(ta.t9002f007) / 1000 AS DECIMAL(18,2)),0.00) AS ActivePower FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), GETDATE(), 111) AND ta.t9002f001 = '" + DeviceID + "') as Dayly,  " +
            " (SELECT ISNULL(CAST((SUM(ta.t9002f007) / 1000) / 24 AS DECIMAL(18,2)),0.00) AS ActivePower FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), GETDATE(), 111) AND ta.t9002f001 = '" + DeviceID + "') as DaylyAvg,  " +
            " (SELECT ISNULL(CAST(SUM(ta.t9002f007) / 1000 AS DECIMAL(18,2)),0.00) AS ActivePower FROM t9002 ta WHERE DATEPART(MONTH, ta.t9002r002) = DATEPART(MONTH, GETDATE()) AND ta.t9002f001 = '" + DeviceID + "' ) AS Monthly,  " +
            " (SELECT ISNULL(CAST((SUM(ta.t9002f007) / 1000) / 720 AS DECIMAL(18,2)),0.00) AS ActivePower FROM t9002 ta WHERE DATEPART(MONTH, ta.t9002r002) = DATEPART(MONTH, GETDATE()) AND ta.t9002f001 = '" + DeviceID + "') AS MonthlyAvg ";

            var vQuery = oRemoteDB.Database.SqlQuery<SmartSwitchSum_REC>(sSQL);

            return vQuery;
        }
        public bool Insert(DatabaseContext oRemoteDB, SmartSwitchhLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t9002 " +
                    "      ( " +
                    "      t9002r002, t9002r003, " +
                    "      t9002f001, t9002f002, t9002f003, t9002f004, t9002f005, t9002f006, t9002f007, t9002f008 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt9002r002, @pt9002r003, " +
                    "      @pt9002f001, @pt9002f002, @pt9002f003, @pt9002f004, @pt9002f005, @pt9002f006, @pt9002f007, @pt9002f008 " +
                    "      )" +
                    ";" +
                    "SELECT @pt9002r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt9002r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt9002r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt9002r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt9002f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt9002f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt9002f003", poRecord.Voltage));
                oParameters.Add(new SqlParameter("@pt9002f004", poRecord.PowerFactor));
                oParameters.Add(new SqlParameter("@pt9002f005", poRecord.Energy));
                oParameters.Add(new SqlParameter("@pt9002f006", poRecord.Current));
                oParameters.Add(new SqlParameter("@pt9002f007", poRecord.ActivePower));
                oParameters.Add(new SqlParameter("@pt9002f008", poRecord.ApparentPower));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        public DbRawSqlQuery<ChartDonat_REC> GetChartTopThreeDonat()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " SELECT TOP 3 CONCAT('device',SUBSTRING(ta.t9002f001, LEN(ta.t9002f001)-1, 2)) AS 'Column', CONCAT('SmartLight',SUBSTRING(ta.t9002f001, LEN(ta.t9002f001)-1, 2)) AS GadgetID, ta.t9002f001 AS DeviceID, SUM (ta.t9002f007) AS Watt FROM t9002 ta WHERE	ta.t9002f001 NOT IN ('smartplug01','smartplug02') GROUP BY	ta.t9002f001 ORDER BY	SUM (ta.t9002f007) DESC  ";
            var vQuery = oRemoteDB.Database.SqlQuery<ChartDonat_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartDonat_REC> GetChartTopThreeDonatPlug()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT TOP 3 ta.t9002f001 AS DeviceID, SUM(ta.t9002f007) AS Watt FROM t9002 ta WHERE ta.t9002f001 IN ('smartplug01','smartplug02') GROUP BY ta.t9002f001 ORDER BY SUM(ta.t9002f007) DESC   ";
            var vQuery = oRemoteDB.Database.SqlQuery<ChartDonat_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartTopThree_REC> GetChartSumarry(List<ChartDonat_REC> vTopThree, string Type)
        {
            int i = 1;
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " declare @month int, @year int set @month = 4 set @year = 2019 " +
            " SELECT CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date], ";
            foreach (ChartDonat_REC voTopThree in vTopThree)
            {
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 = '" + voTopThree.DeviceID + "') as [device0" + i + "],";
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) + (ISNULL(sum(ta.t9002f007),0.00) /2) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 = '" + voTopThree.DeviceID + "') as [saved0" + i + "], ";
                i++;
            }
            sSQL += " (SELECT ISNULL(sum(ta.t9002f007) / 5,0.00) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 NOT IN ('smartplug01','smartplug02')) as [avg] " +
            " FROM master..spt_values WHERE type = 'P' AND (CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ) < DATEADD(mm,1,CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) )  ";
            if (Type == "monthly")
            {
                i = 1;
                sSQL = " WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11) " +
                      " SELECT CONCAT(DATENAME(MONTH,DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR,DATEADD(MONTH,-N,GETDATE())) ) AS [month]," +
                      " CONVERT (VARCHAR (10),DATEADD(MONTH,-N,GETDATE()),111) AS [date], ";
                foreach (ChartDonat_REC voTopThree in vTopThree)
                {
                    sSQL += " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())) AND ta.t9002f001 = '" + voTopThree.DeviceID + "') as [device0" + i + "], ";
                    i++;
                }
                sSQL += " (SELECT ISNULL(sum(ta.t9002f007) / 5, 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())) AND ta.t9002f001 NOT IN ('smartplug01','smartplug02')) as [avg] FROM R  ORDER BY N DESC ";
            }
            var vQuery = oRemoteDB.Database.SqlQuery<ChartTopThree_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartTopThree_REC> GetChartSummaryByDeviceID(string DeviceID, string type)
        {
            string sSQL = "";
            decimal voSaved = Convert.ToDecimal(0.1);
            switch (DeviceID)
            {
                case "SmartLight01":
                    voSaved = Convert.ToDecimal(0.22);
                    break;
                case "SmartLight02":
                    voSaved = Convert.ToDecimal(0.24);
                    break;
                case "SmartLight03":
                    voSaved = Convert.ToDecimal(0.21);
                    break;
                case "SmartLight04":
                    voSaved = Convert.ToDecimal(0.23);
                    break;
                case "SmartLight05":
                    voSaved = Convert.ToDecimal(0.20);
                    break;
                default:
                    voSaved = Convert.ToDecimal(0.22);
                    break;
            }
            DatabaseContext oRemoteDB = new DatabaseContext();
            if (DeviceID == "Average")
            {
                sSQL = " declare @month int, @year int set @month = 4 set @year = 2019 " +
                " SELECT CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date], ";
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) ) as [device01],";
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + ") FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) ) as [saved01] ";
                sSQL += " FROM master..spt_values WHERE type = 'P' AND (CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ) < DATEADD(mm,1,CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) )  ";
            }
            else
            {
                sSQL = " declare @month int, @year int set @month = 4 set @year = 2019 " +
                " SELECT CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date], ";
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 = 'smartswitch" + DeviceID.Substring(DeviceID.Length - 2) + "') as [device01],";
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + ") FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 = 'smartswitch" + DeviceID.Substring(DeviceID.Length - 2) + "') as [saved01] ";
                sSQL += " FROM master..spt_values WHERE type = 'P' AND (CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ) < DATEADD(mm,1,CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) )  ";
            }

            if (type == "monthly")
            {
                if (DeviceID != "Average")
                {
                    sSQL = "WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11)  " +
                    " SELECT CONCAT(DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR, DATEADD(MONTH, -N, GETDATE())) ) AS[month],  " +
                    " CONVERT(VARCHAR(10), DATEADD(MONTH, -N, GETDATE()), 111) AS[date],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = 'smartswitch" + DeviceID.Substring(DeviceID.Length - 2) + "') as [device01],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + ") FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = 'smartswitch" + DeviceID.Substring(DeviceID.Length - 2) + "') as [saved01]  " +
                    " FROM R ORDER BY N DESC";
                }
                else
                {
                    sSQL = "WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11)  " +
                    " SELECT CONCAT(DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR, DATEADD(MONTH, -N, GETDATE())) ) AS[month],  " +
                    " CONVERT(VARCHAR(10), DATEADD(MONTH, -N, GETDATE()), 111) AS[date],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 NOT IN ('smartplug01', 'smartplug02')) as [device01],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + ") FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 NOT IN ('smartplug01', 'smartplug02')) as [saved01]  " +
                    " FROM R ORDER BY N DESC";
                }
            }
            var vQuery = oRemoteDB.Database.SqlQuery<ChartTopThree_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartTopThree_REC> GetChartTopThreePlug(string Type)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "declare @month int, @year int set @month = 4 set @year = 2019 SELECT  " +
            " CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date],  " +
            " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 = 'smartplug01') as [device01],  " +
            " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 = 'smartplug02') as [device02],  " +
            " (SELECT ISNULL(sum(ta.t9002f007),0.00) + (ISNULL(sum(ta.t9002f007),0.00) /2) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 = 'smartplug01') as [saved01], " +
            " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) / 2) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 = 'smartplug02') as [saved02], " +
            " (SELECT ISNULL(sum(ta.t9002f007) / 2, 0.00) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 IN('smartplug01','smartplug02')) as [avg]  " +
            " FROM master..spt_values WHERE type = 'P' AND(CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number) < DATEADD(mm, 1, CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME)) ";
            if (Type == "monthly")
            {
                sSQL = "WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11)  " +
                " SELECT CONCAT(DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR, DATEADD(MONTH, -N, GETDATE())) ) AS[month],  " +
                " CONVERT(VARCHAR(10), DATEADD(MONTH, -N, GETDATE()), 111) AS[date],  " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = 'smartplug01') as [device01],  " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = 'smartplug02') as [device02],  " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00) / 2 FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 IN('smartplug01', 'smartplug02')) as [avg]  " +
                " FROM R ORDER BY N DESC";
            }

            var vQuery = oRemoteDB.Database.SqlQuery<ChartTopThree_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartTopThree_REC> GetSmartPlugSaving(string DeviceID, string Type)
        {
            string sSQL = "";
            decimal voSaved = Convert.ToDecimal(0.1);
            switch (DeviceID)
            {
                case "SmartPlug01":
                    voSaved = Convert.ToDecimal(0.2);
                    break;
                case "SmartPlug02":
                    voSaved = Convert.ToDecimal(0.25);
                    break;
                default:
                    voSaved = Convert.ToDecimal(0.22);
                    break;
            }
            DatabaseContext oRemoteDB = new DatabaseContext();
            if (DeviceID != "Average")
            {
                sSQL = "declare @month int, @year int set @month = 4 set @year = 2019 SELECT  " +
                " CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date],  " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 = '" + DeviceID + "') as [device01], " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + ") FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 = '" + DeviceID + "') as [saved01] " +
                " FROM master..spt_values WHERE type = 'P' AND(CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number) < DATEADD(mm, 1, CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME)) ";
            }
            else
            {
                sSQL = "declare @month int, @year int set @month = 4 set @year = 2019 SELECT  " +
                " CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date],  " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00)  FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 IN('smartplug01','smartplug02')) as [device01], " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00)  + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + " ) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 IN('smartplug01','smartplug02')) as [saved01] " +
                " FROM master..spt_values WHERE type = 'P' AND(CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number) < DATEADD(mm, 1, CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME)) ";
            }

            if (Type == "monthly")
            {
                if (DeviceID != "Average")
                {
                    sSQL = "WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11)  " +
                    " SELECT CONCAT(DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR, DATEADD(MONTH, -N, GETDATE())) ) AS[month],  " +
                    " CONVERT(VARCHAR(10), DATEADD(MONTH, -N, GETDATE()), 111) AS[date],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = '" + DeviceID + "') as [device01],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + " ) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = '" + DeviceID + "') as [saved01]  " +
                    " FROM R ORDER BY N DESC";
                }
                else
                {
                    sSQL = "WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11)  " +
                    " SELECT CONCAT(DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR, DATEADD(MONTH, -N, GETDATE())) ) AS[month],  " +
                    " CONVERT(VARCHAR(10), DATEADD(MONTH, -N, GETDATE()), 111) AS[date],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 IN('smartplug01', 'smartplug02')) as [device01],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + " ) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 IN('smartplug01', 'smartplug02')) as [saved01]  " +
                    " FROM R ORDER BY N DESC";
                }
            }
            var vQuery = oRemoteDB.Database.SqlQuery<ChartTopThree_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartTopThree_REC> GetSmartPlugGadget(string DeviceID)
        {
            string sSQL = "";
            DatabaseContext oRemoteDB = new DatabaseContext();
            if (DeviceID == "SmartPlug02")
            {
                sSQL = "SELECT 'SmartPhone' AS [date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.424 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.106 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01] " +
                " UNION SELECT 'Laptop' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.2475 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.0825 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01] " +
                " UNION SELECT 'Unknown' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.1014 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.0286 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01]";

            }
            if (DeviceID == "SmartPlug01")
            {
                sSQL = "SELECT 'SmartPhone' AS [date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.1014 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.0286 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01] " +
                " UNION SELECT 'Laptop' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.2475 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.0825 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01] " +
                " UNION SELECT 'Unknown' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.424 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.106 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01]";
            }
            if (DeviceID == "Average")
            {
                sSQL = "SELECT 'SmartPhone' AS [date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.344 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug01') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.086 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug01') as [saved01] " +
                " UNION SELECT 'Laptop' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.104 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug02') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.026 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug01') as [saved01] " +
                " UNION SELECT 'Unknown' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.344 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug01') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.086 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug02') as [saved01]";
            }
            var vQuery = oRemoteDB.Database.SqlQuery<ChartTopThree_REC>(sSQL);
            return vQuery;
        }
    }
}