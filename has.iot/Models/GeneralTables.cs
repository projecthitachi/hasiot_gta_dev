﻿using has.iot.Components;
using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class GeneralTables
    {
        private long t8990r001;
        [Display(Name = "RecordID")]
        public virtual long RecordID { get { return t8990r001; } set { t8990r001 = value; } }

        private DateTime t8990r002;
        [Display(Name = "RecordTimestamp")]
        public virtual DateTime RecordTimestamp { get { return t8990r002; } set { t8990r002 = value; } }

        private Int32 t8990r003;
        [Display(Name = "Record Status")]
        public virtual Int32 RecordStatus { get { return t8990r003; } set { t8990r003 = value; } }

        private string t8990f001;
        [Display(Name = "ID")]
        public virtual string ID { get { return t8990f001; } set { t8990f001 = value; } }

        private string t8990f002;
        [Display(Name = "Name")]
        public virtual string Name { get { return t8990f002; } set { t8990f002 = value; } }

        private int t8990f003;
        [Display(Name = "Type")]
        public virtual int ListType { get { return t8990f003; } set { t8990f003 = value; } }

        private string t8990f004;
        [Display(Name = "Value")]
        public virtual string Value { get { return t8990f004; } set { t8990f004 = value; } }

        public virtual string ListTypeName { get; set; }
        
    }
    public class GeneralType
    {
        private long t8991r001;
        [Display(Name = "RecordID")]
        public virtual long RecordID { get { return t8991r001; } set { t8991r001 = value; } }

        private DateTime t8991r002;
        [Display(Name = "RecordTimestamp")]
        public virtual DateTime RecordTimestamp { get { return t8991r002; } set { t8991r002 = value; } }

        private Int32 t8991r003;
        [Display(Name = "Record Status")]
        public virtual Int32 RecordStatus { get { return t8991r003; } set { t8991r003 = value; } }

        private string t8991f001;
        [Display(Name = "ID")]
        public virtual string ID { get { return t8991f001; } set { t8991f001 = value; } }

        private string t8991f002;
        [Display(Name = "Name")]
        public virtual string Name { get { return t8991f002; } set { t8991f002 = value; } }
    }

    public class GeneralTablesQuery
    {
        private string t8990FieldsSelect = @"
            t8990r001 as RecordID,
            t8990r002 as RecordTimestamp,
            t8990r003 as RecordStatus,
            t8990f001 as ID,
            t8990f002 as Name,
            t8990f003 as ListType,
            t8990f004 as Value
        ";
        private string t8991FieldsSelect = @"
            t8991r001 as RecordID,
            t8991r002 as RecordTimestamp,
            t8991r003 as RecordStatus,
            t8991f001 as ID,
            t8991f002 as Name
        ";
        public string ErrorMessage = "";
        
        public DbRawSqlQuery<GeneralTables> GeneralTablesGetList(DatabaseContext oRemoteDB)
        {
            string sSQL = "SELECT " + t8990FieldsSelect + ", t8991f002 as ListTypeName from t8990 LEFT JOIN t8991 on t8990f003 = t8991f001;";
            var vQuery = oRemoteDB.Database.SqlQuery<GeneralTables>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<GeneralType> GeneralTypeGetList(DatabaseContext oRemoteDB)
        {
            string sSQL = "SELECT " + t8991FieldsSelect + " from t8991;";
            var vQuery = oRemoteDB.Database.SqlQuery<GeneralType>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<GeneralTables> GeneralTablesGetListByType(DatabaseContext oRemoteDB, string sListType)
        {
            string sSQL = "SELECT " + t8990FieldsSelect + " from t8990 where t8990f003 = '"+ sListType + "';";
            var vQuery = oRemoteDB.Database.SqlQuery<GeneralTables>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<GeneralTables> GeneralTablesGetIDByType(string sText, string sListType)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t8990FieldsSelect + " from t8990 where t8990f001 = '"+ sText + "' AND t8990f003 = '" + sListType + "';";
            var vQuery = oRemoteDB.Database.SqlQuery<GeneralTables>(sSQL);
            return vQuery;
        }

        public bool Insert(DatabaseContext oRemoteDB, GeneralTables poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t8990 " +
                    "      ( " +
                    "      t8990r002, t8990r003, " +
                    "      t8990f001, t8990f002, " +
                    "      t8990f003 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt8990r002, @pt8990r003, " +
                    "      @pt8990f001, @pt8990f002, " +
                    "      @pt8990f003 " +
                    "      )" +
                    ";" +
                    "SELECT @pt8990r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt8990r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt8990r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt8990r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt8990f001", poRecord.ID));
                oParameters.Add(new SqlParameter("@pt8990f002", poRecord.Name));
                oParameters.Add(new SqlParameter("@pt8990f003", poRecord.ListType));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        public bool Update(DatabaseContext oRemoteDB, GeneralTables poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t8990 " +
                "   SET " +
                "      t8990f001 = @pt8990f001, " +
                "      t8990f002 = @pt8990f002, " +
                "      t8990f003 = @pt8990f003 " +
                "   WHERE (t8990r001 = @pt8990r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt8990r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt8990f001", poRecord.ID));
            oParameters.Add(new SqlParameter("@pt8990f002", poRecord.Name));
            oParameters.Add(new SqlParameter("@pt8990f003", poRecord.ListType));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, GeneralTables poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t8990 " +
                "   WHERE (t8990r001 = @pt8990r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt8990r001", poRecord.RecordID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }

        public bool InsertType(DatabaseContext oRemoteDB, GeneralType poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t8991 " +
                    "      ( " +
                    "      t8991r002, t8991r003, " +
                    "      t8991f001, t8991f002 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt8991r002, @pt8991r003, " +
                    "      @pt8991f001, @pt8991f002 " +
                    "      )" +
                    ";" +
                    "SELECT @pt8991r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt8991r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt8991r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt8991r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt8991f001", poRecord.ID));
                oParameters.Add(new SqlParameter("@pt8991f002", poRecord.Name));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        public bool UpdateType(DatabaseContext oRemoteDB, GeneralType poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t8991 " +
                "   SET " +
                "      t8991f001 = @pt8991f001, " +
                "      t8991f002 = @pt8991f002 " +
                "   WHERE (t8991r001 = @pt8991r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt8991r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt8991f001", poRecord.ID));
            oParameters.Add(new SqlParameter("@pt8991f002", poRecord.Name));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool DeleteType(DatabaseContext oRemoteDB, GeneralType poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t8991 " +
                "   WHERE (t8991r001 = @pt8991r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt8991r001", poRecord.RecordID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
    }
}