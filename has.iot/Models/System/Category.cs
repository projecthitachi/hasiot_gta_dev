﻿using has.iot.Components;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace has.iot.Models.System
{
    public class Category_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
    }

    public class Category
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public Category()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }

        #region "t70100fieldselect"
        private string t70100fieldselect = @"
            t70100r001 as RecordID,
            t70100r002 as RecordTimestamp,
            t70100r003 as RecordStatus,
            t70100f001 as CategoryID,
            t70100f002 as CategoryName,
            t70100f003 as Description,
            t70100f004 as Image";
        #endregion

        public DbRawSqlQuery<Category_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70100fieldselect + " FROM t70100 ORDER BY t70100r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<Category_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Category_REC> GetOne(long RecordID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70100fieldselect + " FROM t70100 WHERE t70100r001 = " + RecordID;
            var vQuery = oRemoteDB.Database.SqlQuery<Category_REC>(sSQL);
            return vQuery;
        }


        public DbRawSqlQuery<Category_REC> GetOneByName(string name)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70100fieldselect + " FROM t70100 WHERE t70100f001 = " + name;
            var vQuery = oRemoteDB.Database.SqlQuery<Category_REC>(sSQL);
            return vQuery;
        }
        public bool Insert(Category_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70100 " +
                    "      ( " +
                    "      t70100r002, t70100r003, " +
                    "      t70100f001, t70100f002, t70100f003,t70100f004) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70100r002, @pt70100r003, " +
                    "      @pt70100f001, @pt70100f002, @pt70100f003, @pt70100f004)" +
                    ";" +
                    "SELECT @pt70100r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70100r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70100r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70100r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70100f001", poRecord.CategoryID));
                oParameters.Add(new SqlParameter("@pt70100f002", poRecord.CategoryName));
                oParameters.Add(new SqlParameter("@pt70100f003", poRecord.Description));
                oParameters.Add(new SqlParameter("@pt70100f004", poRecord.Image));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                DatabaseContext oRemoteDB = new DatabaseContext();
                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Insert(string sSQL)
        {
            bool bReturn = true;
            //------------------------------
            try
            {

                DatabaseContext oRemoteDB = new DatabaseContext();
                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL);

                if (nReturn == 1)
                {
                    //poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public long Insert(DatabaseContext oRemoteDB, Category_REC poRecord)
        {
            long bReturn = 0;
            //------------------------------
            poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;

            GlobalFunction.TrimNull(poRecord);
            //----------
            string sSQL = "" +
                "INSERT INTO t70100 " +
                "      ( " +
                "      t70100r002, t70100r003, " +
                "      t70100f001, t70100f002, t70100f003, t70100f004) " +
                "      VALUES " +
                "      (" +
                "      @pt70100r002, @pt70100r003, " +
                "      @pt70100f001, @pt70100f002, @pt70100f003, @pt70100f004)" +
                ";" +
                "SELECT @pt70100r001 = SCOPE_IDENTITY(); "
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter { ParameterName = "@pt70100r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
            oParameters.Add(new SqlParameter("@pt70100r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70100r003", poRecord.RecordStatus));

            oParameters.Add(new SqlParameter("@pt70100f001", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70100f002", poRecord.CategoryName));
            oParameters.Add(new SqlParameter("@pt70100f003", poRecord.Description));
            oParameters.Add(new SqlParameter("@pt70100f004", poRecord.Image));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

            if (nReturn == 1)
            {
                bReturn = Convert.ToInt64(vSqlParameter[0].Value);
            }
            else
            {
                ErrorMessage = "Failed to insert record!";
                bReturn = 0;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(Category_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                DateTime dRecordTimestamp = poRecord.RecordTimestamp;
                //----------
                //poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                //poRecord.RecordStatus = 0;
                //poRecord.RecordFlag = 0;
                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "UPDATE t70100 " +
                    "   SET " +
                    "      t70100f002 = @pt70100f002, " +
                    "      t70100f003 = @pt70100f003, " +
                    "      t70100f004 = @pt70100f004 " +
                    "   WHERE (t70100f001 = @pt70100f001) " +
                    ";"
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70100f001", poRecord.CategoryID));
                oParameters.Add(new SqlParameter("@pt70100f002", poRecord.CategoryName));
                oParameters.Add(new SqlParameter("@pt70100f003", poRecord.Description));
                oParameters.Add(new SqlParameter("@pt70100f004", poRecord.Image));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    oTransaction.Commit();
                }
                else
                {
                    oTransaction.Rollback();
                    ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(Category_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                //poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                //poRecord.RecordStatus = 0;
                //poRecord.RecordFlag = 0;
                //----------
                string sSQL = "" +
                    "DELETE t70100 " +
                    "   WHERE (t70100f001 = @pt70100f001) " +
                    ";"
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter("@pt70100f001", poRecord.CategoryID));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    oTransaction.Commit();
                }
                else
                {
                    oTransaction.Rollback();
                    ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";

                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

    }
}