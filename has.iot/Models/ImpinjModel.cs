﻿using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class Impinj_REC
    {
        [Key]
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Display(Name = "Device ID")]
        public string DeviceID { get; set; }

        [Display(Name = "Model Name")]
        public string ModelName { get; set; }
        [Display(Name = "Category ID")]
        public string CategoryID { get; set; }
        [Display(Name = "Operating Region")]
        public string OperatingRegion { get; set; }
        [Display(Name = "Antenna Hub Enable?")]
        public int AntennaHubEnabled { get; set; }
        [Display(Name = "Reader Mode")]
        public string ReaderMode { get; set; }
        [Display(Name = "Search Mode")]
        public string SearchMode { get; set; }
        [Display(Name = "Session")]
        public Int32 Session { get; set; }
        [Display(Name = "IP Address")]
        public string IPAddress { get; set; }
    }
    public class ImpinjAntenna_REC
    {
        [Key]
        public long? RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Display(Name = "Reader ID")]
        public long ReaderID { get; set; }
        [Display(Name = "Antenna Port")]
        public int AntennaPort { get; set; }
        [Display(Name = "Antenna No")]
        public int AntennaNo { get; set; }
        [Display(Name = "Tx Power")]
        public Decimal TxPower { get; set; }
        [Display(Name = "Rx Sensitivity")]
        public int RxSensitivity { get; set; }
    }
    public class ImpinjModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public ImpinjModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t70205fieldselect"
        private string t70205fieldselect = @"
            t70205r001 as RecordID,
            t70205r002 as RecordTimestamp,
            t70205r003 as RecordStatus,
            t70205f001 as DeviceID,
            t70205f002 as ModelName,
            t70205f003 as CategoryID,
            t70205f004 as OperatingRegion,
            t70205f005 as AntennaHubEnabled,
            t70205f006 as ReaderMode,
            t70205f007 as SearchMode,
            t70205f008 as Session,
            t70205f009 as IPAddress";
        #endregion
        #region "t70206fieldselect"
        private string t70206fieldselect = @"
            t70206r001 as RecordID,
            t70206r002 as RecordTimestamp,
            t70206r003 as RecordStatus,
            t70206f001 as ReaderID,
            t70206f002 as AntennaPort,
            t70206f003 as AntennaNo,
            t70206f004 as TxPower,
            t70206f005 as RxSensitivity";
        #endregion

        public DbRawSqlQuery<Impinj_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70205fieldselect + " FROM t70205 ORDER BY t70205r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<Impinj_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ImpinjAntenna_REC> GetAntennaByParentID(DatabaseContext oRemoteDB, long RecordID)
        {
            string sSQL = "SELECT " + t70206fieldselect + " FROM t70206 where t70206f001 = " + RecordID + " ORDER BY t70206f002 ASC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjAntenna_REC>(sSQL);
            return vQuery;
        }
    }
}