﻿using has.iot.Components;
using has.iot.Models;
using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    public class PartialController : Controller
    {
        public ActionResult _MainMenu()
        {
            Brands oClass = new Brands();
            var vResult = oClass.GetListBrand()
                .GroupBy(w => w.CategoryID)
                .Select(grp=>grp.Take(1).SingleOrDefault()).ToList();
            ViewData["Data"] = vResult;
            return PartialView("Partials/_MainMenu");
        }
        public ActionResult _MainMenuIotCategory()
        {
            Category oClass = new Category();
            var vResult = oClass.GetList().ToList();
            ViewData["Data"] = vResult;
            return PartialView("Partials/_MainMenuIotCategory");
        }
        public ActionResult _LocationPopup()
        {
            return PartialView("WindowsPopup/_Location");
        }
    }
}