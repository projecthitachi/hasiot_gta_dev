﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using has.iot.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Kendo.Mvc.UI;
using has.iot.Models.System;
using Kendo.Mvc.Extensions;
using has.iot.Components;

namespace has.iot.Controllers
{
    [IsLoggedIn]
    public class DeviceManagerController : Controller
    {
        // GET: DeviceManager
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest]DataSourceRequest poRequest, Device_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    DeviceModel oClass = new DeviceModel();
                    if (!oClass.Insert(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        public ActionResult Read([DataSourceRequest]DataSourceRequest poRequest, string plugin)
        {
            DeviceModel oClass = new DeviceModel();
            var vResult = oClass.GetList().ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest]DataSourceRequest poRequest, Device_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    DeviceModel oClass = new DeviceModel();
                    if (!oClass.Update(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest poRequest, Device_REC poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    DeviceModel oClass = new DeviceModel();
                    if (!oClass.Delete(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        public ActionResult GetBrandByPlugin(string pluginID, [DataSourceRequest]DataSourceRequest poRequest)
        {
            List<SelectListItem> listBrand = new List<SelectListItem> { };
            return Json(listBrand, JsonRequestBehavior.AllowGet);
        }

        // SmartSwitch
        public ActionResult GetSmartSwitchLogSum(string DeviceID)
        {
            SmartSwitchModel oClass = new SmartSwitchModel();
            var vResult = oClass.GetDaylyToday(DeviceID).SingleOrDefault();
            return Json(vResult);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SmartSwitchLog(SmartSwitchhLog_REC poRecord)
        {
            Dictionary<string, string> vsLogMessage = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            var oTransaction = oRemoteDB.Database.BeginTransaction();
            try
            {
                SmartSwitchModel oClass = new SmartSwitchModel();
                if (!oClass.Insert(oRemoteDB, poRecord))
                {
                    ModelState.AddModelError("Error", oClass.ErrorMessage);
                }
                vsLogMessage.Add("errorcode", "0");
                vsLogMessage.Add("title", "Success");
                vsLogMessage.Add("msg", "success");
                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                vsLogMessage.Add("errorcode", "100");
                vsLogMessage.Add("title", "warning");
                vsLogMessage.Add("msg", ex.Message);
            }
            return Json(vsLogMessage);
        }
    }
}