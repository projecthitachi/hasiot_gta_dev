﻿using has.iot.Components;
using has.iot.Models;
using has.iot.Models.System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    [IsLoggedIn]
    public class LocationController : Controller
    {
        // GET: Location
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest]DataSourceRequest poRequest)
        {
            LocationModel oClass = new LocationModel();
            DatabaseContext oRemoteDB = new DatabaseContext();
            var vResult = oClass.GetList(oRemoteDB).ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest]DataSourceRequest poRequest, Location_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    LocationModel oClass = new LocationModel();
                    if (!oClass.Insert(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest]DataSourceRequest poRequest, Location_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    LocationModel oClass = new LocationModel();
                    if (!oClass.Update(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest poRequest, Location_REC poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    LocationModel oClass = new LocationModel();
                    if (!oClass.Delete(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        public ActionResult ReadDetail([DataSourceRequest]DataSourceRequest poRequest, long ParentRecordID)
        {
            LocationModel oClass = new LocationModel();
            DatabaseContext oRemoteDB = new DatabaseContext();
            var vResult = oClass.GetListDetail(oRemoteDB, ParentRecordID).ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateDetail([DataSourceRequest]DataSourceRequest poRequest, LocationDetail_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    LocationModel oClass = new LocationModel();
                    if (!oClass.InsertDetail(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateDetail([DataSourceRequest]DataSourceRequest poRequest, LocationDetail_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    LocationModel oClass = new LocationModel();
                    if (!oClass.UpdateDetail(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroyDetail([DataSourceRequest]DataSourceRequest poRequest, LocationDetail_REC poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    LocationModel oClass = new LocationModel();
                    if (!oClass.DeleteDetail(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        public ActionResult GetReaderList()
        {
            ImpinjModel oClass = new ImpinjModel();
            var vResult = oClass.GetList().ToList();
            return Json(vResult);
        }
        public ActionResult GetAntennaByReaderID(long ParentID)
        {
            ImpinjModel oClass = new ImpinjModel();
            DatabaseContext oRemoteDB = new DatabaseContext();
            var vResult = oClass.GetAntennaByParentID(oRemoteDB, ParentID).ToList();
            return Json(vResult);
        }
        public ActionResult GetLocationByFloor()
        {
            LocationModel oClass = new LocationModel();
            DatabaseContext oRemoteDB = new DatabaseContext();
            var vResult = oClass.GetLocationByFloor(oRemoteDB).ToList();
            return Json(vResult);
        }
    }
}