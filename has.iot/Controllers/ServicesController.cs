﻿using has.iot.Models.Services;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ServiceProcess;
using has.iot.Components;

namespace has.iot.Controllers
{
    [IsLoggedIn]
    public class ServicesController : Controller
    {
        // GET: Services
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ServicesGetList([DataSourceRequest]DataSourceRequest poRequest)
        {
            ServiceQuery oClass = new ServiceQuery();
            DatabaseContext oRemoteDB = new DatabaseContext();
            IList<Services> vResult = oClass.ServicesGetList(oRemoteDB).ToList();
            var jsonResult = Json(vResult.ToDataSourceResult(poRequest));
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult ServicesForm(long? RecordID)
        {
            if (RecordID != null)
            {
                ServiceQuery oClass = new ServiceQuery();
                DatabaseContext oRemoteDB = new DatabaseContext();
                Services voData = oClass.ServicesGetByRecordID(oRemoteDB, RecordID.Value).SingleOrDefault();
                ViewData["data"] = voData;
                return View("Edit");
            }
            else
                return View("Add");
        }
        public ActionResult GetCurrentStatus(string servicename)
        {
            string status = "";
            try
            {
                ServiceController sc = new ServiceController(servicename);
                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        status = "Running";
                        break;
                    case ServiceControllerStatus.Stopped:
                        status = "Stopped";
                        break;
                    case ServiceControllerStatus.Paused:
                        status = "Paused";
                        break;
                    case ServiceControllerStatus.StopPending:
                        status = "Stopping";
                        break;
                    case ServiceControllerStatus.StartPending:
                        status = "Starting";
                        break;
                    default:
                        status = "Status Changing";
                        break;
                }
            }
            catch(Exception ex)
            {
                status = ex.Message;
            }
            return Json(new { errorcode = 0, msg = status });
        }
        public ActionResult StartService(string servicename)
        {
            string status = "";
            try
            {
                ServiceController sc = new ServiceController(servicename);
                sc.Start();
                sc.WaitForStatus(ServiceControllerStatus.Running);
                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        status = "Running";
                        break;
                    case ServiceControllerStatus.Stopped:
                        status = "Stopped";
                        break;
                    case ServiceControllerStatus.Paused:
                        status = "Paused";
                        break;
                    case ServiceControllerStatus.StopPending:
                        status = "Stopping";
                        break;
                    case ServiceControllerStatus.StartPending:
                        status = "Starting";
                        break;
                    default:
                        status = "Status Changing";
                        break;
                }
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }
            return Json(new { errorcode = 0, msg = status });
        }
        public ActionResult RestartService(string servicename)
        {
            string status = "";
            try
            {
                ServiceController sc = new ServiceController(servicename);
                sc.Refresh();
                sc.WaitForStatus(ServiceControllerStatus.Running);
                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        status = "Running";
                        break;
                    case ServiceControllerStatus.Stopped:
                        status = "Stopped";
                        break;
                    case ServiceControllerStatus.Paused:
                        status = "Paused";
                        break;
                    case ServiceControllerStatus.StopPending:
                        status = "Stopping";
                        break;
                    case ServiceControllerStatus.StartPending:
                        status = "Starting";
                        break;
                    default:
                        status = "Status Changing";
                        break;
                }
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }
            return Json(new { errorcode = 0, msg = status });
        }
        public ActionResult StopService(string servicename)
        {
            string status = "";
            try
            {
                ServiceController sc = new ServiceController(servicename);
                sc.Stop();
                sc.WaitForStatus(ServiceControllerStatus.Stopped);
                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        status = "Running";
                        break;
                    case ServiceControllerStatus.Stopped:
                        status = "Stopped";
                        break;
                    case ServiceControllerStatus.Paused:
                        status = "Paused";
                        break;
                    case ServiceControllerStatus.StopPending:
                        status = "Stopping";
                        break;
                    case ServiceControllerStatus.StartPending:
                        status = "Starting";
                        break;
                    default:
                        status = "Status Changing";
                        break;
                }
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }
            return Json(new { errorcode = 0, msg = status });
        }

        public ActionResult Insert(Services poData)
        {
            Dictionary<string,string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    ServiceQuery oQuery = new ServiceQuery();
                    oQuery.Insert(oRemoteDB, poData);
                    oTransactions.Commit();

                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Insert " + poData.ServicesName + " Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }
            
            return Json(ResultData);
        }
        public ActionResult Update(Services poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    ServiceQuery oQuery = new ServiceQuery();
                    oQuery.Update(oRemoteDB, poData);
                    oTransactions.Commit();

                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Update " + poData.ServicesName + " Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }

            return Json(ResultData);
        }
        public ActionResult Delete(Services poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    ServiceQuery oQuery = new ServiceQuery();
                    oQuery.Delete(oRemoteDB, poData);
                    oTransactions.Commit();

                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Delete " + poData.ServicesName + " Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }

            return Json(ResultData);
        }
    }
}