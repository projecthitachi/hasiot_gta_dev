﻿using has.iot.Components;
using has.iot.Models;
using has.iot.Models.System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Driver;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt.Messages;
using static has.iot.Components.GlobalFunction;

namespace has.iot.Controllers
{
    [IsLoggedIn]
    public class ConfigurationController : Controller
    {
        private object insertDoc;

        public ActionResult Index()
        {
            var database = Constant.client.GetDatabase("IoT");
            var collection = database.GetCollection<ConfigurationModel>("Configuration");
            var filter = new BsonDocument() {};
            ConfigurationModel res = collection.Find(filter).Limit(1).SingleOrDefault();
            ViewData["Configuration"] = res;
            return View();
        }

        public ActionResult Update(ConfigurationModel poData, String status)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                var database = Constant.client.GetDatabase("IoT");
                var collection = database.GetCollection<ConfigurationModel>("Configuration");
                var filter = new BsonDocument() { };
                ConfigurationModel res = collection.Find(filter).Limit(1).SingleOrDefault();
                if (res == null)
                {
                    ResultData.Add("msg", "No Data");
                    ResultData.Add("errorcode", "0");
                }
                else
                {
                    if(status == "enableDisable")
                    {
                        if (poData.OutDoorTemperature != 0)
                        {
                            var update = Builders<ConfigurationModel>.Update.Set("OutDoorTemperatureStatus", poData.OutDoorTemperatureStatus);
                            collection.UpdateOne(filter, update);
                        }
                        else if (poData.ConstantLux != 0)
                        {
                            var update = Builders<ConfigurationModel>.Update.Set("ConstantLuxStatus", poData.ConstantLuxStatus);
                            collection.UpdateOne(filter, update);
                        }
                        else if (poData.SkinTemperature != 0)
                        {
                            var update = Builders<ConfigurationModel>.Update.Set("SkinTemperatureStatus", poData.SkinTemperatureStatus);
                            collection.UpdateOne(filter, update);
                        }
                        else if (poData.HeartRate != 0)
                        {
                            var update = Builders<ConfigurationModel>.Update.Set("HeartRateStatus", poData.HeartRateStatus);
                            collection.UpdateOne(filter, update);

                        }
                    }
                    else if(status == "update")
                    {
                        if (poData.OutDoorTemperature != 0)
                        {
                            var update = Builders<ConfigurationModel>.Update.Set("OutDoorTemperature", poData.OutDoorTemperature);
                            collection.UpdateOne(filter, update);
                        }
                        else if (poData.ConstantLux != 0)
                        {
                            var update = Builders<ConfigurationModel>.Update.Set("ConstantLux", poData.ConstantLux);
                            collection.UpdateOne(filter, update);
                        }
                        else if (poData.SkinTemperature != 0)
                        {
                            var update = Builders<ConfigurationModel>.Update.Set("SkinTemperature", poData.SkinTemperature);
                            collection.UpdateOne(filter, update);
                        }
                        else if (poData.HeartRate != 0)
                        {
                            var update = Builders<ConfigurationModel>.Update.Set("HeartRate", poData.HeartRate);
                            collection.UpdateOne(filter, update);

                        }

                    }
                    else if(status == "enableAll")
                    {
                        var update = Builders<ConfigurationModel>.Update.Set("OutDoorTemperatureStatus", true).Set("ConstantLuxStatus", true)
                                .Set("SkinTemperatureStatus", true).Set("HeartRateStatus", true);
                        collection.UpdateOne(filter, update);

                    }
                    else if (status == "disableAll")
                    {
                        var update = Builders<ConfigurationModel>.Update.Set("OutDoorTemperatureStatus", false).Set("ConstantLuxStatus", false)
                                .Set("SkinTemperatureStatus", false).Set("HeartRateStatus", false);
                        collection.UpdateOne(filter, update);

                    }
                    else if (status == "updateAll")
                    {
                        var update = Builders<ConfigurationModel>.Update.Set("OutDoorTemperature", poData.OutDoorTemperature)
                            .Set("ConstantLux", poData.ConstantLux)
                            .Set("SkinTemperature", poData.SkinTemperature)
                            .Set("HeartRate", poData.HeartRate);
                        collection.UpdateOne(filter, update);
                    }
                }
            }
            catch (Exception e)
            {
                ResultData.Add("msg", e.Message);
                ResultData.Add("errorcode", "100");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
    }
}