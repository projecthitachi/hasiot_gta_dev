﻿using has.iot.Components;
using has.iot.Models;
using has.iot.Models.System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    [IsLoggedIn]
    public class GeneralTablesController : Controller
    {
        // GET: GeneralTables
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Type()
        {
            return View();
        }
        public ActionResult GeneralTablesGetList([DataSourceRequest]DataSourceRequest poRequest)
        {
            GeneralTablesQuery oClass = new GeneralTablesQuery();
            DatabaseContext oRemoteDB = new DatabaseContext();
            IList<GeneralTables> vResult = oClass.GeneralTablesGetList(oRemoteDB).ToList();
            var jsonResult = Json(vResult.ToDataSourceResult(poRequest));
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult GeneralTypeGetList([DataSourceRequest]DataSourceRequest poRequest)
        {
            GeneralTablesQuery oClass = new GeneralTablesQuery();
            DatabaseContext oRemoteDB = new DatabaseContext();
            IList<GeneralType> vResult = oClass.GeneralTypeGetList(oRemoteDB).ToList();
            var jsonResult = Json(vResult.ToDataSourceResult(poRequest));
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult GeneralTypeGetListDDL()
        {
            GeneralTablesQuery oClass = new GeneralTablesQuery();
            DatabaseContext oRemoteDB = new DatabaseContext();
            IList<GeneralType> vResult = oClass.GeneralTypeGetList(oRemoteDB).ToList();
            var jsonResult = Json(vResult,JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult GeneralTablesGetListByType(string ListType)
        {
            GeneralTablesQuery oClass = new GeneralTablesQuery();
            DatabaseContext oRemoteDB = new DatabaseContext();
            IList<GeneralTables> vResult = oClass.GeneralTablesGetListByType(oRemoteDB, ListType).ToList();
            var jsonResult = Json(vResult);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest]DataSourceRequest poRequest, GeneralTables poRecord)
        {
            if ((poRecord != null))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    GeneralTablesQuery oClass = new GeneralTablesQuery();
                    if (!oClass.Insert(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest]DataSourceRequest poRequest, GeneralTables poRecord)
        {
            if ((poRecord != null))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    GeneralTablesQuery oClass = new GeneralTablesQuery();
                    if (!oClass.Update(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest poRequest, GeneralTables poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    GeneralTablesQuery oClass = new GeneralTablesQuery();
                    if (!oClass.Delete(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateType([DataSourceRequest]DataSourceRequest poRequest, GeneralType poRecord)
        {
            if ((poRecord != null))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    GeneralTablesQuery oClass = new GeneralTablesQuery();
                    if (!oClass.InsertType(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateType([DataSourceRequest]DataSourceRequest poRequest, GeneralType poRecord)
        {
            if ((poRecord != null))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    GeneralTablesQuery oClass = new GeneralTablesQuery();
                    if (!oClass.UpdateType(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroyType([DataSourceRequest]DataSourceRequest poRequest, GeneralType poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    GeneralTablesQuery oClass = new GeneralTablesQuery();
                    if (!oClass.DeleteType(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
    }
}