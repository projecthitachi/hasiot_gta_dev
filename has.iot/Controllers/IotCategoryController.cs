﻿using has.iot.Components;
using has.iot.Models;
using has.iot.Models.System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    [IsLoggedIn]
    public class IotCategoryController : Controller
    {
        public ActionResult Index()
        {
            Category oClass = new Category();
            var vResult = oClass.GetList().ToList();
            ViewData["Data"] = vResult;
            return View();
        }
        public ActionResult Brands(string Category)
        {
            Brands oClass = new Brands();
            var vResult = oClass.GetList(Category).ToList();
            ViewData["Data"] = vResult;
            return View();
        }
        public ActionResult GetBrands()
        {
            Brands oClass = new Brands();
            var vResult = oClass.GetListBrand()
                .GroupBy(w => w.CategoryID)
                .Select(grp => grp.Take(1).SingleOrDefault()).ToList();
            return Json(vResult,JsonRequestBehavior.AllowGet);
        }
        public ActionResult RemovePlugin(string ID)
        {
            Dictionary<string, string> vsLogMessage = new Dictionary<string, string>();
            try
            {
                Category oClass = new Category();
                Category_REC voPlugin = new Category_REC();
                voPlugin.CategoryID = ID.Replace(" ", String.Empty);
                oClass.Delete(voPlugin);

                // Check if file exists with its full path    
                string targetPathDLL = Server.MapPath("~/bin/" + voPlugin.CategoryID + ".dll");
                string targetPathDLLConf = Server.MapPath("~/bin/" + voPlugin.CategoryID + ".dll.config");
                if (System.IO.File.Exists(targetPathDLL))
                {
                    System.IO.File.Delete(targetPathDLL);
                }
                if (System.IO.File.Exists(targetPathDLLConf))
                {
                    System.IO.File.Delete(targetPathDLLConf);
                }

                vsLogMessage.Add("errorcode", "0");
                vsLogMessage.Add("title", "Success");
                vsLogMessage.Add("msg", "Plugin Removed");
            }
            catch (Exception ex)
            {
                vsLogMessage.Add("errorcode", "100");
                vsLogMessage.Add("title", "warning");
                vsLogMessage.Add("msg", ex.Message);
            }
            return Json(vsLogMessage, JsonRequestBehavior.AllowGet);
        }

    }
}