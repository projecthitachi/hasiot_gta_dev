﻿using has.iot.Components;
using has.iot.Models;
using has.iot.Models.System;
using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    [IsLoggedIn]
    public class ProfileController : Controller
    {
        // GET: Profile
        public ActionResult Index()
        {
            UsersData voData = GlobalFunction.GetLoginData(HttpContext.Session["userdata"].ToString());
            ViewData["data"] = voData;
            return View();
        }
    }
}