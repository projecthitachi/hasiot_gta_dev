﻿using has.iot.Components;
using has.iot.Models;
using has.iot.Models.System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    [IsLoggedIn]
    public class EventsLogController : Controller
    {
        // GET: EventsLog
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EventsLogGetList([DataSourceRequest]DataSourceRequest poRequest)
        {
            EventsLogQuery oClass = new EventsLogQuery();
            DatabaseContext oRemoteDB = new DatabaseContext();
            IList<EventsLog> vResult = oClass.EventsLogGetList(oRemoteDB).ToList();
            var jsonResult = Json(vResult.ToDataSourceResult(poRequest));
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}