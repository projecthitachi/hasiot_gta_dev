﻿using has.iot.Components;
using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    public class UploadController : Controller
    {
        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadFile(IEnumerable<HttpPostedFileBase> files)
        {
            Dictionary<string, string> vsLogMessage = new Dictionary<string, string>();
            try
            {
                var DirUpload = Server.MapPath("~/Components/Plugins/");
                string PluginName = "";

                if (files != null)
                {
                    // Process Extract File
                    foreach (var file in files)
                    {
                        using (ZipArchive archive = new ZipArchive(file.InputStream))
                        {
                            foreach (ZipArchiveEntry entry in archive.Entries)
                            {
                                if (!string.IsNullOrEmpty(Path.GetExtension(entry.FullName)))
                                {
                                    entry.ExtractToFile(Path.Combine(DirUpload, entry.FullName));
                                }
                                else
                                {
                                    Directory.CreateDirectory(Path.Combine(DirUpload, entry.FullName));
                                }
                            }
                        }
                        PluginName = file.FileName.Replace(".zip","");
                    }

                    string sourcePathSQL = Server.MapPath("~/Components/Plugins/"+ PluginName + "/data.sql");
                    string sourcePathDLL = Server.MapPath("~/Components/Plugins/"+ PluginName + "/dll");
                    string targetPathDLL = Server.MapPath("~/bin/");
                    string sourcePathView = Server.MapPath("~/Components/Plugins/" + PluginName + "/views");
                    string targetPathView = Server.MapPath("~/Areas/" + PluginName);

                    if (!Directory.Exists(targetPathDLL))
                    {
                        Directory.CreateDirectory(targetPathDLL);
                    }

                    if (!Directory.Exists(targetPathView))
                    {
                        Directory.CreateDirectory(targetPathView);
                    }
                    else
                    {
                        // Process Remove Old Plugin 
                       Directory.Delete(targetPathView, true);
                       Directory.CreateDirectory(targetPathView);
                    }

                    // Process move dll.
                    string[] sourcefilesDLL = Directory.GetFiles(sourcePathDLL);
                    foreach (string sourcefile in sourcefilesDLL)
                    {
                        string fileName = Path.GetFileName(sourcefile); 
                        string destFile = Path.Combine(targetPathDLL, fileName);

                        if (System.IO.File.Exists(destFile))
                        {
                            System.IO.File.Delete(destFile);
                            System.IO.File.Move(sourcefile, destFile);
                        }
                        else
                        {
                            System.IO.File.Move(sourcefile, destFile);
                        }
                    }

                    // Process move view.
                    string[] sourcefilesView = Directory.GetFiles(sourcePathView);
                    foreach (string sourcefile in sourcefilesView)
                    {
                        string fileName = Path.GetFileName(sourcefile);
                        string destFile = Path.Combine(targetPathView, fileName);
                        System.IO.File.Move(sourcefile, destFile);
                    }

                    // Process move view subdirectories.
                    string[] dirs = Directory.GetDirectories(sourcePathView);
                    foreach (string dir in dirs)
                    {
                        string fileName = Path.GetFileName(dir);
                        string destinationDir = Path.Combine(targetPathView, fileName);
                        System.IO.Directory.Move(dir, destinationDir);
                    }

                    // Process read sql file
                    string script = System.IO.File.ReadAllText(sourcePathSQL);
                    Category oClass = new Category();
                    oClass.Insert(script);

                    // scan assembly
                    //ICollection<IPlugin> plugins = GenericPluginLoader<IPlugin>.LoadPlugins("\\Components\\Plugins\\pools");
                    //foreach (var item in plugins)
                    //{
                    //    if (!GlobalFunction._Plugins.ContainsKey(item.Name.Replace(" ", String.Empty)))
                    //    {
                    //        GlobalFunction._Plugins.Add(item.Name.Replace(" ", String.Empty), item);
                    //    }
                    //}

                    // Process Remove Plugin extract Files
                    System.IO.Directory.Delete(Server.MapPath("~/Components/Plugins/" + PluginName), true);
                }
                vsLogMessage.Add("errorcode", "0");
                vsLogMessage.Add("title", "Success");
                vsLogMessage.Add("msg", "success");
            }
            catch (Exception ex)
            {
                vsLogMessage.Add("errorcode", "100");
                vsLogMessage.Add("title", "warning");
                vsLogMessage.Add("msg", ex.Message);
            }
            return Json(vsLogMessage, JsonRequestBehavior.AllowGet);
        }
    }
}