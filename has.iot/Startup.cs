﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;
using Microsoft.Owin;
using has.iot.Controllers;
using has.iot.Components;

[assembly: OwinStartup(typeof(has.iot.Startup))]

namespace has.iot
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //HomeController.Receive();
            app.MapSignalR();
        }
    }
}