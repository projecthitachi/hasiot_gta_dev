﻿using has.iot.Components;
using Sensor.Lord.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace MVCPluggableDemo
{
    public class SensorAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SensorLord";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SensorLord_default",
                "SensorLord/{controller}/{action}/{id}",
                new {controller= "Lord", action = "Index", id = UrlParameter.Optional },
                new string[] { "Sensor.Lord.Controllers" }
            );
            Constants.MqttClient = new MqttClient(GlobalFunction.mqttServer);
            Constants.MqttClient.Subscribe(new string[] { "Sensor/Lord/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            Constants.MqttClient.MqttMsgPublishReceived += Sensor.Lord.Controllers.LordController.Lord_MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            Constants.MqttClient.Connect(clientId);
        }
    }
}