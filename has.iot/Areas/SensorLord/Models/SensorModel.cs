﻿using has.iot.Components;
using Kendo.Mvc;
using Kendo.Mvc.Export;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Sensor.Lord.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sensor.Lord.Models
{
    public class Lord_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string BaseStation { get; set; }
        public string NodeName { get; set; }
        public string Chanel { get; set; }
        public string StatusDevice { get; set; }
        public int Live { get; set; }
        public string LocationID { get; set; }
    }
    public class SensorDatalogs_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string Node { get; set; }
        public string Chanel { get; set; }
        public string PresentValue { get; set; }
    }
    public class LordDataLog_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [Display(Name = "RecordID")]
        public long RecordID { get; set; }

        [Display(Name = "RecordTimestamp")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }

        [Display(Name = "Record Status")]
        public Int32 RecordStatus { get; set; }

        [Display(Name = "BaseStation ID")]
        public string BaseStationID { get; set; }

        [Display(Name = "Node ID")]
        public string NodeID { get; set; }

        [Display(Name = "Chanel")]
        public string Chanel { get; set; }
        [Display(Name = "Payload")]
        public string Payload { get; set; }
        [Display(Name = "Value")]
        public string Value { get; set; }

        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string Node { get; set; }
        public string PresentValue { get; set; }
        public string Units { get; set; }
    }
    public class CountData
    {
        public int count { get; set; }
    }
    public class ParameterExport
    {
        public IList<ExportColumnSettings> columns { get; set; }
        public ParameterExportOptions options { get; set; }
        public FilterDate transport { get; set; }
    }
    public class ParameterExportOptions
    {
        public string format { get; set; }
        public string title { get; set; }
    }
    public class FilterDate
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
    public class LordModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public LordModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t70309fieldselect"
        private string t70309fieldselect = @"
            t70309r001 as RecordID,
            t70309r002 as RecordTimestamp,
            t70309r003 as RecordStatus,
            t70309f001 as BaseStationID,
            t70309f002 as NodeID,
            t70309f003 as Payload,
            t70309f004 as Chanel,
            t70309f005 as Value";
        #endregion
        public DbRawSqlQuery<CountData> GetCountData(string table)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT COUNT(*) as count from " + table;
            var vQuery = oRemoteDB.Database.SqlQuery<CountData>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<LordDataLog_REC> GetListDataLogReport()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70309fieldselect + " FROM t70309 ORDER BY t70309r001 DESC";

            var vQuery = oRemoteDB.Database.SqlQuery<LordDataLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<LordDataLog_REC> GetListDataLog([DataSourceRequest]DataSourceRequest poRequest)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            int start = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int end = poRequest.Page * poRequest.PageSize;

            string WhereClause = "";
            foreach (FilterDescriptor filter in poRequest.Filters)
            {
                WhereClause += filter.Member + " LIKE '%" + filter.Value + "%' AND ";
            }

            string Offset = "";
            string Limit = "";
            if (WhereClause.Length == 0)
            {
                Offset = "([table].RowNum BETWEEN " + start + " AND " + end + ")";
                Limit = "TOP(" + poRequest.PageSize + ")";
            }
            else
                WhereClause = WhereClause.Remove(WhereClause.LastIndexOf("AND"), 3);

            string sSQL = "SELECT " + Limit + "* " +
                "FROM(SELECT " + t70309fieldselect + ", ROW_NUMBER() OVER(ORDER BY t70309r001 DESC) AS RowNum " +
                "FROM t70309) AS [table] " +
                "WHERE " + WhereClause + Offset;

            var vQuery = oRemoteDB.Database.SqlQuery<LordDataLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<LordDataLog_REC> GetListDataLogByChanel([DataSourceRequest]DataSourceRequest poRequest, string BaseStation, string NodeName, string Chanel)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            int start = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int end = poRequest.Page * poRequest.PageSize;

            string WhereClause = "";
            foreach (FilterDescriptor filter in poRequest.Filters)
            {
                WhereClause += filter.Member + " LIKE '%" + filter.Value + "%' AND ";
            }

            string Offset = "";
            string Limit = "";
            if (WhereClause.Length == 0)
            {
                Offset = "([table].RowNum BETWEEN " + start + " AND " + end + ")";
                Limit = "TOP(" + poRequest.PageSize + ")";
            }
            else
                WhereClause = WhereClause.Remove(WhereClause.LastIndexOf("AND"), 3);

            string sSQL = "SELECT " + Limit + "* " +
                "FROM(SELECT " + t70309fieldselect + ", ROW_NUMBER() OVER(ORDER BY t70309r001 DESC) AS RowNum " +
                "FROM t70309 WHERE t70309f001 = '" + BaseStation + "' AND t70309f002 = '" + NodeName + "' AND t70309f004 = '" + Chanel + "') AS [table] " +
                "WHERE  " + WhereClause + Offset;

            var vQuery = oRemoteDB.Database.SqlQuery<LordDataLog_REC>(sSQL);
            return vQuery;
        }
        public bool DataLogInsert(DatabaseContext oRemoteDB, LordDataLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70309 " +
                    "      ( " +
                    "      t70309r002, t70309r003, " +
                    "      t70309f001, t70309f002, t70309f003, t70309f004, t70309f005" +
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70309r002, @pt70309r003, " +
                    "      @pt70309f001, @pt70309f002, @pt70309f003, @pt70309f004, @pt70309f005" +
                    "      )" +
                    ";" +
                    "SELECT @pt70309r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70309r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70309r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70309r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70309f001", poRecord.BaseStationID));
                oParameters.Add(new SqlParameter("@pt70309f002", poRecord.NodeID));
                oParameters.Add(new SqlParameter("@pt70309f003", poRecord.Payload));
                oParameters.Add(new SqlParameter("@pt70309f004", poRecord.Chanel));
                oParameters.Add(new SqlParameter("@pt70309f005", poRecord.Value));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        public DbRawSqlQuery<Lord_REC> GetSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT tb.t70200f005 AS DeviceID FROM t70100 ta JOIN t70200 tb ON ta.t70100f001 = tb.t70200f001 WHERE ta.t70100f001 = 'Sensor';";

            var vQuery = oRemoteDB.Database.SqlQuery<Lord_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Lord_REC> GetLastSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT TOP 1 t70209f001 AS DeviceID FROM t70209 ORDER BY t70209r001 DESC;";

            var vQuery = oRemoteDB.Database.SqlQuery<Lord_REC>(sSQL);

            return vQuery;
        }
        #region
        private string t70209fieldselect = @"
            t70209r001 AS RecordID, 
            t70209r002 AS RecordTimestamp, 
            t70209r003 AS RecordStatus, 
            t70209f001 AS DeviceID, 
            t70209f002 AS BaseStation, 
            t70209f003 AS NodeName, 
            t70209f004 AS Chanel, 
            t70209f005 AS StatusDevice,  
            t70209f006 AS Live,
            t70209f007 AS LocationID";
        #endregion
        
        public DbRawSqlQuery<Lord_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT "+t70209fieldselect+" FROM t70209 " +
                "ORDER BY t70209r001 DESC;";

            var vQuery = oRemoteDB.Database.SqlQuery<Lord_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Lord_REC> GetOne(string BaseStation, string Node, string Chanel)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT t70209f001 AS DeviceID, t70209f005 AS StatusDevice, t70209f006 AS Live FROM t70209 " +
                "WHERE t70209f002 = '" + BaseStation + "' AND t70209f003 = '" + Node + "' AND t70209f004 = '" + Chanel + "' ;";

            var vQuery = oRemoteDB.Database.SqlQuery<Lord_REC>(sSQL);

            return vQuery;
        }
        public bool Insert(DatabaseContext oRemoteDB, Lord_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;
                if (GetLastSerialNo().ToList().Count == 0)
                {
                    poRecord.DeviceID = GetSerialNo().SingleOrDefault().DeviceID + ".0000.0001";
                }
                else
                {
                    string[] serialNo = GetLastSerialNo().SingleOrDefault().DeviceID.Split('.');
                    int lastNo = Convert.ToInt32(serialNo[3]) + 1;
                    poRecord.DeviceID = serialNo[0] + "." + serialNo[1] + "." + serialNo[2] + "." + lastNo.ToString("D4");

                }

                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70209 " +
                    "      ( " +
                    "      t70209r002, t70209r003, " +
                    "      t70209f001, t70209f002, t70209f003, t70209f004, t70209f005, t70209f006 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70209r002, @pt70209r003, " +
                    "      @pt70209f001, @pt70209f002, @pt70209f003, @pt70209f004, @pt70209f005, @pt70209f006 " +
                    "      )" +
                    ";" +
                    "SELECT @pt70209r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70209r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70209r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70209r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70209f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70209f002", poRecord.BaseStation));
                oParameters.Add(new SqlParameter("@pt70209f003", poRecord.NodeName));
                oParameters.Add(new SqlParameter("@pt70209f004", poRecord.Chanel));
                oParameters.Add(new SqlParameter("@pt70209f005", poRecord.StatusDevice));
                oParameters.Add(new SqlParameter("@pt70209f006", poRecord.Live));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, Lord_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70209 " +
                "   SET " +
                "      t70209f002 = @pt70209f002, " +
                "      t70209f003 = @pt70209f003, " +
                "      t70209f004 = @pt70209f004, " +
                "      t70209f005 = @pt70209f005, " +
                "      t70209f006 = @pt70209f006 " +
                "   WHERE (t70209f001 = @pt70209f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70209f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70209f002", poRecord.BaseStation));
            oParameters.Add(new SqlParameter("@pt70209f003", poRecord.NodeName));
            oParameters.Add(new SqlParameter("@pt70209f004", poRecord.Chanel));
            oParameters.Add(new SqlParameter("@pt70209f005", poRecord.StatusDevice));
            oParameters.Add(new SqlParameter("@pt70209f006", poRecord.Live));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateStatus(DatabaseContext oRemoteDB, Lord_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            poRecord.RecordTimestamp = DateTime.Now;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70209 " +
                "   SET " +
                "      t70209f005 = @pt70209f005 " +
                "   WHERE (t70209f001 = @pt70209f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70209f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70209f005", poRecord.StatusDevice));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, Lord_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t70209 " +
                "   WHERE (t70209f001 = @pt70209f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70209f001", poRecord.DeviceID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
    }
}