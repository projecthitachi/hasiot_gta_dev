﻿using has.iot.Components;
using Sensor.Lord.Models.System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sensor.Lord.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using uPLibrary.Networking.M2Mqtt.Messages;
using Kendo.Mvc.Export;
using Telerik.Documents.SpreadsheetStreaming;
using System.IO;
using System.Text;
using Sensor.Lord.Components;
using Kendo.Mvc;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using MongoDB.Driver;

namespace Sensor.Lord.Controllers
{
    public class LordController : Controller
    {
        // GET: Lord
        public ActionResult Index()
        {
            has.iot.Models.Brands oModel = new has.iot.Models.Brands();
            IList<has.iot.Models.Brands_REC> oTable = oModel.GetList("Lord").ToList();
            ViewData["DataList"] = oTable;
            return View();
        }
        public ActionResult ConfigService()
        {
            return View();
        }
        public ActionResult Read([DataSourceRequest]DataSourceRequest poRequest)
        {
            LordModel oClass = new LordModel();
            var vResult = oClass.GetList().ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest]DataSourceRequest poRequest, Lord_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    LordModel oClass = new LordModel();
                    if (!oClass.Insert(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest]DataSourceRequest poRequest, Lord_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    LordModel oClass = new LordModel();
                    if (!oClass.Update(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest poRequest, Lord_REC poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    LordModel oClass = new LordModel();
                    if (!oClass.Delete(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        public ActionResult Command(string cmd, string value)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();

            if (value != "All")
            {
                string strValue = Convert.ToString(cmd);
                Constants.MqttClient.Publish("Sensor/Lord/" + value + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            else
            {
                LordModel oClass = new LordModel();
                var vResult = oClass.GetList().ToList();
                foreach (Lord_REC oData in vResult)
                {
                    string strValue = Convert.ToString(cmd);
                    Constants.MqttClient.Publish("Sensor/Lord/" + oData.DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Commands(string cmd, string IPAddress, string Port, string MQTTHOST, string Interval)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();

            string strValue = "";
            if (cmd == "GetConfig") { strValue = "{}"; }
            else if (cmd == "SetConfig")
            {
                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("IPAddress", IPAddress);
                voResData.Add("Port", Port);
                voResData.Add("MQTTHOST", MQTTHOST);
                voResData.Add("Interval", Interval);

                strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            }
            Constants.MqttClient.Publish("Sensor/Lord/" + cmd, Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public static void Lord_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            string[] topic = e.Topic.Split('/');
            string Category = topic[0];
            if (topic.Length > 3)
            {
                var message = System.Text.Encoding.Default.GetString(e.Message);
                if (GlobalFunction.IsValidJson(message))
                {
                    var RT = GlobalHost.ConnectionManager.GetHubContext<RealTimeHub>();
                    object result = JsonConvert.DeserializeObject(message);
                    JObject voobj = JObject.Parse(result.ToString());

                    string Brand = topic[1];
                    string hostDevice = topic[2];
                    string cmd = topic[3];
                    if (cmd == "Datas")
                    {
                        DatabaseContext oRemoteDB = new DatabaseContext();
                        LordModel oQuery = new LordModel();
                        string[] hsn = hostDevice.Split(':');

                        for (int i = 0; i < voobj.Count; i++)
                        {
                            var vResult = oQuery.GetOne(hsn[0], hsn[1], voobj["" + i + ""]["ChanelName"].ToString()).SingleOrDefault();
                            if (vResult != null) { 
                                if (vResult.Live == 1)
                                {
                                    LordDataLog_REC poRecord = new LordDataLog_REC();
                                    poRecord.RecordTimestamp = DateTime.Now;
                                    poRecord.BaseStationID = hsn[0];
                                    poRecord.NodeID = hsn[1];
                                    poRecord.Chanel = voobj["" + i + ""]["ChanelName"].ToString();
                                    poRecord.Value = voobj["" + i + ""]["Value"].ToString();
                                    poRecord.Payload = message;
                                    oQuery.DataLogInsert(oRemoteDB, poRecord);

                                    if (vResult.StatusDevice != "Conneted")
                                    {
                                        // begin set status
                                        Lord_REC oRecord = new Lord_REC();
                                        oRecord.DeviceID = vResult.DeviceID;
                                        oRecord.StatusDevice = "Conneted";
                                        oQuery.UpdateStatus(oRemoteDB, oRecord);
                                        var json = new JavaScriptSerializer().Serialize(oRecord);
                                        RT.Clients.All.Lord("Device", json);
                                    }
                                }
                            }
                        }
                        RT.Clients.All.Lord("DataLog", message);

                    }
                    else
                    if (cmd == "setConfigData")
                    {
                        RT.Clients.All.Lord("SetConfig", message);
                    }
                    else
                    if (cmd == "getConfigData")
                    {
                        RT.Clients.All.Lord("GetConfig", message);
                    }
                }
            }
        }
        public ActionResult DataLog()
        {
            return View();
        }
        public ActionResult GetListDataLog([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            //ElevatorModel oModel = new ElevatorModel();
            //IList<ElevatorDataLog_REC> oTable = oModel.GetListDataLog(DeviceID).ToList();

            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<SensorDatalogs_REC> Collections = database.GetCollection<SensorDatalogs_REC>("SensorDataLogs");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<SensorDatalogs_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                    { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<SensorDatalogs_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable;
            ds.Total = Convert.ToInt32(Collections.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public IEnumerable<LordDataLog_REC> GetListDataLogReport([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            /*ElevatorModel oClass = new ElevatorModel();
            IEnumerable<ElevatorDataLog_REC> vResult = oClass.GetListDataLog().ToList();
            return vResult;*/
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<LordDataLog_REC> Collections = database.GetCollection<LordDataLog_REC>("SensorDataLogs");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<LordDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();
            if (filter_date != null)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) },
                    { "$lt", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            IEnumerable<LordDataLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).ToList();
            return oTable;
        }
        [HttpPost]
        public ActionResult ExportServer([DataSourceRequest]DataSourceRequest poRequest, ParameterExport data)
        {
            var columnsData = data.columns.ToArray();
            SpreadDocumentFormat exportFormat = data.options.format.ToString() == "csv" ? exportFormat = SpreadDocumentFormat.Csv : exportFormat = SpreadDocumentFormat.Xlsx;
            Action<ExportCellStyle> cellStyle = new Action<ExportCellStyle>(ChangeCellStyle);
            Action<ExportRowStyle> rowStyle = new Action<ExportRowStyle>(ChangeRowStyle);
            Action<ExportColumnStyle> columnStyle = new Action<ExportColumnStyle>(ChangeColumnStyle);

            string fileName = string.Format("{0}.{1}", data.options.title, data.options.format);
            string mimeType = Helpers.GetMimeType(exportFormat);

            Stream exportStream = exportFormat == SpreadDocumentFormat.Xlsx ?
                GetListDataLogReport(poRequest, data.transport).ToXlsxStream(columnsData) :
                GetListDataLogReport(poRequest, data.transport).ToCsvStream(columnsData);

            var fileStreamResult = new FileStreamResult(exportStream, mimeType);
            fileStreamResult.FileDownloadName = fileName;
            fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);
            using (var fileStream = System.IO.File.Create(Server.MapPath("~" + Constants.ExportDirectory + fileName)))
            {
                fileStreamResult.FileStream.CopyTo(fileStream);
            }

            return Json(new { filename = fileName });
        }
        private void ChangeCellStyle(ExportCellStyle e)
        {
            bool isHeader = e.Row == 0;
            SpreadBorder border = new SpreadBorder(SpreadBorderStyle.Thick, new SpreadThemableColor(new SpreadColor(0, 0, 0)));
            SpreadCellFormat format = new SpreadCellFormat
            {
                FontSize = 11,
                ForeColor = new SpreadThemableColor(new SpreadColor(0, 0, 0)),
                WrapText = false
            };
            e.Cell.SetFormat(format);
        }
        private void ChangeRowStyle(ExportRowStyle e)
        {
            e.Row.SetHeightInPixels(e.Index == 0 ? 80 : 30);
        }
        private void ChangeColumnStyle(ExportColumnStyle e)
        {
            double width = e.Name == "Item ID" || e.Name == "Barcode" ? 200 : 100;
            e.Column.SetWidthInPixels(width);
        }
    }
}