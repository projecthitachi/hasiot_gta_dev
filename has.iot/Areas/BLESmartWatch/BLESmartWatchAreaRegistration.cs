﻿using has.iot.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
namespace MVCPluggableDemo
{
    public class BLESmartWatchAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BLESmartWatch";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "BLESmartWatch_default",
                "BLESmartWatch/{controller}/{action}/{id}",
                new {controller= "SmartWatch", action = "Index", id = UrlParameter.Optional },
                new string[] { "BLE.SmartWatch.Controllers" }
            );        
        }
    }
}