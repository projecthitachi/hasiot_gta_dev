﻿using Power.Raspberry.Components;
using has.iot.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace MVCPluggableDemo
{
    public class PowerRaspberryAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PowerRaspberry";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PowerRaspberry_default",
                "PowerRaspberry/{controller}/{action}/{id}",
                new {controller= "Raspberry", action = "Index", id = UrlParameter.Optional },
                new string[] { "Power.Raspberry.Controllers" }
            );
            Constants.MqttClient = new MqttClient(GlobalFunction.mqttServer);
            Constants.MqttClient.Subscribe(new string[] { "Power/Raspberry/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            Constants.MqttClient.MqttMsgPublishReceived += Power.Raspberry.Controllers.RaspberryController.Raspberry_MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            Constants.MqttClient.Connect(clientId);
        }
    }
}