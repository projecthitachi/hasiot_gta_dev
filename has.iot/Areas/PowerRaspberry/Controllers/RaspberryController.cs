﻿using has.iot.Components;
using Power.Raspberry.Models.System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Power.Raspberry.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using uPLibrary.Networking.M2Mqtt.Messages;
using Kendo.Mvc.Export;
using Telerik.Documents.SpreadsheetStreaming;
using System.IO;
using System.Text;
using Power.Raspberry.Components;
using MongoDB.Driver;
using MongoDB.Bson;
using Kendo.Mvc;
using System.Text.RegularExpressions;

namespace Power.Raspberry.Controllers
{
    public class RaspberryController : Controller
    {
        // GET: Raspberry
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ConfigService()
        {
            return View();
        }
        public ActionResult Whois()
        {
            var categories = new List<CategoryView_REC>() {
                   new CategoryView_REC() {
                      CategoryName = "Read Only",
                      CategoryID = 1
                  },
                  new CategoryView_REC() {
                      CategoryName = "Write Only",
                      CategoryID = 2
                  },
                  new CategoryView_REC() {
                      CategoryName = "Both",
                      CategoryID = 3
                  }
            };

            ViewData["categories"] = categories;
            ViewData["defaultCategory"] = categories.First();
            return View();
        }

        public ActionResult GetLokkuplist(string Type, string text)
        {
            RaspberryModel oClass = new RaspberryModel();
            var voTable = oClass.GetForLookup(Type, text);
            return Json(voTable, JsonRequestBehavior.AllowGet);
        }

        #region CRUD
        public ActionResult Read([DataSourceRequest]DataSourceRequest poRequest)
        {
            RaspberryModel oClass = new RaspberryModel();
            var vResult = oClass.GetList().ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest]DataSourceRequest poRequest, Raspberry_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    RaspberryModel oClass = new RaspberryModel();
                    if (!oClass.Insert(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest]DataSourceRequest poRequest, Raspberry_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    RaspberryModel oClass = new RaspberryModel();
                    if (!oClass.Update(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest poRequest, Raspberry_REC poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    RaspberryModel oClass = new RaspberryModel();
                    if (!oClass.Delete(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        #endregion

        #region object instance
        public ActionResult ObjectRead([DataSourceRequest]DataSourceRequest poRequest, string DeviceID)
        {
            var categories = new List<CategoryView_REC>() {
                   new CategoryView_REC() {
                      CategoryName = "Read Only",
                      CategoryID = 1
                  },
                  new CategoryView_REC() {
                      CategoryName = "Write Only",
                      CategoryID = 2
                  },
                  new CategoryView_REC() {
                      CategoryName = "Both",
                      CategoryID = 3
                  }
            };

            RaspberryModel oClass = new RaspberryModel();
            var vResult = oClass.GetListDataObject(DeviceID).ToList();
            List<RaspberryObject_REC> ListObjt = new List<RaspberryObject_REC>();
            foreach (RaspberryObject_REC objData in vResult)
            {
                objData.Category = categories.Find(f => f.CategoryID == objData.Type);
                ListObjt.Add(objData);
            }
            return Json(ListObjt.ToDataSourceResult(poRequest));
        }
        public ActionResult ObjectReadDLL(string DeviceID, string Type, string text)
        {
            var categories = new List<CategoryView_REC>() {
                   new CategoryView_REC() {
                      CategoryName = "Read Only",
                      CategoryID = 1
                  },
                  new CategoryView_REC() {
                      CategoryName = "Write Only",
                      CategoryID = 2
                  },
                  new CategoryView_REC() {
                      CategoryName = "Both",
                      CategoryID = 3
                  }
            };

            RaspberryModel oClass = new RaspberryModel();
            var vResult = oClass.GetListDLLObject(DeviceID, Type, text).ToList();
            List<RaspberryObject_REC> ListObjt = new List<RaspberryObject_REC>();
            foreach (RaspberryObject_REC objData in vResult)
            {
                objData.Category = categories.Find(f => f.CategoryID == objData.Type);
                objData.ObjectName = objData.ObjectName + ":" + objData.Instance;
                ListObjt.Add(objData);
            }

            return Json(ListObjt, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ObjectUpdate([DataSourceRequest]DataSourceRequest poRequest, [Bind(Prefix = "models")]IEnumerable<RaspberryObject_REC> poRecord)
        {
            Dictionary<string, string> vsLogMessage = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            var oTransaction = oRemoteDB.Database.BeginTransaction();
            try
            {
                RaspberryModel oClass = new RaspberryModel();
                foreach (RaspberryObject_REC voObj in poRecord)
                {
                    voObj.Type = voObj.Category.CategoryID;
                    if (!oClass.ObjectUpdate(oRemoteDB, voObj))
                    {
                        throw new Exception(oClass.ErrorMessage);
                    }
                }
                oTransaction.Commit();


                vsLogMessage.Add("errorcode", "0");
                vsLogMessage.Add("title", "Success");
                vsLogMessage.Add("msg", "Insert Data seccess");
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                vsLogMessage.Add("errorcode", "100");
                vsLogMessage.Add("title", "warning");
                vsLogMessage.Add("msg", ex.Message);
            }

            return Json(vsLogMessage, JsonRequestBehavior.DenyGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateStatusObj(string[] RecordID, string[] voObject, string DeviceID)
        {
            Dictionary<string, string> vsLogMessage = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            var oTransaction = oRemoteDB.Database.BeginTransaction();
            try
            {
                RaspberryModel oClass = new RaspberryModel();
                string voRecordID = (RecordID !=null ) ? string.Join(", ", RecordID) : null;
                if (!oClass.updateStatusObj(oRemoteDB, voRecordID, DeviceID))
                {
                    throw new Exception(oClass.ErrorMessage);
                }
                oTransaction.Commit();

                // begin write to json file
                string targetPathView = Server.MapPath("~/Components/Plugins/Power/");
                if (!Directory.Exists(targetPathView))
                {
                    Directory.CreateDirectory(targetPathView);
                }
                else
                {
                    string strValue = Convert.ToString(JsonConvert.SerializeObject(voObject));
                    System.IO.File.WriteAllText(Server.MapPath("~/Components/Plugins/Power/" + DeviceID + ".json"), strValue);
                }
                
                // end wriet to json file

                vsLogMessage.Add("errorcode", "0");
                vsLogMessage.Add("title", "Success");
                vsLogMessage.Add("msg", "Insert Data seccess");
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                vsLogMessage.Add("errorcode", "100");
                vsLogMessage.Add("title", "warning");
                vsLogMessage.Add("msg", ex.Message);
            }

            return Json(vsLogMessage, JsonRequestBehavior.DenyGet);
        }
        #endregion

        #region MQTT
        public ActionResult Command(string cmd, string value)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();

            if (value != "All")
            {
                string strValue = "{'deviceID':'" + value + "'}";
                Constants.MqttClient.Publish("Power/Raspberry/Whois", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            else
            {
                RaspberryModel oClass = new RaspberryModel();
                var vResult = oClass.GetList().ToList();
                foreach (Raspberry_REC oData in vResult)
                {
                    string strValue = Convert.ToString(cmd);
                    Constants.MqttClient.Publish("Power/Raspberry/" + oData.DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Commands(string cmd,string DeviceID, string ObjType, string value)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();

            string strValue = "{'deviceID':'" + DeviceID + "','ObjectType':'"+ObjType+"','value':'"+value+"'}";
            if (cmd == "GetConfig") { strValue = "{}"; }else if (cmd == "SetConfig")
            {
                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("MQTTHOST", DeviceID);
                voResData.Add("PatchFile", ObjType);
                voResData.Add("Interval", value);

                strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            }
            Constants.MqttClient.Publish("Power/Raspberry/"+cmd, Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public static void Raspberry_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            string[] topic = e.Topic.Split('/');
            string Category = topic[0];
            if (topic.Length > 3)
            {
                var message = System.Text.Encoding.Default.GetString(e.Message);
                if (GlobalFunction.IsValidJson(message))
                {
                    var RT = GlobalHost.ConnectionManager.GetHubContext<RealTimeHub>();
                    object result = JsonConvert.DeserializeObject(message);
                    JObject voobj = JObject.Parse(result.ToString());

                    string Brand = topic[1];
                    string hostDevice = topic[2];
                    string cmd = topic[3];
                    if (cmd == "WhoisDatas")
                    {
                        string DeviceID = hostDevice;
                        DatabaseContext oRemoteDB = new DatabaseContext();
                        RaspberryModel oQuery = new RaspberryModel();
                        RaspberryObject_REC voRecord = new RaspberryObject_REC();
                        voRecord.DeviceID = DeviceID;
                        oQuery.ObjectDelete(oRemoteDB, voRecord);
                        for (int i = 0; i < voobj.Count; i++)
                        {

                            RaspberryObject_REC poRecord = new RaspberryObject_REC();
                            poRecord.DeviceID = DeviceID;
                            poRecord.ObjectName = voobj["" + i + ""]["ObjType"].ToString().Split(':')[0];
                            poRecord.Instance = voobj["" + i + ""]["ObjType"].ToString().Split(':')[1];
                            poRecord.Description = voobj["" + i + ""]["ObjName"].ToString();

                            poRecord.ObjectFullName = voobj["" + i + ""]["ObjName"].ToString();
                            poRecord.ObjectIdentifier = voobj["" + i + ""]["objectIdentifier"].ToString();
                            poRecord.ObjectType = voobj["" + i + ""]["objectType"].ToString()+" : "+ voobj["" + i + ""]["ObjType"].ToString().Split(':')[0];
                            poRecord.Type = 1;
                            oQuery.ObjectInsert(oRemoteDB, poRecord);
                        }
                        RT.Clients.All.Raspberry("WhoisDevice", DeviceID);
                    }
                    else if (cmd == "Error")
                    {
                        // begin set status
                        DatabaseContext oRemoteDB = new DatabaseContext();
                        RaspberryModel oQuery = new RaspberryModel();
                        Raspberry_REC oRecord = new Raspberry_REC();
                        oRecord.DeviceName = hostDevice;
                        oRecord.StatusDevice = "Disconnected";
                        oQuery.UpdateStatus(oRemoteDB, oRecord);
                        var json = new JavaScriptSerializer().Serialize(oRecord);
                        RT.Clients.All.Raspberry("Device", json);
                        RT.Clients.All.Raspberry("Error", voobj["msg"]);
                    }
                    else if (cmd == "Datas")
                    {
                        DatabaseContext oRemoteDB = new DatabaseContext();
                        RaspberryModel oQuery = new RaspberryModel();

                        var vResult = oQuery.GetOne(hostDevice).SingleOrDefault();
                        if (vResult != null) { 
                            if (vResult.Live == 1)
                            {
                                RaspberryDataLog_REC poRecord = new RaspberryDataLog_REC();
                                poRecord.RecordTimestamp = DateTime.Now;
                                poRecord.DeviceID = hostDevice;
                                poRecord.ObjectID = hostDevice;
                                poRecord.Payload = message;
                                oQuery.DataLogInsert(oRemoteDB, poRecord);
                                RT.Clients.All.Raspberry("DataLog", message);

                                if (vResult.StatusDevice != "Conneted")
                                {
                                    // begin set status
                                    Raspberry_REC oRecord = new Raspberry_REC();
                                    oRecord.DeviceName = hostDevice;
                                    oRecord.StatusDevice = "Conneted";
                                    oQuery.UpdateStatus(oRemoteDB, oRecord);
                                    var json = new JavaScriptSerializer().Serialize(oRecord);
                                    RT.Clients.All.Raspberry("Device", json);
                                }
                            }
                        }
                    }
                    else
                    if (cmd == "setConfigData")
                    {
                        RT.Clients.All.Raspberry("SetConfig", message);
                    }
                    else
                    if (cmd == "getConfigData")
                    {
                        RT.Clients.All.Raspberry("GetConfig", message);
                    }
                    else
                    if (cmd == "ReadDatas")
                    {
                        RT.Clients.All.Raspberry("ReadDatas", voobj["value"].ToString());
                    }
                    else
                    if (cmd == "WriteDatas")
                    {
                        RT.Clients.All.Raspberry("WriteDatas", voobj["value"].ToString());
                    }
                }
            }
        }
        #endregion

        #region Datalog
        public ActionResult DataLog()
        {
            return View();
        }

        public ActionResult FlowRate()
        {
            return View();
        }
        public ActionResult GetFlowRateDatalogs([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date, string weekday, string floor, string tableName)
        {
            var ds = new List<RaspberryDataLog_REC>().ToDataSourceResult(poRequest);
            var database = Constants.MongoClient.GetDatabase("IoT");
            var collecCount = database.GetCollection<FlowRateDataLog_REC>("AirCondDataLogs");
            var insertFlr = database.GetCollection<FlowRate_REC>(tableName);

            var filter = new BsonDocument
            {
                { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
            };
            string add = "{$addFields: { dow: { $dayOfWeek: \"$RecordTimestamp\" }}}";
            string grp = "{$match: { $or: [{ dow: 1 },{dow: 7 }], ObjectName: {$in: [\"AHU-2-3-1 CHWS Temp\", \"AHU-2-3-2 CHWS Temp\", \"AHU-2-3-1 CHWR Temp\", \"AHU-2-3-2 CHWR Temp\", \"AHU-2-3-1 AHU CHWS Flow\", \"AHU-2-3-2 CHWS Flow\"] }, RecordTimestamp: " + filter + " } }";
            if (weekday == "1")
            {
                grp = "{$match: { dow: {$gte: 2, $lte: 6}, ObjectName: {$in: [\"AHU-2-3-1 CHWS Temp\", \"AHU-2-3-2 CHWS Temp\", \"AHU-2-3-1 CHWR Temp\", \"AHU-2-3-2 CHWR Temp\", \"AHU-2-3-1 AHU CHWS Flow\", \"AHU-2-3-2 CHWS Flow\"] }, RecordTimestamp: " + filter + " } }";
            }
            if (floor == "7")
            {
                grp = "{$match: { $or: [{ dow: 1 },{dow: 7 }], ObjectName: {$in: [\"AHU-2-7-1 CHWS Temp\", \"AHU-2-7-2 CHWS Temp\",\"AHU-2-7-1 CHWR Temp\", \"AHU-2-7-2 CHWR Temp\", \"AHU-2-7-1 CHWS Flow\", \"AHU-2-7-2 CHWS Flow\"] }, RecordTimestamp: " + filter + " } }";
                if (weekday == "1")
                {
                    grp = "{$match: { dow: {$gte: 2, $lte: 6}, ObjectName: {$in: [\"AHU-2-7-1 CHWS Temp\", \"AHU-2-7-2 CHWS Temp\", \"AHU-2-7-1 CHWR Temp\", \"AHU-2-7-2 CHWR Temp\", \"AHU-2-7-1 CHWS Flow\", \"AHU-2-7-2 CHWS Flow\"] }, RecordTimestamp: " + filter + " } }";
                }

            }
            string prj = "{$project: {RecordTimestamp: {\"$dateToString\": {\"format\": \"%Y%m%d%H%M\", \"date\": \"$RecordTimestamp\"} }, DeviceID : 1, ObjectName : {$cond: [ { $eq: [ \"$InstanceID\", \"1014\" ] }, { $substr: [ \"$ObjectName\", 14, 9 ] }, { $substr: [ \"$ObjectName\", 10, 9 ] } ]}, InstanceID : 1, PresentValue: {$cond: [ { $gt: [ {$toDouble: \"$PresentValue\"}, 0 ] }, {$toDouble: \"$PresentValue\"}, 0 ] } } }";
            string srt = "{$sort: {RecordTimestamp: 1, ObjectName: 1, InstanceID: 1}}";
            var pipeline = collecCount.Aggregate().AppendStage<FlowRateDataLog_REC>(add).AppendStage<FlowRateDataLog_REC>(grp).AppendStage<FlowRateDataLog_REC>(prj).AppendStage<FlowRateDataLog_REC>(srt);
            var resData = pipeline.ToList();
            List<FlowRate_REC> FLowrateList = new List<FlowRate_REC>();
            int RootIndex = 0;
            int voindex1 = 0;
            int voindex2 = 0;
            int voindex3 = 0;
            long lasttime = 0;
            string lastObject = "";
            decimal lastValue1 = Convert.ToDecimal(0.00);
            decimal lastValue2 = Convert.ToDecimal(0.00);
            decimal lastValue3 = Convert.ToDecimal(0.00);
            //Dictionary<int, int> dictionary = new Dictionary<int, int> { { 1014, 3 }, { 1016, 7 }, { 1017, 7 }, { 888, 3 }, { 887, 3 }, { 872, 7 }, { 879, 7 }, { 871, 7 }, { 878, 7 }, { 896, 3 }, { 895, 3 }, { 1015, 3 } };
            FlowRate_REC FLR = new FlowRate_REC();
            foreach (FlowRateDataLog_REC voFRData in resData)
            {
                if (RootIndex < resData.Count)
                {
                    if (lasttime == Convert.ToInt64(voFRData.RecordTimestamp) || lasttime == 0)
                    {
                        if (voFRData.ObjectName == "CHWS Temp")
                        {
                            voindex1++;
                            lastValue1 += Convert.ToDecimal(voFRData.PresentValue);
                            FLR.CHWSTemp = lastValue1 / voindex1;
                            FLR.CHWSTemp = Math.Abs(FLR.CHWSTemp);
                            FLR.CHWSTemp = Math.Round(FLR.CHWSTemp, 2);
                        }
                        else if (voFRData.ObjectName == "CHWR Temp")
                        {
                            voindex2++;
                            lastValue2 += Convert.ToDecimal(voFRData.PresentValue);
                            FLR.CHWRTemp = lastValue2 / voindex2;
                            FLR.CHWRTemp = Math.Abs(FLR.CHWRTemp);
                            FLR.CHWRTemp = Math.Round(FLR.CHWRTemp, 2);
                        }
                        else if (voFRData.ObjectName == "CHWS Flow")
                        {
                            voindex3++;
                            lastValue3 += Convert.ToDecimal(voFRData.PresentValue);
                            FLR.CHWSFlow = lastValue3 / voindex3;
                            FLR.CHWSFlow = Math.Abs(FLR.CHWSFlow);
                            FLR.CHWSFlow = Math.Round(FLR.CHWSFlow, 2);
                        }
                    }
                    else
                    {
                        FLR.Timestamp = Convert.ToInt64(voFRData.RecordTimestamp);
                        FLR.KW = (FLR.CHWRTemp - FLR.CHWSTemp) * FLR.CHWSFlow * Convert.ToDecimal(1.19) * Convert.ToDecimal(3.517);
                        FLR.KW = Math.Abs(FLR.KW);
                        FLR.KW = Math.Round(FLR.KW, 2);
                        //FLR.KWh = FLR.KW *(lasttime - Convert.ToInt64(voFRData.RecordTimestamp)) / 3600;
                        FLR.KWh = FLR.KW * 60 / 3600;
                        FLR.KWh = Math.Abs(FLR.KWh);
                        FLR.KWh = Math.Round(FLR.KWh, 2);
                        FLR.Floor = Convert.ToInt32(floor);
                        insertFlr.InsertOne(FLR);
                        lastValue1 = Convert.ToDecimal(0.00);
                        lastValue2 = Convert.ToDecimal(0.00);
                        lastValue3 = Convert.ToDecimal(0.00);
                        voindex1 = 0;
                        voindex2 = 0;
                        voindex3 = 0;
                        FLR = new FlowRate_REC();
                    }
                }
                else
                {
                    FLR.Timestamp = Convert.ToInt64(voFRData.RecordTimestamp);
                    FLR.KW = (FLR.CHWRTemp - FLR.CHWSTemp) * FLR.CHWSFlow * Convert.ToDecimal(1.19) * Convert.ToDecimal(3.517);
                    FLR.KW = Math.Abs(FLR.KW);
                    FLR.KW = Math.Round(FLR.KW, 2);
                    //FLR.KWh = FLR.KW * (lasttime - Convert.ToInt64(voFRData.RecordTimestamp)) / 3600;
                    FLR.KWh = FLR.KW * 60 / 3600;
                    FLR.KWh = Math.Abs(FLR.KWh);
                    FLR.KWh = Math.Round(FLR.KWh, 2);
                    insertFlr.InsertOne(FLR);
                    lastValue1 = Convert.ToDecimal(0.00);
                    lastValue2 = Convert.ToDecimal(0.00);
                    lastValue3 = Convert.ToDecimal(0.00);
                    voindex1 = 0;
                    voindex2 = 0;
                    voindex3 = 0;
                    FLR = new FlowRate_REC();
                }
                lasttime = Convert.ToInt64(voFRData.RecordTimestamp);
            }
            ds.Data = resData;
            ds.Total = Convert.ToInt32(resData.Count());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult GetListDataLog([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            /*RaspberryModel oModel = new RaspberryModel();
            IList<RaspberryDataLog_REC> oTable = oModel.GetListDataLog(poRequest,DeviceID).ToList();
            var ds = oTable.ToDataSourceResult(poRequest);

            if (poRequest.Filters.Count > 0)
                ds.Total = oTable.Count;
            else
            {
                ds.Total = oModel.GetCountData("t70310").SingleOrDefault().count;
                ds.Data = oTable;
            }

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;*/

            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<RaspberryDataLog_REC> PowerDataLogs = database.GetCollection<RaspberryDataLog_REC>("PowerDataLogs");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<RaspberryDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                    { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<RaspberryDataLog_REC> oTable = PowerDataLogs.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable;
            ds.Total = Convert.ToInt32(PowerDataLogs.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public IEnumerable<RaspberryDataLog_REC> GetListDataLogReport([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            //RaspberryModel oClass = new RaspberryModel();
            //IEnumerable<RaspberryDataLog_REC> vResult = oClass.GetListDataLog().ToList();
            //return vResult;
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<RaspberryDataLog_REC> PowerDataLogs = database.GetCollection<RaspberryDataLog_REC>("PowerDataLogs");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<RaspberryDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();
            if (filter_date != null)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) },
                    { "$lt", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            IEnumerable<RaspberryDataLog_REC> oTable = PowerDataLogs.Find(filter_mongo).Sort(sort_mongo).ToList();
            return oTable;
        }
        [HttpPost]
        public ActionResult ExportServer([DataSourceRequest]DataSourceRequest poRequest, ParameterExport data)
        {
            var columnsData = data.columns.ToArray();
            SpreadDocumentFormat exportFormat = data.options.format.ToString() == "csv" ? exportFormat = SpreadDocumentFormat.Csv : exportFormat = SpreadDocumentFormat.Xlsx;
            Action<ExportCellStyle> cellStyle = new Action<ExportCellStyle>(ChangeCellStyle);
            Action<ExportRowStyle> rowStyle = new Action<ExportRowStyle>(ChangeRowStyle);
            Action<ExportColumnStyle> columnStyle = new Action<ExportColumnStyle>(ChangeColumnStyle);

            string fileName = string.Format("{0}.{1}", data.options.title, data.options.format);
            string mimeType = Helpers.GetMimeType(exportFormat);

            Stream exportStream = exportFormat == SpreadDocumentFormat.Xlsx ?
                GetListDataLogReport(poRequest, data.transport).ToXlsxStream(columnsData) :
                GetListDataLogReport(poRequest, data.transport).ToCsvStream(columnsData);

            var fileStreamResult = new FileStreamResult(exportStream, mimeType);
            fileStreamResult.FileDownloadName = fileName;
            fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);
            using (var fileStream = System.IO.File.Create(Server.MapPath("~" + Constants.ExportDirectory + fileName)))
            {
                fileStreamResult.FileStream.CopyTo(fileStream);
            }

            return Json(new { filename = fileName });
        }
        private void ChangeCellStyle(ExportCellStyle e)
        {
            bool isHeader = e.Row == 0;
            SpreadBorder border = new SpreadBorder(SpreadBorderStyle.Thick, new SpreadThemableColor(new SpreadColor(0, 0, 0)));
            SpreadCellFormat format = new SpreadCellFormat
            {
                FontSize = 11,
                ForeColor = new SpreadThemableColor(new SpreadColor(0, 0, 0)),
                WrapText = false
            };
            e.Cell.SetFormat(format);
        }
        private void ChangeRowStyle(ExportRowStyle e)
        {
            e.Row.SetHeightInPixels(e.Index == 0 ? 80 : 30);
        }
        private void ChangeColumnStyle(ExportColumnStyle e)
        {
            double width = e.Name == "Item ID" || e.Name == "Barcode" ? 200 : 100;
            e.Column.SetWidthInPixels(width);
        }
        #endregion
    }
}