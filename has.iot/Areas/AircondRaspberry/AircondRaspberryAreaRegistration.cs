﻿using Aircond.Raspberry.Components;
using has.iot.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace MVCPluggableDemo
{
    public class AircondRaspberryAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AircondRaspberry";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AircondRaspberry_default",
                "AircondRaspberry/{controller}/{action}/{id}",
                new {controller= "Raspberry", action = "Index", id = UrlParameter.Optional },
                new string[] { "Aircond.Raspberry.Controllers" }
            );
            Constants.MqttClient = new MqttClient(GlobalFunction.mqttServer);
            Constants.MqttClient.Subscribe(new string[] { "Aircond/Raspberry/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            Constants.MqttClient.MqttMsgPublishReceived += Aircond.Raspberry.Controllers.RaspberryController.Raspberry_MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            Constants.MqttClient.Connect(clientId);
        }
    }
}