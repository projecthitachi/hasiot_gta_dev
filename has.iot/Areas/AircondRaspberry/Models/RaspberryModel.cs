﻿using Aircond.Raspberry.Models.System;
using has.iot.Components;
using Kendo.Mvc;
using Kendo.Mvc.Export;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Aircond.Raspberry.Models
{
    public class Header_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string ids { get; set; }
        public string dis { get; set; }
        public string marker { get; set; }
        public string DeviceID { get; set; }
        public List<Detail_REC> Detail { get; set; }
    }
    public class HeaderView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string ids { get; set; }
        public string dis { get; set; }
        public string marker { get; set; }
        public string DeviceID { get; set; }
        public List<Detail_REC> Detail { get; set; }
    }
    public class Detail_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string paraent_id { get; set; }
        public int RowIndex { get; set; }
        public string tag_kinds { get; set; }
        public string tag_name { get; set; }
        public string value { get; set; }
    }
    public class Raspberry_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string CategoryID { get; set; }
        public string IPAddress { get; set; }
        public string MacAddress { get; set; }
        public string SSID { get; set; }
        public string Password { get; set; }
        public string MQTTHost { get; set; }
        public string MQTTUsername { get; set; }
        public string MQTTPassword { get; set; }
        public string StatusDevice { get; set; }
        public string StatusSwitch { get; set; }
        public int Live { get; set; }
        public string ids { get; set; }
        public string dis { get; set; }
        public string marker { get; set; }
    }
    public class RaspberryDataLog_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [Display(Name = "RecordID")]
        public long RecordID { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }

        [Display(Name = "Record Status")]
        public Int32 RecordStatus { get; set; }

        [Display(Name = "Device ID")]
        public string DeviceID { get; set; }

        [Display(Name = "Device Name")]
        public string DeviceName { get; set; }

        [Display(Name = "Object ID")]
        public string ObjectID { get; set; }

        [Display(Name = "Object Name")]
        public string ObjectName { get; set; }

        [Display(Name = "Object ID")]
        public string InstanceID { get; set; }

        [Display(Name = "Present Value")]
        public string PresentValue { get; set; }
        
        [Display(Name = "Payload")]
        public string Payload { get; set; }

    }
    public class ParameterExport
    {
        public IList<ExportColumnSettings> columns { get; set; }
        public ParameterExportOptions options { get; set; }
        public FilterDate transport { get; set; }
    }

    public class ParameterExportOptions
    {
        public string format { get; set; }
        public string title { get; set; }
    }
    public class RaspberryObject_REC
    {
        [Display(Name = "RecordID")]
        public long RecordID { get; set; }

        [Display(Name = "RecordTimestamp")]
        public DateTime RecordTimestamp { get; set; }

        [Display(Name = "Record Status")]
        public Int32 RecordStatus { get; set; }

        [Display(Name = "Device ID")]
        public string DeviceID { get; set; }

        [Display(Name = "ObjectName")]
        public string ObjectName { get; set; }

        [Display(Name = "Instance")]
        public string Instance { get; set; }

        [Display(Name = "Type")]
        public int Type { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [UIHint("ClientCategory")]
        public CategoryView_REC Category
        {
            get;
            set;
        }
        [Display(Name = "ObjectFullName")]
        public string ObjectFullName { get; set; }
        [Display(Name = "ObjectIdentifier")]
        public string ObjectIdentifier { get; set; }
        [Display(Name = "ObjectType")]
        public string ObjectType { get; set; }
    }

    public class CategoryView_REC
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
    public class CountData
    {
        public int count { get; set; }
    }
    public class FilterDate
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
    public class RaspberryModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public RaspberryModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t70310fieldselect"
        private string t70310fieldselect = @"
            t70310r001 as RecordID,
            t70310r002 as RecordTimestamp,
            t70310r003 as RecordStatus,
            t70310f001 as DeviceID,
            t70310f002 as ObjectID,
            t70310f003 as Payload";
        #endregion
        public DbRawSqlQuery<CountData> GetCountData(string table)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT COUNT(*) as count from " + table;
            var vQuery = oRemoteDB.Database.SqlQuery<CountData>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<RaspberryDataLog_REC> GetListDataLog([DataSourceRequest]DataSourceRequest poRequest, string DeviceID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            int start = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int end = poRequest.Page * poRequest.PageSize;

            string WhereClause = "";
            foreach (FilterDescriptor filter in poRequest.Filters)
            {
                WhereClause += filter.Member + " LIKE '%" + filter.Value + "%' AND ";
            }

            string Offset = "";
            string Limit = "";
            if (WhereClause.Length == 0)
            {
                Offset = "([table].RowNum BETWEEN " + start + " AND " + end + ")";
                Limit = "TOP(" + poRequest.PageSize + ")";
            }
            else
                WhereClause = WhereClause.Remove(WhereClause.LastIndexOf("AND"), 3);

            string sSQL = "SELECT " + Limit + "* " +
                "FROM(SELECT " + t70310fieldselect + ", ROW_NUMBER() OVER(ORDER BY t70310r001 DESC) AS RowNum " +
                "FROM t70310 WHERE t70310f001 NOT IN ('200007') ) AS [table] " +
                "WHERE " + WhereClause + Offset;

            if (DeviceID != null)
            {
                sSQL = "SELECT " + Limit + "* " +
                "FROM(SELECT " + t70310fieldselect + ", ROW_NUMBER() OVER(ORDER BY t70310r001 DESC) AS RowNum " +
                "FROM t70310 WHERE t70310f001 = '" + DeviceID + "') AS [table] " +
                "WHERE " + WhereClause + Offset;
            }
            var vQuery = oRemoteDB.Database.SqlQuery<RaspberryDataLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<RaspberryDataLog_REC> GetListDataLog()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70310fieldselect + " FROM t70310 ORDER BY t70310r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<RaspberryDataLog_REC>(sSQL);
            return vQuery;
        }
        public bool DataLogInsert(DatabaseContext oRemoteDB, RaspberryDataLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70310 " +
                    "      ( " +
                    "      t70310r002, t70310r003, " +
                    "      t70310f001, t70310f002, t70310f003"+
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70310r002, @pt70310r003, " +
                    "      @pt70310f001, @pt70310f002, @pt70310f003" +
                    "      )" +
                    ";" +
                    "SELECT @pt70310r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70310r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70310r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70310r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70310f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70310f002", poRecord.ObjectID));
                oParameters.Add(new SqlParameter("@pt70310f003", poRecord.Payload));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        #region "t70211fieldselect"
        private string t70211fieldselect = @"
            t70211r001 as RecordID,
            t70211r002 as RecordTimestamp,
            t70211r003 as RecordStatus,
            t70211f001 as DeviceID,
            t70211f002 as ObjectName,
            t70211f003 as Instance,
            t70211f004 as Type,
            t70211f005 as Description,
            t70211f006 as ObjectFullName,
            t70211f007 as ObjectIdentifier,
            t70211f008 as ObjectType,
            t70211f009 as LocationID";
        #endregion
        
        public DbRawSqlQuery<RaspberryObject_REC> GetListDataObject(string DeviceID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70211fieldselect + " FROM t70211 WHERE t70211f001 = '" + DeviceID + "' AND t70211f004  IN ('1','3') ORDER BY t70211r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<RaspberryObject_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<RaspberryObject_REC> GetListDLLObject(string DeviceID, string voType, string voText)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70211fieldselect + " FROM t70211 WHERE t70211f001 = '" + DeviceID + "' AND t70211f002 LIKE '%" + voText + "%' AND t70211f004  IN ('" + voType + "','3')  ORDER BY t70211r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<RaspberryObject_REC>(sSQL);
            return vQuery;
        }
        public bool ObjectInsert(DatabaseContext oRemoteDB, RaspberryObject_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70211 " +
                    "      ( " +
                    "      t70211r002, t70211r003, " +
                    "      t70211f001, t70211f002, t70211f003, t70211f004, t70211f005, t70211f006, t70211f007, t70211f008  " +
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70211r002, @pt70211r003, " +
                    "      @pt70211f001, @pt70211f002, @pt70211f003, @pt70211f004, @pt70211f005, @pt70211f006, @pt70211f007, @pt70211f008 " +
                    "      )" +
                    ";" +
                    "SELECT @pt70211r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70211r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70211r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70211r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70211f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70211f002", poRecord.ObjectName));
                oParameters.Add(new SqlParameter("@pt70211f003", poRecord.Instance));
                oParameters.Add(new SqlParameter("@pt70211f004", poRecord.Type));
                oParameters.Add(new SqlParameter("@pt70211f005", poRecord.Description));
                oParameters.Add(new SqlParameter("@pt70211f006", poRecord.ObjectFullName));
                oParameters.Add(new SqlParameter("@pt70211f007", poRecord.ObjectIdentifier));
                oParameters.Add(new SqlParameter("@pt70211f008", poRecord.ObjectType));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool ObjectUpdate(DatabaseContext oRemoteDB, RaspberryObject_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            poRecord.RecordTimestamp = DateTime.Now;

            //----------
            //oRemoteDB.TrimNull(poRecord);
            string sSQL = "" +
            "UPDATE t70211 " +
            "   SET " +
            "      t70211f002 = @pt70211f002, " +
            "      t70211f003 = @pt70211f003, " +
            "      t70211f004 = @pt70211f004, " +
            "      t70211f005 = @pt70211f005, " +
            "      t70211f006 = @pt70211f006, " +
            "      t70211f007 = @pt70211f007, " +
            "      t70211f008 = @pt70211f008 " +
            "   WHERE (t70211r001 = @pt70211r001) " +
            ";"
            ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70211r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70211r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70211r003", poRecord.RecordStatus));

            oParameters.Add(new SqlParameter("@pt70211f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70211f002", poRecord.ObjectName));
            oParameters.Add(new SqlParameter("@pt70211f003", poRecord.Instance));
            oParameters.Add(new SqlParameter("@pt70211f004", poRecord.Type));
            oParameters.Add(new SqlParameter("@pt70211f005", poRecord.Description));
            oParameters.Add(new SqlParameter("@pt70211f006", poRecord.ObjectFullName));
            oParameters.Add(new SqlParameter("@pt70211f007", poRecord.ObjectIdentifier));
            oParameters.Add(new SqlParameter("@pt70211f008", poRecord.ObjectType));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            //------------------------------
            return bReturn;
        }
        public bool ObjectDelete(DatabaseContext oRemoteDB, RaspberryObject_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t70211 " +
                "   WHERE (t70211f001 = @pt70211f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70211f001", poRecord.DeviceID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        public DbRawSqlQuery<Raspberry_REC> GetSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT tb.t70200f005 AS DeviceID FROM t70100 ta JOIN t70200 tb ON ta.t70100f001 = tb.t70200f001 WHERE ta.t70100f001 = 'Aircond';";

            var vQuery = oRemoteDB.Database.SqlQuery<Raspberry_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Raspberry_REC> GetLastSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT TOP 1 t70210f001 AS DeviceID FROM t70210 ORDER BY t70210r001 DESC;";

            var vQuery = oRemoteDB.Database.SqlQuery<Raspberry_REC>(sSQL);

            return vQuery;
        }

        public DbRawSqlQuery<Raspberry_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT t70210r001 AS RecordID, t70210r002 AS RecordTimestamp, t70210r003 AS RecordStatus, t70210f001 AS DeviceID, t70210f002 AS DeviceName, t70210f003 AS CategoryID, t70210f004 AS IPAddress, t70210f005 AS MacAddress, t70210f006 AS SSID, t70210f007 AS Password, t70210f008 AS MQTTHost, t70210f009 AS MQTTUsername, t70210f010 AS MQTTPassword, t70210f011 AS StatusDevice, t70210f012 AS StatusSwitch, t70210f013 AS Live, t70210f014 AS DeviceType, t70210f015 AS ids, t70210f016 AS dis, t70210f017 AS marker FROM dbo.t70210 " +
                " WHERE t70210f014 = 'Aircond' ORDER BY t70210r001 DESC;";

            var vQuery = oRemoteDB.Database.SqlQuery<Raspberry_REC>(sSQL);

            return vQuery;
        }

        public DbRawSqlQuery<Raspberry_REC> GetOne(string DeviceID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT t70210f011 AS StatusDevice, t70210f013 AS Live, t70210f015 AS ids, t70210f016 AS dis, t70210f017 AS marker FROM dbo.t70210 " +
                "WHERE t70210f002 = '"+ DeviceID + "' AND t70210f014 = 'Aircond' ;";

            var vQuery = oRemoteDB.Database.SqlQuery<Raspberry_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Raspberry_REC> GetForLookup(string voType, string voText)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT t70210r001 AS RecordID, t70210r002 AS RecordTimestamp, t70210r003 AS RecordStatus, t70210f001 AS DeviceID, t70210f002 AS DeviceName, t70210f003 AS CategoryID, t70210f004 AS IPAddress, t70210f005 AS MacAddress, t70210f006 AS SSID, t70210f007 AS Password, t70210f008 AS MQTTHost, t70210f009 AS MQTTUsername, t70210f010 AS MQTTPassword, t70210f011 AS StatusDevice, t70210f012 AS StatusSwitch, t70210f013 AS Live, t70210f015 AS ids, t70210f016 AS dis, t70210f017 AS marker FROM t70210 " +
                "   WHERE t70210f014 = 'Aircond' AND t70210f002 LIKE '%" + voText + "%'  " +
                "   ORDER BY t70210f001 ASC" +
                ";"
                ;

            var vQuery = oRemoteDB.Database.SqlQuery<Raspberry_REC>(sSQL);

            return vQuery;
        }
        public bool Insert(DatabaseContext oRemoteDB, Raspberry_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;
                if (GetLastSerialNo().ToList().Count == 0)
                {
                    poRecord.DeviceID = GetSerialNo().SingleOrDefault().DeviceID+".0000.0001";
                }
                else
                {
                    string[] serialNo = GetLastSerialNo().SingleOrDefault().DeviceID.Split('.');
                    int lastNo = Convert.ToInt32(serialNo[3]) + 1;
                    poRecord.DeviceID = serialNo[0]+"."+ serialNo[1]+"."+ serialNo[2]+"."+lastNo.ToString("D4");

                }
                //----------
                GlobalFunction.TrimNull(poRecord);
                
                string sSQL = "" +
                    "INSERT INTO t70210 " +
                    "      ( " +
                    "      t70210r002, t70210r003, " +
                    "      t70210f001, t70210f002, t70210f003, t70210f004, t70210f005, t70210f006, t70210f007, t70210f008, t70210f009, t70210f010, t70210f011, t70210f012, t70210f013, t70210f014, t70210f015, t70210f016, t70210f017 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70210r002, @pt70210r003, " +
                    "      @pt70210f001, @pt70210f002, @pt70210f003, @pt70210f004, @pt70210f005, @pt70210f006, @pt70210f007, @pt70210f008, @pt70210f009, @pt70210f010, @pt70210f011, @pt70210f012, @pt70210f013, @pt70210f014, @pt70210f015, @pt70210f016, @pt70210f017 " +
                    "      )" +
                    ";" +
                    "SELECT @pt70210r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70210r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70210r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70210r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70210f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70210f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt70210f003", poRecord.CategoryID));
                oParameters.Add(new SqlParameter("@pt70210f004", poRecord.IPAddress));
                oParameters.Add(new SqlParameter("@pt70210f005", poRecord.MacAddress));
                oParameters.Add(new SqlParameter("@pt70210f006", poRecord.SSID));
                oParameters.Add(new SqlParameter("@pt70210f007", poRecord.Password));
                oParameters.Add(new SqlParameter("@pt70210f008", poRecord.MQTTHost));
                oParameters.Add(new SqlParameter("@pt70210f009", poRecord.MQTTUsername));
                oParameters.Add(new SqlParameter("@pt70210f010", poRecord.MQTTPassword));
                oParameters.Add(new SqlParameter("@pt70210f011", poRecord.StatusDevice));
                oParameters.Add(new SqlParameter("@pt70210f012", poRecord.StatusSwitch));
                oParameters.Add(new SqlParameter("@pt70210f013", poRecord.Live));
                oParameters.Add(new SqlParameter("@pt70210f014", "Aircond"));
                oParameters.Add(new SqlParameter("@pt70210f015", poRecord.ids));
                oParameters.Add(new SqlParameter("@pt70210f016", poRecord.dis));
                oParameters.Add(new SqlParameter("@pt70210f017", poRecord.marker));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, Raspberry_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70210 " +
                "   SET " +
                "      t70210f002 = @pt70210f002, " +
                "      t70210f003 = @pt70210f003, " +
                "      t70210f004 = @pt70210f004, " +
                "      t70210f005 = @pt70210f005, " +
                "      t70210f006 = @pt70210f006, " +
                "      t70210f007 = @pt70210f007, " +
                "      t70210f008 = @pt70210f008, " +
                "      t70210f009 = @pt70210f009, " +
                "      t70210f010 = @pt70210f010, " +
                "      t70210f011 = @pt70210f011, " +
                "      t70210f012 = @pt70210f012, " +
                "      t70210f013 = @pt70210f013, " +
                "      t70210f015 = @pt70210f015, " +
                "      t70210f016 = @pt70210f016, " +
                "      t70210f017 = @pt70210f017 " +
                "   WHERE (t70210f001 = @pt70210f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70210f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70210f002", poRecord.DeviceName));
            oParameters.Add(new SqlParameter("@pt70210f003", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70210f004", poRecord.IPAddress));
            oParameters.Add(new SqlParameter("@pt70210f005", poRecord.MacAddress));
            oParameters.Add(new SqlParameter("@pt70210f006", poRecord.SSID));
            oParameters.Add(new SqlParameter("@pt70210f007", poRecord.Password));
            oParameters.Add(new SqlParameter("@pt70210f008", poRecord.MQTTHost));
            oParameters.Add(new SqlParameter("@pt70210f009", poRecord.MQTTUsername));
            oParameters.Add(new SqlParameter("@pt70210f010", poRecord.MQTTPassword));
            oParameters.Add(new SqlParameter("@pt70210f011", poRecord.StatusDevice));
            oParameters.Add(new SqlParameter("@pt70210f012", poRecord.StatusSwitch));
            oParameters.Add(new SqlParameter("@pt70210f013", poRecord.Live));
            oParameters.Add(new SqlParameter("@pt70210f015", poRecord.ids));
            oParameters.Add(new SqlParameter("@pt70210f016", poRecord.dis));
            oParameters.Add(new SqlParameter("@pt70210f017", poRecord.marker));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateStatus(DatabaseContext oRemoteDB, Raspberry_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70210 " +
                "   SET " +
                "      t70210f011 = @pt70210f011 " +
                "   WHERE (t70210f002 = @pt70210f002) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70210f002", poRecord.DeviceName));
            oParameters.Add(new SqlParameter("@pt70210f011", poRecord.StatusDevice));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, Raspberry_REC poRecord)
        {
            bool bReturn = true;
            try { 
                string sSQL = "" +
                    "DELETE FROM t70210 " +
                    "   WHERE (t70210f001 = @pt70210f001) " +
                    ";"
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter("@pt70210f001", poRecord.DeviceID));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            }catch(Exception e)
            {
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool updateStatusObj(DatabaseContext oRemoteDB, string RecordID, string DeviceID)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;

            string sSQL = " UPDATE t70211 SET t70211r003 = 0 WHERE t70211f001 = @pt70210r001; ";
            if (RecordID != null)
            {
                sSQL += " UPDATE t70211 SET t70211r003 = 1 WHERE t70211f001 = @pt70210r001 AND t70211r001 IN(" + RecordID + ");";
            }
            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70210r001", DeviceID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            //------------------------------
            return bReturn;
        }
    }
}