﻿using Kendo.Mvc;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace Aircond.Raspberry.Components
{
    public class Constants
    {
        public static MqttClient MqttClient = null;
        public static MongoClient MongoClient = new MongoClient(WebConfigurationManager.AppSettings["mongodb_server"].ToString());
        public static string ExportDirectory = "/Components/Export/AirCond/";
        public static FilterDescriptor KendoChangeComposite(IEnumerable<IFilterDescriptor> filters)
        {
            FilterDescriptor filt = new FilterDescriptor();
            foreach (var filter in filters)
            {
                if (filter is CompositeFilterDescriptor)
                    KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                else
                    filt = ((FilterDescriptor)filter);
            }
            return filt;
        }
    }
}