﻿using has.iot.Components;
using Aircond.Raspberry.Models.System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Aircond.Raspberry.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using uPLibrary.Networking.M2Mqtt.Messages;
using Kendo.Mvc.Export;
using Telerik.Documents.SpreadsheetStreaming;
using System.IO;
using System.Text;
using Aircond.Raspberry.Components;
using MongoDB.Driver;
using Kendo.Mvc;
using MongoDB.Bson;
using System.Text.RegularExpressions;

namespace Aircond.Raspberry.Controllers
{
    public class HaystackController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Form(string id)
        {
            if (id == null)
            {
                return View("Form");
            }
            else
            {
                var database = Constants.MongoClient.GetDatabase("IoT");
                var GetData = database.GetCollection<HeaderView_REC>("HaystackDatas");

                var builder = Builders<HeaderView_REC>.Filter;
                var filter = builder.Eq<string>("RecordID", id);
                var vResult = GetData.Find(filter).SingleOrDefault();

                ViewData["Data"] = vResult;
                return View("FormEdit");
            }
        }
        public ActionResult Detail(string id)
        {
            if (id == null)
            {
                return View("Form");
            }
            else
            {
                var database = Constants.MongoClient.GetDatabase("IoT");
                var GetData = database.GetCollection<HeaderView_REC>("HaystackDatas");

                var builder = Builders<HeaderView_REC>.Filter;
                var filter = builder.Eq<string>("RecordID", id);
                var vResult = GetData.Find(filter).SingleOrDefault();

                ViewData["Data"] = vResult;
                return View("FormDetail");
            }
        }
        public ActionResult RemoveForm(string id)
        {
            if (id == null)
            {
                return View("Form");
            }
            else
            {
                var database = Constants.MongoClient.GetDatabase("IoT");
                var GetData = database.GetCollection<HeaderView_REC>("HaystackDatas");

                var builder = Builders<HeaderView_REC>.Filter;
                var filter = builder.Eq<string>("RecordID", id);
                var vResult = GetData.Find(filter).SingleOrDefault();

                ViewData["Data"] = vResult;
                return View("FormRemove");
            }
        }
        public ActionResult Remove(HeaderView_REC poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            if (poData.RecordID == null)
            {
                return View("Form");
            }
            else
            {
                var database = Constants.MongoClient.GetDatabase("IoT");
                var GetData = database.GetCollection<HeaderView_REC>("HaystackDatas");

                var builder = Builders<HeaderView_REC>.Filter;
                //var filter = builder.Eq<long>("RecordID", poData.RecordID);
                var filter = builder.Eq<string>("DeviceID", poData.DeviceID);
                var vResult = GetData.Find(filter).SingleOrDefault();

                if (vResult != null)
                {
                    GetData.DeleteOne(filter);
                    ResultData.Add("msg", "Success");
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                }
                else
                {
                    ResultData.Add("msg", "Failed remove : RecordID (" + poData.ids + ") is not exist.!");
                    ResultData.Add("errorcode", "100");
                    ResultData.Add("title", "Failed remove");
                }
                return Json(ResultData, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Read([DataSourceRequest]DataSourceRequest poRequest)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            var database = Constants.MongoClient.GetDatabase("IoT");
            var GetData = database.GetCollection<HeaderView_REC>("HaystackDatas");

            IMongoCollection<HeaderView_REC> listData = database.GetCollection<HeaderView_REC>("HaystackDatas");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<HeaderView_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            //if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            //{
            //    Dictionary<string, object> obj_filter = new Dictionary<string, object>();
            //    obj_filter.Add("RecordTimestamp", new BsonDocument() {
            //        { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
            //        { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
            //    });
            //    filter_mongo.AddRange(obj_filter);
            //}

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = Constants.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<HeaderView_REC> oTable = listData.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable;
            ds.Total = Convert.ToInt32(listData.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult Insert(Header_REC poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            var database = Constants.MongoClient.GetDatabase("IoT");
            var InsertData = database.GetCollection<Header_REC>("HaystackDatas");
            var GetData = database.GetCollection<HeaderView_REC>("HaystackDatas");

            var builder = Builders<HeaderView_REC>.Filter;
            var filter = builder.Eq<string>("DeviceID", poData.DeviceID);
            var resData = GetData.Find(filter).ToList();
            var contData = GetData.Find(_ => true).ToList();

            if (resData.Count == 0)
            {
                poData.RecordID = (contData.Count > 0) ? contData.OrderBy(x => x.RecordID).LastOrDefault().RecordID + 1 : 1;
                poData.ids = (poData.ids.Contains("@")) ? poData.ids.ToLower() : "@" + poData.ids.ToLower();
                poData.ids = (poData.ids != null) ? poData.ids.ToLower() : "";
                poData.dis = (poData.dis != null) ? poData.dis.ToLower() : "";
                poData.marker = (poData.marker != null) ? poData.marker.ToLower() : "";
                poData.RecordTimestamp = DateTime.Now;
                InsertData.InsertOne(poData);
                ResultData.Add("msg", "Success");
                ResultData.Add("errorcode", "0");
                ResultData.Add("title", "Success");
            }
            else
            {
                ResultData.Add("msg", "Failed insert : DeviceID (" + poData.DeviceID + ") is already exist.!");
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "Failed insert");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Update(Header_REC poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            var database = Constants.MongoClient.GetDatabase("IoT");
            var InsertData = database.GetCollection<Header_REC>("HaystackDatas");
            var GetData = database.GetCollection<HeaderView_REC>("HaystackDatas");

            var builder = Builders<HeaderView_REC>.Filter;
            //var filter = builder.Eq<long>("RecordID", poData.RecordID);
            var filter = builder.Eq<string>("DeviceID", poData.DeviceID);
            var resData = GetData.Find(filter).ToList();

            if (resData.Count > 0)
            {
                poData.ids = (poData.ids != null) ? poData.ids.ToLower() : "";
                poData.dis = (poData.dis != null) ? poData.dis.ToLower() : "";
                poData.marker = (poData.marker != null) ? poData.marker.ToLower() : "";
                poData.RecordTimestamp = DateTime.Now;
                if (poData.Detail != null)
                {
                    var update = Builders<HeaderView_REC>.Update.Set("Detail", poData.Detail)
                       .Set("RecordTimestamp", poData.RecordTimestamp).Set("ids", poData.ids).Set("dis", poData.dis).Set("marker", poData.marker).Set("DeviceID", poData.DeviceID);
                    GetData.UpdateOne(filter, update);
                }
                else
                {
                    var update = Builders<HeaderView_REC>.Update
                       .Set("RecordTimestamp", poData.RecordTimestamp).Set("ids", poData.ids).Set("dis", poData.dis).Set("marker", poData.marker).Set("DeviceID", poData.DeviceID);
                    GetData.UpdateOne(filter, update);
                }
                ResultData.Add("msg", "Success update");
                ResultData.Add("errorcode", "0");
                ResultData.Add("title", "Success");
            }
            else
            {
                var contData = GetData.Find(_ => true).ToList();
                poData.RecordID = (contData.Count > 0) ? contData.OrderBy(x => x.RecordID).LastOrDefault().RecordID + 1 : 1;
                poData.ids = (poData.ids.Contains("@")) ? poData.ids.ToLower() : "@" + poData.ids.ToLower();
                poData.ids = (poData.ids != null) ? poData.ids.ToLower() : "";
                poData.dis = (poData.dis != null) ? poData.dis.ToLower() : "";
                poData.marker = (poData.marker != null) ? poData.marker.ToLower() : "";
                poData.RecordTimestamp = DateTime.Now;
                InsertData.InsertOne(poData);

                ResultData.Add("msg", "Success insert");
                ResultData.Add("errorcode", "0");
                ResultData.Add("title", "Success");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
    }
}