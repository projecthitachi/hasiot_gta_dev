﻿using Uwb.Psg.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Uwb.Psg.Models
{
    public class Psg_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string CategoryID { get; set; }
        public string IPAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string StatusDevice { get; set; }
        public string URLRtsp { get; set; }
    }
    public class PsgModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public PsgModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t70209fieldselect"
        private string t70209fieldselect = @"
            t70209r001 as RecordID,
            t70209r002 as RecordTimestamp,
            t70209r003 as RecordStatus,
            t70209f001 as DeviceID,
            t70209f002 as DeviceName,
            t70209f003 as CategoryID,
            t70209f004 as IPAddress,
            t70209f005 as Username,
            t70209f006 as Password,
            t70209f007 as StatusDevice,
            t70209f008 as URLRtsp";
        #endregion

        public DbRawSqlQuery<Psg_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " SELECT "+t70209fieldselect+" FROM dbo.t70209 ORDER BY t70209r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<Psg_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Psg_REC> GetByGatewayID(DatabaseContext oRemoteDB, string sGatewayID)
        {
            string sSQL = " SELECT " + t70209fieldselect + " FROM dbo.t70209 where t70209f001 = '"+ sGatewayID + "' ORDER BY t70209r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<Psg_REC>(sSQL);
            return vQuery;
        }
        public bool Insert(DatabaseContext oRemoteDB, Psg_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                has.iot.Components.GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70209 " +
                    "      ( " +
                    "      t70209r002, t70209r003, " +
                    "      t70209f001, t70209f002, t70209f003, t70209f004, t70209f005, t70209f006, t70209f007, t70209f008" +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70209r002, @pt70209r003, " +
                    "      @pt70209f001, @pt70209f002, @pt70209f003, @pt70209f004, @pt70209f005, @pt70209f006, @pt70209f007, @pt70209f008" +
                    "      )" +
                    ";" +
                    "SELECT @pt70209r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70209r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70209r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70209r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70209f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70209f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt70209f003", poRecord.CategoryID));
                oParameters.Add(new SqlParameter("@pt70209f004", poRecord.IPAddress));
                oParameters.Add(new SqlParameter("@pt70209f005", poRecord.Username));
                oParameters.Add(new SqlParameter("@pt70209f006", poRecord.Password));
                oParameters.Add(new SqlParameter("@pt70209f007", poRecord.StatusDevice));
                oParameters.Add(new SqlParameter("@pt70209f008", poRecord.URLRtsp));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, Psg_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            has.iot.Components.GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70209 " +
                "   SET " +
                "      t70209f002 = @pt70209f002, " +
                "      t70209f003 = @pt70209f003, " +
                "      t70209f004 = @pt70209f004, " +
                "      t70209f005 = @pt70209f005, " +
                "      t70209f006 = @pt70209f006, " +
                "      t70209f007 = @pt70209f007, " +
                "      t70209f008 = @pt70209f008 " +
                "   WHERE (t70209r001 = @pt70209r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70209r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70209f002", poRecord.DeviceName));
            oParameters.Add(new SqlParameter("@pt70209f003", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70209f004", poRecord.IPAddress));
            oParameters.Add(new SqlParameter("@pt70209f005", poRecord.Username));
            oParameters.Add(new SqlParameter("@pt70209f006", poRecord.Password));
            oParameters.Add(new SqlParameter("@pt70209f007", poRecord.StatusDevice));
            oParameters.Add(new SqlParameter("@pt70209f008", poRecord.URLRtsp));
            //oParameters.Add(new SqlParameter("@pt70209f011", poRecord.StatusDevice));
            //oParameters.Add(new SqlParameter("@pt70209f012", poRecord.StatusSwitch));
            //oParameters.Add(new SqlParameter("@pt70209f013", poRecord.Live));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateStatus(DatabaseContext oRemoteDB, Psg_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            //GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70209 " +
                "   SET " +
                "      t70209f007 = @t70209f007 " +
                "   WHERE (t70209r001 = @pt70209r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70209r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70209f007", poRecord.StatusDevice));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, Psg_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t70209 " +
                "   WHERE (t70209r001 = @pt70209r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70209r001", poRecord.RecordID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }

    }
}