﻿using has.iot.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
namespace MVCPluggableDemo
{
    public class UwbPsgAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "UwbPsg";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "UwbPsg_default",
                "UwbPsg/{controller}/{action}/{id}",
                new {controller= "Psg", action = "Index", id = UrlParameter.Optional },
                new string[] { "Uwb.Psg.Controllers" }
            );   
        }
    }
}