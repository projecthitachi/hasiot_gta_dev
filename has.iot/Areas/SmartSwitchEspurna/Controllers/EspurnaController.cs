﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using has.iot.Components;
using Kendo.Mvc.Export;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SmartSwitch.Espurna.Components;
using SmartSwitch.Espurna.Models;
using SmartSwitch.Espurna.Models.System;
using Telerik.Documents.SpreadsheetStreaming;
using uPLibrary.Networking.M2Mqtt.Messages;
using MongoDB.Driver;
using MongoDB.Bson;
using Kendo.Mvc;
using System.Text.RegularExpressions;

namespace SmartSwitch.Espurna.Controllers
{
    public class EspurnaController : Controller
    {
        // GET: Espurna
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Log_Read([DataSourceRequest]DataSourceRequest poRequest, string DeviceID)
        {
            EspurnaModel oClass = new EspurnaModel();
            var vResult = oClass.GetLogList(DeviceID).ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        #region SQL CRUD
        public ActionResult ReadSql([DataSourceRequest]DataSourceRequest poRequest)
        {
            EspurnaModel oClass = new EspurnaModel();
            var vResult = oClass.GetList().ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateSql([DataSourceRequest]DataSourceRequest poRequest, Espurna_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    EspurnaModel oClass = new EspurnaModel();
                    if (!oClass.Insert(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSql([DataSourceRequest]DataSourceRequest poRequest, Espurna_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    EspurnaModel oClass = new EspurnaModel();
                    if (!oClass.Update(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroySql([DataSourceRequest]DataSourceRequest poRequest, Espurna_REC poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    EspurnaModel oClass = new EspurnaModel();
                    if (!oClass.Delete(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        #endregion

        #region MongoDB CRUD
        public ActionResult Read([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<SmartDevices_REC> Collections = database.GetCollection<SmartDevices_REC>("SmartDevices");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<SmartDevices_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                    { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<SmartDevices_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable.Where(x => x.Category == "Smartlight").ToList();
            ds.Total = Convert.ToInt32(Collections.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest]DataSourceRequest poRequest, SmartDevices_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();

                IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                IMongoCollection<SmartDevices_REC> Collections = database.GetCollection<SmartDevices_REC>("SmartDevices");
                try
                {
                    SmartDevices_REC voData = new SmartDevices_REC();
                    voData.RecordTimestamp = DateTime.Now;
                    voData.DeviceID = poRecord.DeviceID;
                    voData.DeviceName = poRecord.DeviceName;
                    voData.IPAddress = poRecord.IPAddress;
                    voData.ObjectName = poRecord.ObjectName;
                    voData.Location = poRecord.Location;
                    voData.SwitchNo = poRecord.SwitchNo;
                    voData.LightController = poRecord.LightController;
                    voData.Category = "Smartlight";
                    voData.Relay = 0;
                    Collections.InsertOne(voData);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        public ActionResult Update([DataSourceRequest]DataSourceRequest poRequest, SmartDevices_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();

                IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                IMongoCollection<SmartDevices_REC> Collections = database.GetCollection<SmartDevices_REC>("SmartDevices");
                try
                {
                    var builder = Builders<SmartDevices_REC>.Filter;
                    var filter = builder.Eq<string>("DeviceID", poRecord.DeviceID);
                    var update = Builders<SmartDevices_REC>.Update.Set("DeviceName", poRecord.DeviceName)
                    .Set("IPAddress",poRecord.IPAddress).Set("ObjectName",poRecord.ObjectName).Set("Location", poRecord.Location)
                    .Set("SwitchNo",poRecord.SwitchNo).Set("LightController",poRecord.LightController);
                    Collections.UpdateOne(filter, update);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest poRequest, SmartDevices_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                IMongoCollection<SmartDevices_REC> Collections = database.GetCollection<SmartDevices_REC>("SmartDevices");
                try
                {
                    var builder = Builders<SmartDevices_REC>.Filter;
                    var filter = builder.Eq<string>("DeviceID", poRecord.DeviceID);
                    Collections.DeleteOne(filter);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        #endregion

        public ActionResult Command(string cmd, string value)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();

            if (value != "All")
            {
                string strValue = Convert.ToString(cmd);
                Constants.MqttClient.Publish("Smartlight/Espurna/" + value + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            else
            {
                EspurnaModel oClass = new EspurnaModel();
                var vResult = oClass.GetList().ToList();
                foreach (Espurna_REC oData in vResult)
                {
                    string strValue = Convert.ToString(cmd);
                    Constants.MqttClient.Publish("Smartlight/Espurna/" + oData.IPAddress + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDeviceList([DataSourceRequest]DataSourceRequest poRequest, string plugin)
        {
            has.iot.Models.DeviceModel oClass = new has.iot.Models.DeviceModel();
            //var vResult = oClass.GetByPlugin("SmartSwitch").ToList();
            var vResult = oClass.GetList().ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        public static void Espurna_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            //string[] topic = e.Topic.Split('/');
            //string Category = topic[0];
            //if (topic.Length > 2)
            //{
            //    string Brand = topic[1];
            //    string hostDevice = topic[2];
            //    string prefix = topic[3];
            //    var message = System.Text.Encoding.Default.GetString(e.Message);

            //    if (GlobalFunction.IsValidJson(message))
            //    {
            //        var RT = GlobalHost.ConnectionManager.GetHubContext<RealTimeHub>();

            //        DatabaseContext oRemoteDB = new DatabaseContext();
            //        SmartSwitchModel oQuery = new SmartSwitchModel();

            //        if (prefix == "data") // SmartSwitch
            //        {
            //            object result = JsonConvert.DeserializeObject(message);
            //            JObject obj = JObject.Parse(result.ToString());
            //            string voltage = (obj["voltage"] == null) ? "0" : obj["voltage"].ToString().ToString();
            //            string current = (obj["current"] == null) ? "0" : obj["current"].ToString();
            //            string power = (obj["power"] == null) ? "0" : obj["power"].ToString();
            //            string apparent = (obj["apparent"] == null) ? "0" : obj["apparent"].ToString();
            //            string factor = (obj["factor"] == null) ? "0" : obj["factor"].ToString();
            //            string energy = (obj["energy"] == null) ? "0" : obj["energy"].ToString();
            //            string relay = (obj["relay/0"] == null) ? "" : obj["relay/0"].ToString();
            //            string host = (obj["host"] == null) ? "" : obj["host"].ToString();
            //            string ip = (obj["ip"] == null) ? "" : obj["ip"].ToString();
            //            string mac = (obj["mac"] == null) ? "" : obj["mac"].ToString();

            //            EspurnaDataLog_REC poRecords = new EspurnaDataLog_REC();
            //            poRecords.RecordTimestamp = DateTime.Now;
            //            poRecords.DeviceID = hostDevice;
            //            poRecords.Current = power;
            //            oQuery.DataLogInsert(oRemoteDB, poRecords);

            //            RT.Clients.All.Espurna("DataLog", message);

            //            //SmartSwitchhLog_REC poRecord = new SmartSwitchhLog_REC();
            //            //poRecord.DeviceID = hostDevice;
            //            //poRecord.DeviceName = "";
            //            //poRecord.Voltage = Convert.ToInt32(voltage);
            //            //poRecord.PowerFactor = Convert.ToInt32(factor);
            //            //poRecord.Energy = Convert.ToDecimal(energy);
            //            //poRecord.Current = Convert.ToDecimal(current);
            //            //poRecord.ActivePower = Convert.ToDecimal(power) / 60;
            //            //poRecord.ApparentPower = Convert.ToDecimal(apparent);

            //            ////SmartSwitchLogs(poRecord); // logs

            //            //poRecord.ActivePower = Convert.ToDecimal(power);
            //            //poRecord.ChartGauge = "chartgauge" + poRecord.DeviceID;
            //            //poRecord.ChartBar = "chartBar" + poRecord.DeviceID;
            //            //var json = new JavaScriptSerializer().Serialize(poRecord);
            //            //RT.Clients.All.Device("chart", json);
            //            //if (relay != "")
            //            //{
            //            //    string payload = "";
            //            //    if (relay == "1")
            //            //    {
            //            //        string bulblight = "";
            //            //        switch (hostDevice)
            //            //        {
            //            //            case "smartswitch01":
            //            //                bulblight = "800 lm (60W)";
            //            //                break;
            //            //            case "smartswitch02":
            //            //                bulblight = "800 lm (60W)";
            //            //                break;
            //            //            case "smartswitch03":
            //            //                bulblight = "3500 lm (300W)";
            //            //                break;
            //            //            case "smartswitch04":
            //            //                bulblight = "800 lm (60W)";
            //            //                break;
            //            //            case "smartswitch05":
            //            //                bulblight = "180 lm (25W)";
            //            //                break;
            //            //            default:
            //            //                bulblight = "1700 lm (100W)";
            //            //                break;
            //            //        }

            //            //        payload = "{\"luminousPower\": \"" + bulblight + "\", \"id\": \"" + hostDevice + "\"}";
            //            //    }
            //            //    else
            //            //    {
            //            //        payload = "{\"luminousPower\": \"Off\", \"id\": \"" + hostDevice + "\"}";
            //            //    }
            //            //    RT.Clients.All.broadcastMessage("bulbPower", payload);
            //            //    RT.Clients.All.ButtonSwitch(relay, json);
            //            //}
            //        }
            //    }
            //}
        }

        public ActionResult DataLog()
        {
            return View();
        }
        public ActionResult GetListDataLog([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<EspurnaDataLog_REC> Collections = database.GetCollection<EspurnaDataLog_REC>("SmartlightDataLogs");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<EspurnaDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                    { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<EspurnaDataLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable;
            ds.Total = Convert.ToInt32(Collections.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public IEnumerable<EspurnaDataLog_REC> GetListDataLogReport([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<EspurnaDataLog_REC> Collections = database.GetCollection<EspurnaDataLog_REC>("SmartswitchDataLogs");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<EspurnaDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();
            if (filter_date != null)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) },
                    { "$lt", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            IEnumerable<EspurnaDataLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).ToList();
            return oTable;
        }
        [HttpPost]
        public ActionResult ExportServer([DataSourceRequest]DataSourceRequest poRequest, ParameterExport data)
        {
            var columnsData = data.columns.ToArray();
            SpreadDocumentFormat exportFormat = data.options.format.ToString() == "csv" ? exportFormat = SpreadDocumentFormat.Csv : exportFormat = SpreadDocumentFormat.Xlsx;
            Action<ExportCellStyle> cellStyle = new Action<ExportCellStyle>(ChangeCellStyle);
            Action<ExportRowStyle> rowStyle = new Action<ExportRowStyle>(ChangeRowStyle);
            Action<ExportColumnStyle> columnStyle = new Action<ExportColumnStyle>(ChangeColumnStyle);

            string fileName = string.Format("{0}.{1}", data.options.title, data.options.format);
            string mimeType = Helpers.GetMimeType(exportFormat);

            Stream exportStream = exportFormat == SpreadDocumentFormat.Xlsx ?
                GetListDataLogReport(poRequest, data.transport).ToXlsxStream(columnsData) :
                GetListDataLogReport(poRequest, data.transport).ToCsvStream(columnsData);

            var fileStreamResult = new FileStreamResult(exportStream, mimeType);
            fileStreamResult.FileDownloadName = fileName;
            fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);
            using (var fileStream = System.IO.File.Create(Server.MapPath("~"+ Constants.ExportDirectory + fileName)))
            {
                fileStreamResult.FileStream.CopyTo(fileStream);
            }

            return Json(new { filename = fileName });
        }
        private void ChangeCellStyle(ExportCellStyle e)
        {
            bool isHeader = e.Row == 0;
            SpreadBorder border = new SpreadBorder(SpreadBorderStyle.Thick, new SpreadThemableColor(new SpreadColor(0, 0, 0)));
            SpreadCellFormat format = new SpreadCellFormat
            {
                //TopBorder = border,
                //BottomBorder = border,
                //LeftBorder = border,
                //RightBorder = border,
                //Fill = SpreadPatternFill.CreateSolidFill(new SpreadColor(255, 255, 255)),
                FontSize = 11,
                ForeColor = new SpreadThemableColor(new SpreadColor(0, 0, 0)),
                WrapText = false
            };
            e.Cell.SetFormat(format);
        }
        private void ChangeRowStyle(ExportRowStyle e)
        {
            e.Row.SetHeightInPixels(e.Index == 0 ? 80 : 30);
        }
        private void ChangeColumnStyle(ExportColumnStyle e)
        {
            double width = e.Name == "Item ID" || e.Name == "Barcode" ? 200 : 100;
            e.Column.SetWidthInPixels(width);
        }
    }
}