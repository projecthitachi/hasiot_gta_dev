﻿using has.iot.Components;
using SmartSwitch.Espurna.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace MVCPluggableDemo
{
    public class SmartSwitchEspurnaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SmartSwitchEspurna";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SmartSwitchEspurna_default",
                "SmartSwitchEspurna/{controller}/{action}/{id}",
                new {controller= "Espurna", action = "Index", id = UrlParameter.Optional },
                new string[] { "SmartSwitch.Espurna.Controllers" }
            );
            Constants.MqttClient = new MqttClient(GlobalFunction.mqttServer);
            Constants.MqttClient.Subscribe(new string[] { "Smartswitch/Espurna/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            Constants.MqttClient.MqttMsgPublishReceived += SmartSwitch.Espurna.Controllers.EspurnaController.Espurna_MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            Constants.MqttClient.Connect(clientId);
        }
    }
}