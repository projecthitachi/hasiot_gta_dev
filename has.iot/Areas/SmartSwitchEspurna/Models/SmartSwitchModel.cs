﻿using has.iot.Components;
using Kendo.Mvc;
using Kendo.Mvc.Export;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using SmartSwitch.Espurna.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SmartSwitch.Espurna.Models
{
    public class SmartLight_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string IPAddress { get; set; }
        public int Relay { get; set; }
        public int Status { get; set; }
        public int TotalUsage { get; set; }
    }

    public class SmartDevices_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string ObjectName { get; set; }
        public string Location { get; set; }
        public int SwitchNo { get; set; }
        public int LightController { get; set; }
        public string Category { get; set; }
        public int Relay { get; set; }
    }
    public class Espurna_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string CategoryID { get; set; }
        public string IPAddress { get; set; }
        public string MacAddress { get; set; }
        public string SSID { get; set; }
        public string Password { get; set; }
        public string MQTTHost { get; set; }
        public string MQTTUsername { get; set; }
        public string MQTTPassword { get; set; }
        public string StatusDevice { get; set; }
        public string StatusSwitch { get; set; }
        public int Live { get; set; }
    }
    public class EspurnaDataLog_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public virtual DateTime RecordTimestamp { get; set; }
        public virtual string DeviceID { get; set; }
        public virtual string DeviceName { get; set; }
        public virtual string IPAddress { get; set; }
        public virtual int Voltage { get; set; }
        public virtual int PowerFactor { get; set; }
        public virtual string Energy { get; set; }
        public virtual string Current { get; set; }
        public virtual int ActivePower { get; set; }
        public virtual int ApparentPower { get; set; }
    }
    public class EspurnaLog_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public string Energy { get; set; }
        public decimal Current { get; set; }
        public decimal ActivePower { get; set; }
        public decimal ApparentPower { get; set; }
        public string ChartGauge { get; set; }
        public string ChartBar { get; set; }
        public string GadGet { get; set; }
        public string fullcharger { get; set; }

    }
    public class EspurnaDefault_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string CategoryID { get; set; }
        public string IPAddress { get; set; }
        public string MacAddress { get; set; }
        public string SSID { get; set; }
        public string Password { get; set; }
        public string MQTTHost { get; set; }
        public string MQTTUsername { get; set; }
        public string MQTTPassword { get; set; }
        public string StatusDevice { get; set; }
        public string StatusSwitch { get; set; }
        public int Live { get; set; }
    }
    public class FilterDate
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
    public class ParameterExport
    {
        public IList<ExportColumnSettings> columns { get; set; }
        public ParameterExportOptions options { get; set; }
        public FilterDate transport { get; set; }
    }
    public class ParameterExportOptions
    {
        public string format { get; set; }
        public string title { get; set; }
    }
    public class EspurnaModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public EspurnaModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }

        #region "t70302fieldselect"
        private string t70302fieldselect = @"
		t70302r001 as RecordID,
		t70302r002 as RecordTimestamp,
		t70302r003 as RecordStatus,
		t70302f001 as BaseStationID,
		t70302f002 as DeviceID,
		t70302f003 as Payload";
        #endregion
        public DbRawSqlQuery<EspurnaDataLog_REC> GetListDataLog()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70302fieldselect + " FROM t70302 ORDER BY t70302r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<EspurnaDataLog_REC>(sSQL);
            return vQuery;
        }
        public bool DataLogInsert(DatabaseContext oRemoteDB, EspurnaDataLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                //poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;

                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70302 " +
                    "      ( " +
                    "      t70302r002, t70302r003, " +
                    "      t70302f002, t70302f003" +
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70302r002, @pt70302r003, " +
                    "      @pt70302f002, @pt70302f003" +
                    "      )" +
                    ";" +
                    "SELECT @pt70302r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70302r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70302r002", poRecord.RecordTimestamp));
                //oParameters.Add(new SqlParameter("@pt70302r003", poRecord.RecordStatus));

                //oParameters.Add(new SqlParameter("@pt70302f001", poRecord.BaseStationID));
                oParameters.Add(new SqlParameter("@pt70302f002", poRecord.DeviceID));
                //oParameters.Add(new SqlParameter("@pt70302f003", poRecord.Payload));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    //poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public DbRawSqlQuery<Espurna_REC> GetSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT tb.t70200f005 AS DeviceID FROM t70100 ta JOIN t70200 tb ON ta.t70100f001 = tb.t70200f001 WHERE ta.t70100f001 = 'SmartSwitch';";

            var vQuery = oRemoteDB.Database.SqlQuery<Espurna_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Espurna_REC> GetLastSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT TOP 1 t70202f001 AS DeviceID FROM t70202 ORDER BY t70202r002 DESC;";

            var vQuery = oRemoteDB.Database.SqlQuery<Espurna_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Espurna_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT t70202r001 AS RecordID, t70202r002 AS RecordTimestamp, t70202r003 AS RecordStatus, t70202f001 AS DeviceID, t70202f002 AS DeviceName, t70202f003 AS CategoryID, t70202f004 AS IPAddress, t70202f005 AS MacAddress, t70202f006 AS SSID, t70202f007 AS Password, t70202f008 AS MQTTHost, t70202f009 AS MQTTUsername, t70202f010 AS MQTTPassword, t70202f011 AS StatusDevice, t70202f012 AS StatusSwitch, t70202f013 AS Live FROM dbo.t70202 " +
                "ORDER BY t70202r001 DESC;";

            var vQuery = oRemoteDB.Database.SqlQuery<Espurna_REC>(sSQL);

            return vQuery;
        }

        public DbRawSqlQuery<EspurnaLog_REC> GetLogList(string DeviceID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT t9002r001 AS RecordID, t9002r002 AS RecordTimestamp, t9002r003 AS RecordStatus, t9002f001 AS DeviceID, t9002f002 AS DeviceName, t9002f003 AS Voltage, t9002f004 AS PowerFactor, t9002f005 AS Energy, t9002f006 AS 'Current', t9002f007 AS ActivePower, t9002f008 AS ApparentPower FROM dbo.t9002 " +
                " WHERE t9002f001 = '" + DeviceID + "' ORDER BY t9002r001 DESC;";

            var vQuery = oRemoteDB.Database.SqlQuery<EspurnaLog_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Espurna_REC> GetOne(long RecordID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT t70202r001 AS RecordID, t70202r002 AS RecordTimestamp, t70202r003 AS RecordStatus, t70202f001 AS DeviceID, t70202f002 AS DeviceName, t70202f003 AS CategoryID, t70202f004 AS IPAddress, t70202f005 AS MacAddress, t70202f006 AS SSID, t70202f007 AS Password, t70202f008 AS MQTTHost, t70202f009 AS MQTTUsername, t70202f010 AS MQTTPassword, t70202f011 AS StatusDevice, t70202f012 AS StatusSwitch, t70202f013 AS Live FROM dbo.t70202 " +
                "   WHERE t70202r001 = " + RecordID +
                ";"
                ;

            var vQuery = oRemoteDB.Database.SqlQuery<Espurna_REC>(sSQL);

            return vQuery;
        }

        public bool InsertLog(DatabaseContext oRemoteDB, EspurnaLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                //GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t9002 " +
                    "      ( " +
                    "      t9002r002, t9002r003, " +
                    "      t9002f001, t9002f002, t9002f003, t9002f004, t9002f005, t9002f006, t9002f007, t9002f008 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt9002r002, @pt9002r003, " +
                    "      @pt9002f001, @pt9002f002, @pt9002f003, @pt9002f004, @pt9002f005, @pt9002f006, @pt9002f007, @pt9002f008 " +
                    "      )" +
                    ";" +
                    "SELECT @pt9002r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt9002r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt9002r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt9002r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt9002f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt9002f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt9002f003", poRecord.Voltage));
                oParameters.Add(new SqlParameter("@pt9002f004", poRecord.PowerFactor));
                oParameters.Add(new SqlParameter("@pt9002f005", poRecord.Energy));
                oParameters.Add(new SqlParameter("@pt9002f006", poRecord.Current));
                oParameters.Add(new SqlParameter("@pt9002f007", poRecord.ActivePower));
                oParameters.Add(new SqlParameter("@pt9002f008", poRecord.ApparentPower));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Insert(DatabaseContext oRemoteDB, Espurna_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;
                if (GetLastSerialNo().ToList().Count == 0)
                {
                    poRecord.DeviceID = GetSerialNo().SingleOrDefault().DeviceID + ".0000.0001";
                }
                else
                {
                    string[] serialNo = GetLastSerialNo().SingleOrDefault().DeviceID.Split('.');
                    int lastNo = Convert.ToInt32(serialNo[3]) + 1;
                    poRecord.DeviceID = serialNo[0] + "." + serialNo[1] + "." + serialNo[2] + "." + lastNo.ToString("D4");

                }
                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70202 " +
                    "      ( " +
                    "      t70202r002, t70202r003, " +
                    "      t70202f001, t70202f002, t70202f003, t70202f004, t70202f005, t70202f006, t70202f007, t70202f008, t70202f009, t70202f010, t70202f011, t70202f012, t70202f013 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70202r002, @pt70202r003, " +
                    "      @pt70202f001, @pt70202f002, @pt70202f003, @pt70202f004, @pt70202f005, @pt70202f006, @pt70202f007, @pt70202f008, @pt70202f009, @pt70202f010, @pt70202f011, @pt70202f012, @pt70202f013 " +
                    "      )" +
                    ";" +
                    "SELECT @pt70202r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70202r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70202r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70202r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70202f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70202f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt70202f003", poRecord.CategoryID));
                oParameters.Add(new SqlParameter("@pt70202f004", poRecord.IPAddress));
                oParameters.Add(new SqlParameter("@pt70202f005", poRecord.MacAddress));
                oParameters.Add(new SqlParameter("@pt70202f006", poRecord.SSID));
                oParameters.Add(new SqlParameter("@pt70202f007", poRecord.Password));
                oParameters.Add(new SqlParameter("@pt70202f008", poRecord.MQTTHost));
                oParameters.Add(new SqlParameter("@pt70202f009", poRecord.MQTTUsername));
                oParameters.Add(new SqlParameter("@pt70202f010", poRecord.MQTTPassword));
                oParameters.Add(new SqlParameter("@pt70202f011", poRecord.StatusDevice));
                oParameters.Add(new SqlParameter("@pt70202f012", poRecord.StatusSwitch));
                oParameters.Add(new SqlParameter("@pt70202f013", poRecord.Live));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, Espurna_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70202 " +
                "   SET " +
                "      t70202f002 = @pt70202f002, " +
                "      t70202f003 = @pt70202f003, " +
                "      t70202f004 = @pt70202f004, " +
                "      t70202f005 = @pt70202f005, " +
                "      t70202f006 = @pt70202f006, " +
                "      t70202f007 = @pt70202f007, " +
                "      t70202f008 = @pt70202f008, " +
                "      t70202f009 = @pt70202f009, " +
                "      t70202f010 = @pt70202f010, " +
                "      t70202f013 = @pt70202f013 " +
                "   WHERE (t70202f001 = @pt70202f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70202f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70202f002", poRecord.DeviceName));
            oParameters.Add(new SqlParameter("@pt70202f003", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70202f004", poRecord.IPAddress));
            oParameters.Add(new SqlParameter("@pt70202f005", poRecord.MacAddress));
            oParameters.Add(new SqlParameter("@pt70202f006", poRecord.SSID));
            oParameters.Add(new SqlParameter("@pt70202f007", poRecord.Password));
            oParameters.Add(new SqlParameter("@pt70202f008", poRecord.MQTTHost));
            oParameters.Add(new SqlParameter("@pt70202f009", poRecord.MQTTUsername));
            oParameters.Add(new SqlParameter("@pt70202f010", poRecord.MQTTPassword));
            //oParameters.Add(new SqlParameter("@pt70202f011", poRecord.StatusDevice));
            //oParameters.Add(new SqlParameter("@pt70202f012", poRecord.StatusSwitch));
            oParameters.Add(new SqlParameter("@pt70202f013", poRecord.Live));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateStatus(DatabaseContext oRemoteDB, Espurna_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70202 " +
                "   SET " +
                "      t70202f011 = @pt70202f011, " +
                "      t70202f012 = @pt70202f012 " +
                "   WHERE (t70202f001 = @pt70202f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70202f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70202f011", poRecord.StatusDevice));
            oParameters.Add(new SqlParameter("@pt70202f012", poRecord.StatusSwitch));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, Espurna_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t70202 " +
                "   WHERE (t70202f001 = @pt70202f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70202f001", poRecord.DeviceID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }

    }

    public class DeviceAndPlugin_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string MacAddress { get; set; }
        public int GatewayID { get; set; }
        public string PluginID { get; set; }
        public string BrandID { get; set; }
        public string LocationID { get; set; }
        // plugin prop
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public string Payload { get; set; }
        public string Protocol { get; set; }
        public string Category { get; set; }
        public string Version { get; set; }

        //device addon field
        public string Status { get; set; }
    }
    public class ChartTopThree_REC
    {
        public string month { get; set; }
        public string date { get; set; }
        public decimal? device01 { get; set; }
        public decimal? device02 { get; set; }
        public decimal? device03 { get; set; }
        public decimal? device04 { get; set; }
        public decimal? device05 { get; set; }
        public decimal? saved01 { get; set; }
        public decimal? saved02 { get; set; }
        public decimal? saved03 { get; set; }
        public decimal? saved04 { get; set; }
        public decimal? saved05 { get; set; }
        public decimal? avg { get; set; }
    }
    public class ChartDonat_REC
    {
        public string Column { get; set; }
        public string GadgetID { get; set; }
        public string DeviceID { get; set; }
        public decimal Watt { get; set; }
    }
    public class SmartSwitchSum_REC
    {
        public decimal Dayly { get; set; }
        public decimal DaylyAvg { get; set; }
        public decimal Monthly { get; set; }
        public decimal MonthlyAvg { get; set; }
    }
    public class SmartSwitchhLog_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public string Energy { get; set; }
        public decimal Current { get; set; }
        public decimal ActivePower { get; set; }
        public decimal ApparentPower { get; set; }
        public string ChartGauge { get; set; }
        public string ChartBar { get; set; }
        public string GadGet { get; set; }
        public string fullcharger { get; set; }

    }
    public class CountData
    {
        public int count { get; set; }
    }
    public class SmartSwitchModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public SmartSwitchModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t70302fieldselect"
        private string t70302fieldselect = @"
            t70302r001 as RecordID,
            t70302r002 as RecordTimestamp,
            t70302r003 as RecordStatus,
            t70302f001 as BaseStationID,
            t70302f002 as DeviceID,
            t70302f003 as Payload,
            t70302f004 as 'Current' ";
        #endregion
        public DbRawSqlQuery<CountData> GetCountData(string table)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT COUNT(*) as count from " + table;
            var vQuery = oRemoteDB.Database.SqlQuery<CountData>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<EspurnaDataLog_REC> GetListDataLogReport()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70302fieldselect + " FROM t70302 ORDER BY t70302r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<EspurnaDataLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<EspurnaDataLog_REC> GetListDataLog([DataSourceRequest]DataSourceRequest poRequest)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            int start = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int end = poRequest.Page * poRequest.PageSize;

            string WhereClause = "";
            foreach (FilterDescriptor filter in poRequest.Filters)
            {
                WhereClause += filter.Member + " LIKE '%" + filter.Value + "%' AND ";
            }

            string Offset = "";
            string Limit = "";
            if (WhereClause.Length == 0)
            {
                Offset = "([table].RowNum BETWEEN " + start + " AND " + end + ")";
                Limit = "TOP(" + poRequest.PageSize + ")";
            }
            else
                WhereClause = WhereClause.Remove(WhereClause.LastIndexOf("AND"), 3);

            string sSQL = "SELECT " + Limit + "* " +
                "FROM(SELECT " + t70302fieldselect + ", ROW_NUMBER() OVER(ORDER BY t70302r001 DESC) AS RowNum " +
                "FROM t70302) AS [table] " +
                "WHERE " + WhereClause + Offset;

            var vQuery = oRemoteDB.Database.SqlQuery<EspurnaDataLog_REC>(sSQL);
            return vQuery;
        }
        public bool DataLogInsert(DatabaseContext oRemoteDB, EspurnaDataLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                //poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                //poRecord.RecordStatus = 0;

                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70302 " +
                    "      ( " +
                    "      t70302r002, t70302r003, " +
                    "      t70302f002, t70302f003, t70302f004" +
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70302r002, @pt70302r003, " +
                    "      @pt70302f002, @pt70302f003, @pt70302f004" +
                    "      )" +
                    ";" +
                    "SELECT @pt70302r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70302r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70302r002", poRecord.RecordTimestamp));
                //oParameters.Add(new SqlParameter("@pt70302r003", poRecord.RecordStatus));

                //oParameters.Add(new SqlParameter("@pt70302f001", poRecord.BaseStationID));
                oParameters.Add(new SqlParameter("@pt70302f002", poRecord.DeviceID));
                //oParameters.Add(new SqlParameter("@pt70302f003", poRecord.Payload));
                oParameters.Add(new SqlParameter("@pt70302f004", poRecord.Current));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    //poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        public DbRawSqlQuery<SmartSwitchSum_REC> GetDaylyToday(string DeviceID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = "SELECT " +
            " (SELECT ISNULL(CAST(SUM(ta.t9002f007) / 1000 AS DECIMAL(18,2)),0.00) AS ActivePower FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), GETDATE(), 111) AND ta.t9002f001 = '" + DeviceID + "') as Dayly,  " +
            " (SELECT ISNULL(CAST((SUM(ta.t9002f007) / 1000) / 24 AS DECIMAL(18,2)),0.00) AS ActivePower FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), GETDATE(), 111) AND ta.t9002f001 = '" + DeviceID + "') as DaylyAvg,  " +
            " (SELECT ISNULL(CAST(SUM(ta.t9002f007) / 1000 AS DECIMAL(18,2)),0.00) AS ActivePower FROM t9002 ta WHERE DATEPART(MONTH, ta.t9002r002) = DATEPART(MONTH, GETDATE()) AND ta.t9002f001 = '" + DeviceID + "' ) AS Monthly,  " +
            " (SELECT ISNULL(CAST((SUM(ta.t9002f007) / 1000) / 720 AS DECIMAL(18,2)),0.00) AS ActivePower FROM t9002 ta WHERE DATEPART(MONTH, ta.t9002r002) = DATEPART(MONTH, GETDATE()) AND ta.t9002f001 = '" + DeviceID + "') AS MonthlyAvg ";

            var vQuery = oRemoteDB.Database.SqlQuery<SmartSwitchSum_REC>(sSQL);

            return vQuery;
        }
        public bool Insert(DatabaseContext oRemoteDB, SmartSwitchhLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                //GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t9002 " +
                    "      ( " +
                    "      t9002r002, t9002r003, " +
                    "      t9002f001, t9002f002, t9002f003, t9002f004, t9002f005, t9002f006, t9002f007, t9002f008 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt9002r002, @pt9002r003, " +
                    "      @pt9002f001, @pt9002f002, @pt9002f003, @pt9002f004, @pt9002f005, @pt9002f006, @pt9002f007, @pt9002f008 " +
                    "      )" +
                    ";" +
                    "SELECT @pt9002r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt9002r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt9002r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt9002r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt9002f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt9002f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt9002f003", poRecord.Voltage));
                oParameters.Add(new SqlParameter("@pt9002f004", poRecord.PowerFactor));
                oParameters.Add(new SqlParameter("@pt9002f005", poRecord.Energy));
                oParameters.Add(new SqlParameter("@pt9002f006", poRecord.Current));
                oParameters.Add(new SqlParameter("@pt9002f007", poRecord.ActivePower));
                oParameters.Add(new SqlParameter("@pt9002f008", poRecord.ApparentPower));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        public DbRawSqlQuery<ChartDonat_REC> GetChartTopThreeDonat()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " SELECT TOP 3 CONCAT('device',SUBSTRING(ta.t9002f001, LEN(ta.t9002f001)-1, 2)) AS 'Column', CONCAT('SmartLight',SUBSTRING(ta.t9002f001, LEN(ta.t9002f001)-1, 2)) AS GadgetID, ta.t9002f001 AS DeviceID, SUM (ta.t9002f007) AS Watt FROM t9002 ta WHERE	ta.t9002f001 NOT IN ('smartplug01','smartplug02') GROUP BY	ta.t9002f001 ORDER BY	SUM (ta.t9002f007) DESC  ";
            var vQuery = oRemoteDB.Database.SqlQuery<ChartDonat_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartDonat_REC> GetChartTopThreeDonatPlug()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT TOP 3 ta.t9002f001 AS DeviceID, SUM(ta.t9002f007) AS Watt FROM t9002 ta WHERE ta.t9002f001 IN ('smartplug01','smartplug02') GROUP BY ta.t9002f001 ORDER BY SUM(ta.t9002f007) DESC   ";
            var vQuery = oRemoteDB.Database.SqlQuery<ChartDonat_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartTopThree_REC> GetChartSumarry(List<ChartDonat_REC> vTopThree, string Type)
        {
            int i = 1;
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " declare @month int, @year int set @month = 4 set @year = 2019 " +
            " SELECT CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date], ";
            foreach (ChartDonat_REC voTopThree in vTopThree)
            {
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 = '" + voTopThree.DeviceID + "') as [device0" + i + "],";
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) + (ISNULL(sum(ta.t9002f007),0.00) /2) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 = '" + voTopThree.DeviceID + "') as [saved0" + i + "], ";
                i++;
            }
            sSQL += " (SELECT ISNULL(sum(ta.t9002f007) / 5,0.00) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 NOT IN ('smartplug01','smartplug02')) as [avg] " +
            " FROM master..spt_values WHERE type = 'P' AND (CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ) < DATEADD(mm,1,CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) )  ";
            if (Type == "monthly")
            {
                i = 1;
                sSQL = " WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11) " +
                      " SELECT CONCAT(DATENAME(MONTH,DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR,DATEADD(MONTH,-N,GETDATE())) ) AS [month]," +
                      " CONVERT (VARCHAR (10),DATEADD(MONTH,-N,GETDATE()),111) AS [date], ";
                foreach (ChartDonat_REC voTopThree in vTopThree)
                {
                    sSQL += " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())) AND ta.t9002f001 = '" + voTopThree.DeviceID + "') as [device0" + i + "], ";
                    i++;
                }
                sSQL += " (SELECT ISNULL(sum(ta.t9002f007) / 5, 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())) AND ta.t9002f001 NOT IN ('smartplug01','smartplug02')) as [avg] FROM R  ORDER BY N DESC ";
            }
            var vQuery = oRemoteDB.Database.SqlQuery<ChartTopThree_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartTopThree_REC> GetChartSummaryByDeviceID(string DeviceID, string type)
        {
            string sSQL = "";
            decimal voSaved = Convert.ToDecimal(0.1);
            switch (DeviceID)
            {
                case "SmartLight01":
                    voSaved = Convert.ToDecimal(0.22);
                    break;
                case "SmartLight02":
                    voSaved = Convert.ToDecimal(0.24);
                    break;
                case "SmartLight03":
                    voSaved = Convert.ToDecimal(0.21);
                    break;
                case "SmartLight04":
                    voSaved = Convert.ToDecimal(0.23);
                    break;
                case "SmartLight05":
                    voSaved = Convert.ToDecimal(0.20);
                    break;
                default:
                    voSaved = Convert.ToDecimal(0.22);
                    break;
            }
            DatabaseContext oRemoteDB = new DatabaseContext();
            if (DeviceID == "Average")
            {
                sSQL = " declare @month int, @year int set @month = 4 set @year = 2019 " +
                " SELECT CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date], ";
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) ) as [device01],";
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + ") FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) ) as [saved01] ";
                sSQL += " FROM master..spt_values WHERE type = 'P' AND (CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ) < DATEADD(mm,1,CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) )  ";
            }
            else
            {
                sSQL = " declare @month int, @year int set @month = 4 set @year = 2019 " +
                " SELECT CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date], ";
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 = 'smartswitch" + DeviceID.Substring(DeviceID.Length - 2) + "') as [device01],";
                sSQL += "(SELECT ISNULL(sum(ta.t9002f007),0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + ") FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 = 'smartswitch" + DeviceID.Substring(DeviceID.Length - 2) + "') as [saved01] ";
                sSQL += " FROM master..spt_values WHERE type = 'P' AND (CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ) < DATEADD(mm,1,CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) )  ";
            }

            if (type == "monthly")
            {
                if (DeviceID != "Average")
                {
                    sSQL = "WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11)  " +
                    " SELECT CONCAT(DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR, DATEADD(MONTH, -N, GETDATE())) ) AS[month],  " +
                    " CONVERT(VARCHAR(10), DATEADD(MONTH, -N, GETDATE()), 111) AS[date],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = 'smartswitch" + DeviceID.Substring(DeviceID.Length - 2) + "') as [device01],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + ") FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = 'smartswitch" + DeviceID.Substring(DeviceID.Length - 2) + "') as [saved01]  " +
                    " FROM R ORDER BY N DESC";
                }
                else
                {
                    sSQL = "WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11)  " +
                    " SELECT CONCAT(DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR, DATEADD(MONTH, -N, GETDATE())) ) AS[month],  " +
                    " CONVERT(VARCHAR(10), DATEADD(MONTH, -N, GETDATE()), 111) AS[date],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 NOT IN ('smartplug01', 'smartplug02')) as [device01],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + ") FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 NOT IN ('smartplug01', 'smartplug02')) as [saved01]  " +
                    " FROM R ORDER BY N DESC";
                }
            }
            var vQuery = oRemoteDB.Database.SqlQuery<ChartTopThree_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartTopThree_REC> GetChartTopThreePlug(string Type)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "declare @month int, @year int set @month = 4 set @year = 2019 SELECT  " +
            " CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date],  " +
            " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 = 'smartplug01') as [device01],  " +
            " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 = 'smartplug02') as [device02],  " +
            " (SELECT ISNULL(sum(ta.t9002f007),0.00) + (ISNULL(sum(ta.t9002f007),0.00) /2) FROM t9002 ta WHERE CONVERT(VARCHAR (10), ta.t9002r002,111) = CONVERT(VARCHAR (10),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ,111) AND ta.t9002f001 = 'smartplug01') as [saved01], " +
            " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) / 2) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 = 'smartplug02') as [saved02], " +
            " (SELECT ISNULL(sum(ta.t9002f007) / 2, 0.00) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 IN('smartplug01','smartplug02')) as [avg]  " +
            " FROM master..spt_values WHERE type = 'P' AND(CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number) < DATEADD(mm, 1, CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME)) ";
            if (Type == "monthly")
            {
                sSQL = "WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11)  " +
                " SELECT CONCAT(DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR, DATEADD(MONTH, -N, GETDATE())) ) AS[month],  " +
                " CONVERT(VARCHAR(10), DATEADD(MONTH, -N, GETDATE()), 111) AS[date],  " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = 'smartplug01') as [device01],  " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = 'smartplug02') as [device02],  " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00) / 2 FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 IN('smartplug01', 'smartplug02')) as [avg]  " +
                " FROM R ORDER BY N DESC";
            }

            var vQuery = oRemoteDB.Database.SqlQuery<ChartTopThree_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartTopThree_REC> GetSmartPlugSaving(string DeviceID, string Type)
        {
            string sSQL = "";
            decimal voSaved = Convert.ToDecimal(0.1);
            switch (DeviceID)
            {
                case "SmartPlug01":
                    voSaved = Convert.ToDecimal(0.2);
                    break;
                case "SmartPlug02":
                    voSaved = Convert.ToDecimal(0.25);
                    break;
                default:
                    voSaved = Convert.ToDecimal(0.22);
                    break;
            }
            DatabaseContext oRemoteDB = new DatabaseContext();
            if (DeviceID != "Average")
            {
                sSQL = "declare @month int, @year int set @month = 4 set @year = 2019 SELECT  " +
                " CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date],  " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 = '" + DeviceID + "') as [device01], " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + ") FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 = '" + DeviceID + "') as [saved01] " +
                " FROM master..spt_values WHERE type = 'P' AND(CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number) < DATEADD(mm, 1, CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME)) ";
            }
            else
            {
                sSQL = "declare @month int, @year int set @month = 4 set @year = 2019 SELECT  " +
                " CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) as [date],  " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00)  FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 IN('smartplug01','smartplug02')) as [device01], " +
                " (SELECT ISNULL(sum(ta.t9002f007), 0.00)  + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + " ) FROM t9002 ta WHERE CONVERT(VARCHAR(10), ta.t9002r002, 111) = CONVERT(VARCHAR(10), CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number, 111) AND ta.t9002f001 IN('smartplug01','smartplug02')) as [saved01] " +
                " FROM master..spt_values WHERE type = 'P' AND(CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number) < DATEADD(mm, 1, CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME)) ";
            }

            if (Type == "monthly")
            {
                if (DeviceID != "Average")
                {
                    sSQL = "WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11)  " +
                    " SELECT CONCAT(DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR, DATEADD(MONTH, -N, GETDATE())) ) AS[month],  " +
                    " CONVERT(VARCHAR(10), DATEADD(MONTH, -N, GETDATE()), 111) AS[date],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = '" + DeviceID + "') as [device01],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + " ) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 = '" + DeviceID + "') as [saved01]  " +
                    " FROM R ORDER BY N DESC";
                }
                else
                {
                    sSQL = "WITH R(N) AS (SELECT 0 UNION ALL SELECT N+1 FROM R WHERE N < 11)  " +
                    " SELECT CONCAT(DATENAME(MONTH, DATEADD(MONTH,-N,GETDATE())), ' ',DATENAME(YEAR, DATEADD(MONTH, -N, GETDATE())) ) AS[month],  " +
                    " CONVERT(VARCHAR(10), DATEADD(MONTH, -N, GETDATE()), 111) AS[date],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 IN('smartplug01', 'smartplug02')) as [device01],  " +
                    " (SELECT ISNULL(sum(ta.t9002f007), 0.00) + (ISNULL(sum(ta.t9002f007), 0.00) * " + voSaved + " ) FROM t9002 ta WHERE DATENAME(MONTH, ta.t9002r002) = DATENAME(MONTH, DATEADD(MONTH, -N, GETDATE())) AND ta.t9002f001 IN('smartplug01', 'smartplug02')) as [saved01]  " +
                    " FROM R ORDER BY N DESC";
                }
            }
            var vQuery = oRemoteDB.Database.SqlQuery<ChartTopThree_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ChartTopThree_REC> GetSmartPlugGadget(string DeviceID)
        {
            string sSQL = "";
            DatabaseContext oRemoteDB = new DatabaseContext();
            if (DeviceID == "SmartPlug02")
            {
                sSQL = "SELECT 'SmartPhone' AS [date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.424 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.106 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01] " +
                " UNION SELECT 'Laptop' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.2475 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.0825 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01] " +
                " UNION SELECT 'Unknown' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.1014 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.0286 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01]";

            }
            if (DeviceID == "SmartPlug01")
            {
                sSQL = "SELECT 'SmartPhone' AS [date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.1014 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.0286 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01] " +
                " UNION SELECT 'Laptop' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.2475 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.0825 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01] " +
                " UNION SELECT 'Unknown' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.424 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.106 FROM t9002 ta WHERE ta.t9002f001 = '" + DeviceID + "') as [saved01]";
            }
            if (DeviceID == "Average")
            {
                sSQL = "SELECT 'SmartPhone' AS [date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.344 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug01') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.086 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug01') as [saved01] " +
                " UNION SELECT 'Laptop' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.104 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug02') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.026 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug01') as [saved01] " +
                " UNION SELECT 'Unknown' AS[date], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.344 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug01') as [device01], (SELECT ISNULL(sum(ta.t9002f007), 0.00) * 0.086 FROM t9002 ta WHERE ta.t9002f001 = 'smartplug02') as [saved01]";
            }
            var vQuery = oRemoteDB.Database.SqlQuery<ChartTopThree_REC>(sSQL);
            return vQuery;
        }
    }
}