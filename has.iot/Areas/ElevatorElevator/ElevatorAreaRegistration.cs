﻿using Elevator.Elevator.Components;
using Elevator.Elevator.Models;
using has.iot.Components;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Web.Configuration;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace MVCPluggableDemo
{
    public class ElevatorAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ElevatorElevator";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ElevatorElevator_default",
                "ElevatorElevator/{controller}/{action}/{id}",
                new {controller= "Elevator", action = "Index", id = UrlParameter.Optional },
                new string[] { "Elevator.Elevator.Controllers" }
            );
            Constants.MqttClient = new MqttClient(WebConfigurationManager.AppSettings["server_mqtt"].ToString());
            //Constants.MqttClient.Subscribe(new string[] { "#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            //Constants.MqttClient.MqttMsgPublishReceived += Elevator.Elevator.Controllers.ElevatorController.Elevator_MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            Constants.MqttClient.Connect(clientId);

            //Constants.isGatewayReturned = false;
            //Thread thr = new Thread(() => SendCommandRepeat());
            //thr.Start();
        }

        public void SendCommandRepeat()
        {
            while (!Constants.isGatewayReturned)
            {
                List<cmds> ListcmdData = new List<cmds>();
                cmdDevice voResData = new cmdDevice();

                cmds cmdHBData = new cmds();
                cmdHBData.a = 511;
                cmdHBData.d = 255;
                ListcmdData.Add(cmdHBData);
                Double now = (Double)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                voResData.timeStamp = now.ToString("0");
                voResData.command = ListcmdData;
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                Constants.MqttClient.Publish("M2L/sg/hitachiasia/G0053350", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                Thread.Sleep(5000);// 5 seconds
            }
        }

    }
}