﻿using Elevator.Elevator.Models.System;
using has.iot.Components;
using Kendo.Mvc.Export;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Elevator.Elevator.Models
{
    public class Elevator_REC//t70220
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Elevator { get; set; }
        public string StatusDevice { get; set; }
        public int Live { get; set; }
    }
    public class ElevatorDataLog_REC//t70320
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [Display(Name = "RecordID")]
        public long RecordID { get; set; }

        [Display(Name = "RecordTimestamp")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }

        [Display(Name = "Record Status")]
        public Int32 RecordStatus { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Type")]
        public string Type { get; set; }

        [Display(Name = "Elevator")]
        public string Elevator { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        [Display(Name = "Payload")]
        public string Payload { get; set; }

        [Display(Name = "Command Type")]
        public string CommandType { get; set; }
    }
    public class ElevatorObject_REC//t70221
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [Display(Name = "RecordID")]
        public long RecordID { get; set; }

        [Display(Name = "RecordTimestamp")]
        public DateTime RecordTimestamp { get; set; }

        [Display(Name = "Record Status")]
        public Int32 RecordStatus { get; set; }

        [Display(Name = "Table")]
        public string Table { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Type")]
        public string Type { get; set; }

        [Display(Name = "DeviceID")]
        public string DeviceID { get; set; }

        [Display(Name = "Elevator")]
        public int Elevator { get; set; }

        [Display(Name = "Floor")]
        public List<int> Floor { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        [Display(Name = "a")]
        public int a { get; set; }

        [Display(Name = "d")]
        public int d { get; set; }

        [UIHint("ClientCategory")]
        public CategoryView_REC Category
        {
            get;
            set;
        }
    }

    public class ElevatorLastCommandData_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [Display(Name = "RecordTimestamp")]
        public DateTime RecordTimestamp { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Type")]
        public string Type { get; set; }

        [Display(Name = "a")]
        public int a { get; set; }
        [Display(Name = "d")]
        public int d { get; set; }
    }

    public class cmdDevice
    {
        public string timeStamp { get; set; }
        public List<cmds> command { get; set; }
    }

    public class cmds
    {
        public int a { get; set; }
        public int d { get; set; }
    }

    public class CategoryView_REC
    {
        public string CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
    public class ParameterExport
    {
        public IList<ExportColumnSettings> columns { get; set; }
        public ParameterExportOptions options { get; set; }
        public FilterDate transport { get; set; }
    }
    public class ParameterExportOptions
    {
        public string format { get; set; }
        public string title { get; set; }
    }
    public class FilterDate
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
    public class ElevatorModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public ElevatorModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t70320fieldselect"
        private string t70320fieldselect = @"
            t70320r001 as RecordID,
            t70320r002 as RecordTimestamp,
            t70320r003 as RecordStatus,
            t70320f001 as Address,
            t70320f002 as Description,
            t70320f003 as Type,
            t70320f004 as Elevator,
            t70320f005 as Remarks,
            t70320f006 as Payload";
        #endregion
        public DbRawSqlQuery<ElevatorDataLog_REC> GetListDataLog(string DeviceID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70320fieldselect + " FROM t70320 ORDER BY t70320r001 DESC;";
            if (DeviceID != null)
            {
                sSQL = "SELECT " + t70320fieldselect + " FROM t70320 WHERE t70320f004 = '"+DeviceID+"' ORDER BY t70320r001 DESC;";
            }
            var vQuery = oRemoteDB.Database.SqlQuery<ElevatorDataLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ElevatorDataLog_REC> GetListDataLog()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70320fieldselect + " FROM t70320 ORDER BY t70320r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ElevatorDataLog_REC>(sSQL);
            return vQuery;
        }
        public bool DataLogInsert(DatabaseContext oRemoteDB, ElevatorDataLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                //poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;
                GlobalFunction.TrimNull(poRecord);
                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70320 " +
                    "      ( " +
                    "      t70320r002, t70320r003, " +
                    "      t70320f001, t70320f002, t70320f003, "+
                    "      t70320f004, t70320f005, t70320f006 "+
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70320r002, @pt70320r003, " +
                    "      @pt70320f001, @pt70320f002, @pt70320f003, " +
                    "      @pt70320f004, @pt70320f005, @pt70320f006 " +
                    "      )" +
                    ";" +
                    "SELECT @pt70320r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70320r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70320r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70320r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70320f001", poRecord.Address));
                oParameters.Add(new SqlParameter("@pt70320f002", poRecord.Description));
                oParameters.Add(new SqlParameter("@pt70320f003", poRecord.Type));
                oParameters.Add(new SqlParameter("@pt70320f004", poRecord.Elevator));
                oParameters.Add(new SqlParameter("@pt70320f005", poRecord.Remarks));
                oParameters.Add(new SqlParameter("@pt70320f006", poRecord.Payload));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        #region "t70220fieldselect"
        private string t70220fieldselect = @"
            t70220r001 as RecordID,
            t70220r002 as RecordTimestamp,
            t70220r003 as RecordStatus,
            t70220f001 as DeviceID,
            t70220f002 as DeviceName,
            t70220f003 as StatusDevice,
            t70220f004 as Live,
            t70220f005 as Elevator";
        #endregion
        #region "t70221fieldselect"
        private string t70221fieldselect = @"
            t70221r001 as RecordID,
            t70221r002 as RecordTimestamp,
            t70221r003 as RecordStatus,
            t70221f001 as Address,
            t70221f002 as Description,
            t70221f003 as Type,
            t70221f004 as DeviceID,
            t70221f005 as Elevator,
            t70221f006 as Remarks,
            t70221f007 as [Table]";
        #endregion
        public DbRawSqlQuery<ElevatorObject_REC> GetListDataObject()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70221fieldselect + " FROM t70221 ORDER BY t70221r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ElevatorObject_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ElevatorObject_REC> GetListDataObject(string Table)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70221fieldselect + " FROM t70221 WHERE t70221f007 = '" + Table + "' ORDER BY t70221r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ElevatorObject_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ElevatorObject_REC> GetListDLLObject(string Table, string voText)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70221fieldselect + " FROM t70221 WHERE t70221f007 = '" + Table + "' AND t70221f002 LIKE '%" + voText + "%' AND t70221f002 NOT IN ('','-')  ORDER BY t70221r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ElevatorObject_REC>(sSQL);
            return vQuery;
        }
        public bool ObjectInsert(DatabaseContext oRemoteDB, ElevatorObject_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;
                GlobalFunction.TrimNull(poRecord);
                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70221 " +
                    "      ( " +
                    "      t70221r002, t70221r003, " +
                    "      , t70221f007, t70221f001, t70221f002, t70221f003, t70221f004, t70221f005, t70221f006, t70221f007 " +
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70221r002, @pt70221r003, " +
                    "      @pt70221f007, @pt70221f001, @pt70221f002, @pt70221f003, @pt70221f004, @pt70221f005, @pt70221f006, @pt70221f007 " +
                    "      )" +
                    ";" +
                    "SELECT @pt70221r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70221r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70221r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70221r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70221f001", poRecord.Address));
                oParameters.Add(new SqlParameter("@pt70221f002", poRecord.Description));
                oParameters.Add(new SqlParameter("@pt70221f003", poRecord.Type));
                oParameters.Add(new SqlParameter("@pt70221f004", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70221f005", poRecord.Elevator));
                oParameters.Add(new SqlParameter("@pt70221f006", poRecord.Remarks));
                oParameters.Add(new SqlParameter("@pt70221f007", poRecord.Table));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool ObjectUpdate(DatabaseContext oRemoteDB, ElevatorObject_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            poRecord.RecordTimestamp = DateTime.Now;
            GlobalFunction.TrimNull(poRecord);
            //----------
            //oRemoteDB.TrimNull(poRecord);
            string sSQL = "" +
            "UPDATE t70221 " +
            "   SET " +
            "      t70221f002 = @pt70221f002, " +
            "      t70221f003 = @pt70221f003, " +
            "      t70221f004 = @pt70221f004, " +
            "      t70221f005 = @pt70221f005, " +
            "      t70221f006 = @pt70221f006, " +
            "      t70221f007 = @pt70221f007 " +
            "   WHERE (t70221r001 = @pt70221r001) " +
            ";"
            ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70221r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70221r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70221r003", poRecord.RecordStatus));

            oParameters.Add(new SqlParameter("@pt70221f001", poRecord.Address));
            oParameters.Add(new SqlParameter("@pt70221f002", poRecord.Description));
            oParameters.Add(new SqlParameter("@pt70221f003", poRecord.Type));
            oParameters.Add(new SqlParameter("@pt70221f004", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70221f005", poRecord.Elevator));
            oParameters.Add(new SqlParameter("@pt70221f006", poRecord.Remarks));
            oParameters.Add(new SqlParameter("@pt70221f007", poRecord.Table));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            //------------------------------
            return bReturn;
        }
        public bool ObjectDelete(DatabaseContext oRemoteDB, ElevatorObject_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t70221 " +
                "   WHERE (t70221f001 = @pt70221f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70221f001", poRecord.Address));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        public DbRawSqlQuery<Elevator_REC> GetSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " SELECT tb.t70200f005 AS DeviceID FROM t70100 ta JOIN t70200 tb ON ta.t70100f001 = tb.t70200f001 WHERE ta.t70100f001 = 'Elevator';";
            var vQuery = oRemoteDB.Database.SqlQuery<Elevator_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Elevator_REC> GetLastSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " SELECT TOP 1 t70220f001 AS DeviceID FROM t70220 ORDER BY t70220r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<Elevator_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Elevator_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT "+t70220fieldselect+" FROM dbo.t70220 " +
                "ORDER BY t70220f005 ASC;";

            var vQuery = oRemoteDB.Database.SqlQuery<Elevator_REC>(sSQL);

            return vQuery;
        }

        public DbRawSqlQuery<Elevator_REC> GetOne(string Elevator)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " SELECT "+ t70220fieldselect + " FROM dbo.t70220 WHERE t70220f005 = '"+ Elevator + "' ;";
            var vQuery = oRemoteDB.Database.SqlQuery<Elevator_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Elevator_REC> GetForLookup(string voType, string voText)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT "+t70220fieldselect+" FROM t70220 " +
                "   WHERE  t70220f002 LIKE '%" + voText + "%'  " +
                "   ORDER BY t70220f001 ASC" +
                ";"
                ;

            var vQuery = oRemoteDB.Database.SqlQuery<Elevator_REC>(sSQL);

            return vQuery;
        }
        public bool Insert(DatabaseContext oRemoteDB, Elevator_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;
                if (GetLastSerialNo().ToList().Count == 0)
                {
                    poRecord.DeviceID = GetSerialNo().SingleOrDefault().DeviceID+".0000.0001";
                }
                else
                {
                    string[] serialNo = GetLastSerialNo().SingleOrDefault().DeviceID.Split('.');
                    int lastNo = Convert.ToInt32(serialNo[3]) + 1;
                    poRecord.DeviceID = serialNo[0]+"."+ serialNo[1]+"."+ serialNo[2]+"."+lastNo.ToString("D4");

                }
                //----------
                GlobalFunction.TrimNull(poRecord);
                
                string sSQL = "" +
                    "INSERT INTO t70220 " +
                    "      ( " +
                    "      t70220r002, t70220r003, " +
                    "      t70220f001, t70220f002, t70220f003, t70220f004, t70220f005 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70220r002, @pt70220r003, " +
                    "      @pt70220f001, @pt70220f002, @pt70220f003, @pt70220f004, @pt70220f005" +
                    "      )" +
                    ";" +
                    "SELECT @pt70220r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70220r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70220r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70220r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70220f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70220f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt70220f003", poRecord.StatusDevice));
                oParameters.Add(new SqlParameter("@pt70220f004", poRecord.Live));
                oParameters.Add(new SqlParameter("@pt70220f005", poRecord.Elevator));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, Elevator_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70220 " +
                "   SET " +
                "      t70220f002 = @pt70220f002, " +
                "      t70220f003 = @pt70220f003, " +
                "      t70220f004 = @pt70220f004, " +
                "      t70220f005 = @pt70220f005 " +
                "   WHERE (t70220f001 = @pt70220f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70220f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70220f002", poRecord.DeviceName));
            oParameters.Add(new SqlParameter("@pt70220f003", poRecord.StatusDevice));
            oParameters.Add(new SqlParameter("@pt70220f004", poRecord.Live));
            oParameters.Add(new SqlParameter("@pt70220f005", poRecord.Elevator));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateStatus(DatabaseContext oRemoteDB, Elevator_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70220 " +
                "   SET " +
                "      t70220f004 = @pt70220f004 " +
                "   WHERE (t70220f001 = @pt70220f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70220f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70220f004", poRecord.StatusDevice));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, Elevator_REC poRecord)
        {
            bool bReturn = true;
            try { 
                string sSQL = "" +
                    "DELETE FROM t70220 " +
                    "   WHERE (t70220f001 = @pt70220f001) " +
                    ";"
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter("@pt70220f001", poRecord.DeviceID));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            }catch(Exception e)
            {
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool updateStatusObj(DatabaseContext oRemoteDB, string RecordID, string Table)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;

            string sSQL = " UPDATE t70221 SET t70221r003 = 0 WHERE t70221f007 = @pt70221f007; ";
            if (RecordID != null)
            {
                sSQL += " UPDATE t70221 SET t70221r003 = 1 WHERE t70221f007 = @pt70220r001 AND t70221r001 IN(" + RecordID + ");";
            }
            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70221f007", Table));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            //------------------------------
            return bReturn;
        }
    }
}