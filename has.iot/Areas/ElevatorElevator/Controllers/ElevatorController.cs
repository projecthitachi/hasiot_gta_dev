﻿using has.iot.Components;
using Elevator.Elevator.Models.System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Elevator.Elevator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using uPLibrary.Networking.M2Mqtt.Messages;
using Kendo.Mvc.Export;
using Telerik.Documents.SpreadsheetStreaming;
using System.IO;
using System.Text;
using Elevator.Elevator.Components;
using MongoDB.Driver;
using MongoDB.Bson;
using Kendo.Mvc;
using System.Text.RegularExpressions;

namespace Elevator.Elevator.Controllers
{
    public class ElevatorController : Controller
    {
        // GET: Elevator
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ConfigService()
        {
            return View();
        }
        public ActionResult cmdParkingOperation()
        {
            return View();
        }
        public ActionResult cmdPeakOperation()
        {
            return View();
        }
        public ActionResult cmdStandby()
        {
            return View();
        }
        public ActionResult cmdHeartbeat()
        {
            return View();
        }
        public ActionResult DataTable()
        {
            return View();
        }

        public ActionResult GetLokkuplist(string Type, string text)
        {
            ElevatorModel oClass = new ElevatorModel();
            var voTable = oClass.GetForLookup(Type, text);
            return Json(voTable, JsonRequestBehavior.AllowGet);
        }

        #region CRUD
        public ActionResult Read([DataSourceRequest]DataSourceRequest poRequest)
        {
            ElevatorModel oClass = new ElevatorModel();
            var vResult = oClass.GetList().ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest]DataSourceRequest poRequest, Elevator_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    ElevatorModel oClass = new ElevatorModel();
                    if (!oClass.Insert(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest]DataSourceRequest poRequest, Elevator_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    ElevatorModel oClass = new ElevatorModel();
                    if (!oClass.Update(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest poRequest, Elevator_REC poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    ElevatorModel oClass = new ElevatorModel();
                    if (!oClass.Delete(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        #endregion

        #region object instance
        public ActionResult ObjectRead([DataSourceRequest]DataSourceRequest poRequest, string DeviceID)
        {
            //ElevatorModel oClass = new ElevatorModel();
            //var vResult = oClass.GetListDataObject(DeviceID).ToList();
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ElevatorObject_REC> ElevatorDataObject = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");
            var filter = new BsonDocument();
            if (!String.IsNullOrEmpty(DeviceID))
                filter.AddRange(new Dictionary<string, object>() { { "Table", DeviceID } });

            IList<ElevatorObject_REC> vResult = ElevatorDataObject.Find(filter).ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }
        public ActionResult ObjectReadDLL(string DeviceID, string Type, string text)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ElevatorObject_REC> ElevatorDataObject = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");
            var filter = new BsonDocument();

            filter.AddRange(new Dictionary<string, object>() {
                { "$and", new BsonArray(){new BsonDocument(){
                    {"Description", new BsonDocument(){
                        {"$ne", ""}
                    }}
                },new BsonDocument(){
                    {"Description", new BsonDocument(){
                        {"$ne", "-"}
                    }}
                }}}
            });

            filter.AddRange(new Dictionary<string, object>() { { "Table", DeviceID } });
            filter.AddRange(new Dictionary<string, object>() { { "Type", Type } });
            filter.AddRange(new Dictionary<string, object>() { { "Description", BsonRegularExpression.Create(new Regex(text, RegexOptions.IgnoreCase)) } });

            var sort = new BsonDocument();
            sort.AddRange(new BsonDocument() { { "Address", 1 } });

            IList <ElevatorObject_REC> vResult = ElevatorDataObject.Find(filter).Sort(sort).ToList();

            return Json(vResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ObjectPeakOperation()
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ElevatorObject_REC> ElevatorDataObject = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");
            var filter = new BsonDocument();
            
            filter.AddRange(new Dictionary<string, object>() { { "Table", "G0053350" } });
            filter.AddRange( new BsonDocument() {
                {
                    "Address", new BsonDocument(){
                        { "$in", new BsonArray() { "81","82" } }
                    }
                }
            } );

            var sort = new BsonDocument();
            sort.AddRange(new BsonDocument() { { "Address", 1 } });

            IList<ElevatorObject_REC> vResult = ElevatorDataObject.Find(filter).Sort(sort).ToList();

            return Json(vResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ObjectHeatbeat()
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ElevatorObject_REC> ElevatorDataObject = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");
            var filter = new BsonDocument();

            filter.AddRange(new BsonDocument() {
                {
                    "Address", new BsonDocument(){
                        { "$in", new BsonArray() { "1FE","1FF" } }
                    }
                }
            });

            var sort = new BsonDocument();
            sort.AddRange(new BsonDocument() { { "Address", 1 } });

            IList<ElevatorObject_REC> vResult = ElevatorDataObject.Find(filter).Sort(sort).ToList();

            return Json(vResult, JsonRequestBehavior.AllowGet);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ObjectUpdate([DataSourceRequest]DataSourceRequest poRequest, [Bind(Prefix = "models")]IEnumerable<ElevatorObject_REC> poRecord)
        {
            Dictionary<string, string> vsLogMessage = new Dictionary<string, string>();
            //DatabaseContext oRemoteDB = new DatabaseContext();
            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            try
            {
                //ElevatorModel oClass = new ElevatorModel();
                //foreach (ElevatorObject_REC voObj in poRecord)
                //{
                //    if (!oClass.ObjectUpdate(oRemoteDB, voObj))
                //    {
                //        throw new Exception(oClass.ErrorMessage);
                //    }
                //}
                //oTransaction.Commit();

                IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                IMongoCollection<ElevatorObject_REC> ElevatorDataObject = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");

                foreach (ElevatorObject_REC voObj in poRecord)
                {
                    var filter = new BsonDocument() {
                        { "Address", voObj.Address },
                        { "Table", voObj.Table }
                    };
                    var update = Builders<ElevatorObject_REC>.Update
                                .Set(p => p.RecordTimestamp, DateTime.Now)
                                .Set(p => p.RecordStatus, voObj.RecordStatus)
                                .Set(p => p.Address, String.IsNullOrEmpty(voObj.Address) ? "" : voObj.Address)
                                .Set(p => p.Description, String.IsNullOrEmpty(voObj.Description) ? "" : voObj.Description)
                                .Set(p => p.DeviceID, String.IsNullOrEmpty(voObj.DeviceID) ? "" : voObj.DeviceID)
                                .Set(p => p.Elevator, voObj.Elevator)
                                .Set(p => p.Remarks, String.IsNullOrEmpty(voObj.Remarks) ? "" : voObj.Remarks)
                                .Set(p => p.Table, String.IsNullOrEmpty(voObj.Table) ? "" : voObj.Table)
                                .Set(p => p.Type, String.IsNullOrEmpty(voObj.Type) ? "" : voObj.Type)
                                .Set(p => p.a, voObj.a)
                                .Set(p => p.d, voObj.d);

                    ElevatorDataObject.UpdateOne(filter,update);
                }

                vsLogMessage.Add("errorcode", "0");
                vsLogMessage.Add("title", "Success");
                vsLogMessage.Add("msg", "Update Data seccess");
            }
            catch (Exception ex)
            {
                //oTransaction.Rollback();
                vsLogMessage.Add("errorcode", "100");
                vsLogMessage.Add("title", "warning");
                vsLogMessage.Add("msg", ex.Message);
            }

            return Json(vsLogMessage, JsonRequestBehavior.DenyGet);
        }
        #endregion

        #region MQTT
        public ActionResult Commands(List<string> aList, List<string> dList)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                /*IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                IMongoCollection<ElevatorObject_REC> ElevatorDataObject = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");
                IMongoCollection<ElevatorLastCommandData_REC> ElevatorLastCommandData = database.GetCollection<ElevatorLastCommandData_REC>("ElevatorLastCommandData");*/
                cmdDevice voResData = new cmdDevice();
                List<cmds> ListcmdData = new List<cmds>();
                
                for (int i = 0; i < aList.Count; i++)
                {
                    cmds cmdData = new cmds();
                    int a = int.Parse(aList[i], System.Globalization.NumberStyles.HexNumber);
                    ushort d = Convert.ToUInt16(dList[i], 2);
                    cmdData.a = a;
                    cmdData.d = d;

                    ListcmdData.Add(cmdData);

                    /*var filter = new BsonDocument(){
                        {"Table", "G0053350"},
                        {"Address", aList[i]},
                        {"Type", new BsonDocument(){ { "$ne", "" } } },
                        {"Description", new BsonDocument(){ { "$ne", "-" } } }
                    };
                    ElevatorObject_REC CommandData = ElevatorDataObject.Find(filter).SingleOrDefault();
                    if(CommandData != null)
                    {
                        var update = Builders<ElevatorObject_REC>.Update
                            .Set(p => p.RecordTimestamp, DateTime.Now)
                            .Set(p => p.d, cmdData.d);
                        
                        ElevatorDataObject.UpdateOne(filter, update);
                    }*/
                }

                Double now = (Double)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                voResData.timeStamp = now.ToString("0");
                voResData.command = ListcmdData;

                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                Constants.MqttClient.Publish("M2L/sg/hitachiasia/G0053350", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                
                ResultData.Add("msg", "Success");
                ResultData.Add("errorcode", "0");
                ResultData.Add("title", "Success");
            }catch(Exception ex)
            {
                ResultData.Add("msg", ex.Message);
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "Error");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public static void Elevator_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ElevatorDataLog_REC> ElevatorDataLog = database.GetCollection<ElevatorDataLog_REC>("ElevatorDataLog");
            IMongoCollection<ElevatorObject_REC> ElevatorDataObject = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");
            
            string[] topic = e.Topic.Split('/');
            string Category = topic[0]; //L2M = Lift to Mqtt, M2L = Mqtt to Lift
            if (topic.Length > 2 && (Category == "L2M" || Category == "M2L"))
            {
                var message = System.Text.Encoding.Default.GetString(e.Message);
                if (GlobalFunction.IsValidJson(message))
                {
                    var RT = GlobalHost.ConnectionManager.GetHubContext<RealTimeHub>();
                    object result = JsonConvert.DeserializeObject(message);
                    JObject voobj = JObject.Parse(result.ToString());
                    ElevatorModel oQuery = new ElevatorModel();

                    string country = topic[1];
                    string project_name = topic[2];
                    string Table = topic[3];
                    if (Category == "Error")
                    {

                    }
                    else if (Category == "L2M")//Subscribe
                    {
                        var rootresult = ElevatorDataObject.Find(_ => true).ToList();
                        if (rootresult.Count > 0)
                        {
                            if (voobj["status"] != null)
                            {
                                JArray voArray = JArray.Parse(voobj["status"].ToString());
                                string timestamp = voobj["timeStamp"].ToString();
                                for (int i = 0; i < voArray.Count; i++)
                                {
                                    JObject poobj = JObject.Parse(voArray[i].ToString());
                                    string a = poobj["a"].ToString();
                                    int d = Convert.ToInt32(poobj["d"].ToString());
                                    if (a == "510" && d == 255)//M2L
                                    {
                                        #region "Response from Remote System"
                                        /* Add Response from Remote System */
                                        Double now = (Double)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                                        cmdDevice voResData = new cmdDevice();
                                        List<cmds> ListcmdData = new List<cmds>();

                                        cmds cmdHBData = new cmds();
                                        cmdHBData.a = 510;
                                        cmdHBData.d = 255;
                                        ListcmdData.Add(cmdHBData);
                                        /* Add Response from Remote System */

                                        /* get last status */
                                        var filter = new BsonDocument(){
                                            {"Table", "G0053350"},
                                            {"Type", new BsonDocument(){ { "$ne", "" } } },
                                            {"Description", new BsonDocument(){ { "$ne", "-" } } },
                                            {"d", new BsonDocument() { { "$ne", 0} } }
                                        };

                                        var sort = new BsonDocument(){
                                            {"Address", 1}
                                        };
                                        List<ElevatorObject_REC> lastdata = ElevatorDataObject.Find(filter).Sort(sort).ToList();
                                        if (lastdata.Count > 0)
                                        {
                                            foreach (ElevatorObject_REC temp in lastdata)
                                            {
                                                cmds cmdTemp = new cmds();
                                                cmdTemp.a = temp.a;
                                                cmdTemp.d = temp.d;
                                                ListcmdData.Add(cmdTemp);
                                            }
                                        }
                                        /* get last status */

                                        voResData.timeStamp = now.ToString("0");
                                        voResData.command = ListcmdData;
                                        string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                                        Constants.MqttClient.Publish("M2L/sg/hitachiasia/G0053350", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                        #endregion

                                        ElevatorDataLog_REC OBdatalog = new ElevatorDataLog_REC();
                                        OBdatalog.RecordTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToDouble(timestamp));
                                        OBdatalog.Address = a;
                                        OBdatalog.Description = "Heartbeat";
                                        OBdatalog.Elevator = d.ToString();
                                        OBdatalog.Type = "Heartbeat";
                                        OBdatalog.Payload = message;
                                        OBdatalog.CommandType = "L2M";
                                        OBdatalog.Remarks = "Request from Gateway [L2M]";
                                        //oQuery.DataLogInsert(oRemoteDB, OBdatalog);
                                        ElevatorDataLog.InsertOne(OBdatalog);
                                    }
                                    else if (a == "511" && d == 255)
                                    {
                                        ElevatorDataLog_REC OBdatalog = new ElevatorDataLog_REC();
                                        OBdatalog.RecordTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToDouble(timestamp));
                                        OBdatalog.Address = a;
                                        OBdatalog.Description = "Heartbeat";
                                        OBdatalog.Elevator = d.ToString();
                                        OBdatalog.Type = "Heartbeat";
                                        OBdatalog.Payload = message;
                                        OBdatalog.CommandType = "L2M";
                                        OBdatalog.Remarks = "Response from Gateway [L2M]";
                                        //oQuery.DataLogInsert(oRemoteDB, OBdatalog);
                                        ElevatorDataLog.InsertOne(OBdatalog);

                                        Constants.isGatewayReturned = true;//stop the loop 5seconds
                                    }
                                    else
                                    {
                                        int intValueA = Convert.ToInt32(a);
                                        string hexValueA = intValueA.ToString("X");
                                        if (hexValueA.Length == 1) { hexValueA = hexValueA.PadLeft(2, '0'); }
                                        string BinValueD = IntToBinaryString(d);

                                        ElevatorObject_REC dataObj = rootresult.FindAll(f => f.Table == Table && f.Address == hexValueA).SingleOrDefault();
                                        if (dataObj != null)
                                        {
                                            if (dataObj.Type == "B")
                                            {
                                                List<string> CarLoadAddress = new List<string>() { "41", "49", "51", "59" };
                                                if (!CarLoadAddress.Contains(dataObj.Address))
                                                {
                                                    List<int> poFloor = new List<int>();
                                                    poFloor.Add(d);
                                                    dataObj.Floor = poFloor;

                                                    ElevatorDataLog_REC OBdatalog = new ElevatorDataLog_REC();
                                                    OBdatalog.RecordTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToDouble(timestamp));
                                                    OBdatalog.Address = dataObj.Address;
                                                    OBdatalog.Description = dataObj.Description;
                                                    OBdatalog.Elevator = dataObj.Elevator.ToString();
                                                    OBdatalog.Type = dataObj.Type;
                                                    OBdatalog.Payload = message;
                                                    OBdatalog.CommandType = "L2M";
                                                    OBdatalog.Remarks = "Floor " + d;
                                                    //oQuery.DataLogInsert(oRemoteDB, OBdatalog);
                                                    ElevatorDataLog.InsertOne(OBdatalog);
                                                }
                                                else
                                                {
                                                    //Remarks Floor jadi Load
                                                    List<int> poFloor = new List<int>();
                                                    poFloor.Add(d);
                                                    dataObj.Floor = poFloor;

                                                    ElevatorDataLog_REC OBdatalog = new ElevatorDataLog_REC();
                                                    OBdatalog.RecordTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToDouble(timestamp));
                                                    OBdatalog.Address = dataObj.Address;
                                                    OBdatalog.Description = dataObj.Description;
                                                    OBdatalog.Elevator = dataObj.Elevator.ToString();
                                                    OBdatalog.Type = dataObj.Type;
                                                    OBdatalog.Payload = message;
                                                    OBdatalog.CommandType = "L2M";
                                                    OBdatalog.Remarks = "Load " + d;
                                                    //oQuery.DataLogInsert(oRemoteDB, OBdatalog);
                                                    ElevatorDataLog.InsertOne(OBdatalog);
                                                }
                                            }
                                            else if (dataObj.Type == "K")
                                            {
                                                BinValueD = BinValueD.PadLeft(8, '0');
                                                char[] array = BinValueD.ToCharArray();
                                                List<int> poFloor = new List<int>();
                                                int ta = 7;

                                                for (int t = 0; t < array.Length; t++)
                                                {
                                                    int volift = t + 1;
                                                    Elevator_REC voRec = oQuery.GetOne(volift.ToString()).SingleOrDefault();
                                                    if (voRec != null)
                                                    {
                                                        ElevatorDataLog_REC OBdatalog = new ElevatorDataLog_REC();
                                                        OBdatalog.RecordTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToDouble(timestamp));
                                                        OBdatalog.Address = dataObj.Address;
                                                        OBdatalog.Description = dataObj.Description;
                                                        OBdatalog.Elevator = volift.ToString();
                                                        OBdatalog.Type = dataObj.Type;
                                                        OBdatalog.Payload = message;
                                                        OBdatalog.CommandType = "L2M";

                                                        int letter = Convert.ToInt32(array[ta].ToString());
                                                        if (letter == 1)
                                                        {
                                                            OBdatalog.Remarks = "On";
                                                        }
                                                        else
                                                        {
                                                            OBdatalog.Remarks = "Off";
                                                        }
                                                        //oQuery.DataLogInsert(oRemoteDB, OBdatalog);
                                                        ElevatorDataLog.InsertOne(OBdatalog);
                                                    }

                                                    ta--;
                                                }
                                            }
                                            else if (dataObj.Type == "F")
                                            {
                                                ElevatorDataLog_REC OBdatalog = new ElevatorDataLog_REC();
                                                OBdatalog.RecordTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToDouble(timestamp));
                                                OBdatalog.Address = dataObj.Address;
                                                OBdatalog.Description = dataObj.Description;
                                                OBdatalog.Elevator = dataObj.Elevator.ToString();
                                                OBdatalog.Type = dataObj.Type;
                                                OBdatalog.Payload = message;
                                                OBdatalog.CommandType = "L2M";

                                                BinValueD = BinValueD.PadLeft(8, '0');
                                                dataObj.Floor = getFloor(BinValueD);
                                                string val = "Floor ";
                                                foreach (int voFloor in dataObj.Floor)
                                                {
                                                    val += " " + voFloor + ", ";
                                                }
                                                OBdatalog.Remarks = val;
                                                //oQuery.DataLogInsert(oRemoteDB, OBdatalog);
                                                ElevatorDataLog.InsertOne(OBdatalog);
                                            }
                                        }
                                        else
                                        {

                                        }
                                    }
                                }
                                RT.Clients.All.Elevator("DataLog", "");
                            }
                        }
                    }
                    else if (Category == "M2L")//publish
                    {
                        var rootresult = ElevatorDataObject.Find(_ => true).ToList();
                        if (rootresult.Count > 0)
                        {
                            if (voobj["command"] != null)
                            {
                                JArray voArray = JArray.Parse(voobj["command"].ToString());
                                string timestamp = voobj["timeStamp"].ToString();
                                for (int i = 0; i < voArray.Count; i++)
                                {
                                    JObject poobj = JObject.Parse(voArray[i].ToString());
                                    string a = poobj["a"].ToString();
                                    int d = Convert.ToInt32(poobj["d"].ToString());
                                    if (a == "511" && d == 255)
                                    {
                                        ElevatorDataLog_REC OBdatalog = new ElevatorDataLog_REC();
                                        OBdatalog.RecordTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToDouble(timestamp));
                                        OBdatalog.Address = a;
                                        OBdatalog.Description = "Heartbeat";
                                        OBdatalog.Elevator = d.ToString();
                                        OBdatalog.Type = "Heartbeat";
                                        OBdatalog.Payload = message;
                                        OBdatalog.CommandType = "M2L";
                                        OBdatalog.Remarks = "Request from Remote System [M2L]";
                                        //oQuery.DataLogInsert(oRemoteDB, OBdatalog);
                                        ElevatorDataLog.InsertOne(OBdatalog);
                                    }
                                    else if (a == "510" && d == 255)
                                    {
                                        ElevatorDataLog_REC OBdatalog = new ElevatorDataLog_REC();
                                        OBdatalog.RecordTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToDouble(timestamp));
                                        OBdatalog.Address = a;
                                        OBdatalog.Description = "Heartbeat";
                                        OBdatalog.Elevator = d.ToString();
                                        OBdatalog.Type = "Heartbeat";
                                        OBdatalog.Payload = message;
                                        OBdatalog.CommandType = "M2L";
                                        OBdatalog.Remarks = "Response from Remote System [M2L]";
                                        //oQuery.DataLogInsert(oRemoteDB, OBdatalog);
                                        ElevatorDataLog.InsertOne(OBdatalog);
                                    }
                                    else
                                    {
                                        int intValueA = Convert.ToInt32(a);
                                        string hexValueA = intValueA.ToString("X");
                                        if (hexValueA.Length == 1) { hexValueA = hexValueA.PadLeft(2, '0'); }
                                        string BinValueD = IntToBinaryString(d);

                                        ElevatorObject_REC dataObj = rootresult.FindAll(f => f.Table == Table && f.Address == hexValueA).SingleOrDefault();
                                        if (dataObj != null)
                                        {
                                            if (dataObj.Type == "B")
                                            {
                                                List<int> poFloor = new List<int>();
                                                poFloor.Add(d);
                                                dataObj.Floor = poFloor;

                                                ElevatorDataLog_REC OBdatalog = new ElevatorDataLog_REC();
                                                OBdatalog.RecordTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToDouble(timestamp));
                                                OBdatalog.Address = dataObj.Address;
                                                OBdatalog.Description = dataObj.Description;
                                                OBdatalog.Elevator = dataObj.Elevator.ToString();
                                                OBdatalog.Type = dataObj.Type;
                                                OBdatalog.Payload = message;
                                                OBdatalog.CommandType = "M2L";

                                                string[] standby_elevator_key = { "42", "4A", "52", "5A", "44", "4C", "54", "5C", "46", "4E", "56", "5E" };
                                                if (standby_elevator_key.Contains(dataObj.Address))
                                                    OBdatalog.Remarks = "Elevator " + d;
                                                else
                                                    OBdatalog.Remarks = "Floor " + d;
                                                //oQuery.DataLogInsert(oRemoteDB, OBdatalog);
                                                ElevatorDataLog.InsertOne(OBdatalog);
                                            }
                                            else if (dataObj.Type == "K")
                                            {
                                                BinValueD = BinValueD.PadLeft(8, '0');
                                                char[] array = BinValueD.ToCharArray();
                                                List<int> poFloor = new List<int>();
                                                int ta = 7;

                                                for (int t = 0; t < array.Length; t++)
                                                {
                                                    int volift = t + 1;
                                                    Elevator_REC voRec = oQuery.GetOne(volift.ToString()).SingleOrDefault();
                                                    if (voRec != null)
                                                    {
                                                        ElevatorDataLog_REC OBdatalog = new ElevatorDataLog_REC();
                                                        OBdatalog.RecordTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToDouble(timestamp));
                                                        OBdatalog.Address = dataObj.Address;
                                                        OBdatalog.Description = dataObj.Description;
                                                        OBdatalog.Elevator = volift.ToString();
                                                        OBdatalog.Type = dataObj.Type;
                                                        OBdatalog.Payload = message;
                                                        OBdatalog.CommandType = "M2L";

                                                        int letter = Convert.ToInt32(array[ta].ToString());
                                                        if (letter == 1)
                                                        {
                                                            OBdatalog.Remarks = "On";
                                                        }
                                                        else
                                                        {
                                                            OBdatalog.Remarks = "Off";
                                                        }
                                                        //oQuery.DataLogInsert(oRemoteDB, OBdatalog);
                                                        ElevatorDataLog.InsertOne(OBdatalog);
                                                    }

                                                    ta--;
                                                }
                                            }
                                        }
                                        else
                                        {

                                        }
                                    }
                                }
                                RT.Clients.All.Elevator("DataLog", "");
                            }
                            else if (voobj["status"] != null)
                            {

                            }
                        }
                    }
                }
            }
        }
        private static List<int> getFloor(string BinValueD)
        {
            char[] array = BinValueD.ToCharArray();
            List<int> poFloor = new List<int>();
            int ta = 7;
            for (int t = 0; t < array.Length; t++)
            {
                int letter = Convert.ToInt32(array[ta].ToString());
                if (letter == 1)
                {
                    poFloor.Add(t + 1);
                }
                ta--;
            }
            return poFloor;
        }
        private static string IntToBinaryString(int number)
        {
            const int mask = 1;
            var binary = string.Empty;
            while (number > 0)
            {
                // Logical AND the number and prepend it to the result string
                binary = (number & mask) + binary;
                number = number >> 1;
            }

            return binary;
        }
        #endregion

        #region Datalog
        public ActionResult DataLog()
        {
            return View();
        }
        
        public ActionResult GetListDataLog([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            //ElevatorModel oModel = new ElevatorModel();
            //IList<ElevatorDataLog_REC> oTable = oModel.GetListDataLog(DeviceID).ToList();

            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ElevatorDataLog_REC> Collections = database.GetCollection<ElevatorDataLog_REC>("ElevatorDataLog");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<ElevatorDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                    { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<ElevatorDataLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable;
            ds.Total = Convert.ToInt32(Collections.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public IEnumerable<ElevatorDataLog_REC> GetListDataLogReport([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            /*ElevatorModel oClass = new ElevatorModel();
            IEnumerable<ElevatorDataLog_REC> vResult = oClass.GetListDataLog().ToList();
            return vResult;*/
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ElevatorDataLog_REC> Collections = database.GetCollection<ElevatorDataLog_REC>("ElevatorDataLog");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<ElevatorDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();
            if (filter_date != null)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) },
                    { "$lt", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            IEnumerable<ElevatorDataLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).ToList();
            return oTable;
        }
        [HttpPost]
        public ActionResult ExportServer([DataSourceRequest]DataSourceRequest poRequest, ParameterExport data)
        {
            var columnsData = data.columns.ToArray();
            SpreadDocumentFormat exportFormat = data.options.format.ToString() == "csv" ? exportFormat = SpreadDocumentFormat.Csv : exportFormat = SpreadDocumentFormat.Xlsx;
            Action<ExportCellStyle> cellStyle = new Action<ExportCellStyle>(ChangeCellStyle);
            Action<ExportRowStyle> rowStyle = new Action<ExportRowStyle>(ChangeRowStyle);
            Action<ExportColumnStyle> columnStyle = new Action<ExportColumnStyle>(ChangeColumnStyle);

            string fileName = string.Format("{0}.{1}", data.options.title, data.options.format);
            string mimeType = Helpers.GetMimeType(exportFormat);

            Stream exportStream = exportFormat == SpreadDocumentFormat.Xlsx ?
                GetListDataLogReport(poRequest, data.transport).ToXlsxStream(columnsData) :
                GetListDataLogReport(poRequest, data.transport).ToCsvStream(columnsData);

            var fileStreamResult = new FileStreamResult(exportStream, mimeType);
            fileStreamResult.FileDownloadName = fileName;
            fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);
            using (var fileStream = System.IO.File.Create(Server.MapPath("~" + Constants.ExportDirectory + fileName)))
            {
                fileStreamResult.FileStream.CopyTo(fileStream);
            }

            return Json(new { filename = fileName });
        }
        private void ChangeCellStyle(ExportCellStyle e)
        {
            bool isHeader = e.Row == 0;
            SpreadBorder border = new SpreadBorder(SpreadBorderStyle.Thick, new SpreadThemableColor(new SpreadColor(0, 0, 0)));
            SpreadCellFormat format = new SpreadCellFormat
            {
                FontSize = 11,
                ForeColor = new SpreadThemableColor(new SpreadColor(0, 0, 0)),
                WrapText = false
            };
            e.Cell.SetFormat(format);
        }
        private void ChangeRowStyle(ExportRowStyle e)
        {
            e.Row.SetHeightInPixels(e.Index == 0 ? 80 : 30);
        }
        private void ChangeColumnStyle(ExportColumnStyle e)
        {
            double width = e.Name == "Item ID" || e.Name == "Barcode" ? 200 : 100;
            e.Column.SetWidthInPixels(width);
        }
        #endregion
    }
}