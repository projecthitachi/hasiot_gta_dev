﻿using Camera.CCTV.Hikvision.Models;
using Camera.CCTV.Hikvision.Models.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Camera.CCTV.Hikvision.Components
{
    public class Constants
    {
        public static CCTVHikvisionDefault_REC GetDefaultSetting()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            CCTVModel oQuery = new CCTVModel();
            CCTVHikvisionDefault_REC voData = oQuery.GetDefaultSetting(oRemoteDB).SingleOrDefault();
            return voData;
        }
    }
}