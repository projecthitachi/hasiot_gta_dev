﻿using has.iot.Components;
using has.iot.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Camera.CCTV.Hikvision.Models;
using Camera.CCTV.Hikvision.Models.System;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;

namespace Camera.CCTV.Hikvision.Controllers
{
    public class CCTVHikvisionController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DataLog()
        {
            return View();
        }
        public ActionResult Read([DataSourceRequest]DataSourceRequest poRequest)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            CCTVModel oClass = new CCTVModel();
            var vResult = oClass.GetList(oRemoteDB).ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest]DataSourceRequest poRequest, CCTVHikvision_REC poRecord)
        {
            if ((poRecord != null))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    CCTVModel oClass = new CCTVModel();
                    if (!oClass.Insert(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest]DataSourceRequest poRequest, CCTVHikvision_REC poRecord)
        {
            if ((poRecord != null))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    CCTVModel oClass = new CCTVModel();
                    if (!oClass.Update(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest poRequest, CCTVHikvision_REC poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    CCTVModel oClass = new CCTVModel();
                    if (!oClass.Delete(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        public ActionResult SubmitDefault(CCTVHikvisionDefault_REC poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    CCTVModel oQuery = new CCTVModel();
                    CCTVHikvisionDefault_REC voTable = oQuery.GetDefaultSetting(oRemoteDB).SingleOrDefault();
                    long PRI = 0;
                    if (voTable == null)
                        PRI = oQuery.InsertDefault(oRemoteDB, poData);
                    else
                        oQuery.UpdateDefault(oRemoteDB, poData);

                    oTransactions.Commit();

                    ResultData.Add("RecordID", PRI.ToString());
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Insert Default Settings " + poData.CategoryID + " Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }

            return Json(ResultData);
        }
        public ActionResult SubmitAllDefault(CCTVHikvisionDefault_REC poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    CCTVModel oQuery = new CCTVModel();
                    CCTVHikvisionDefault_REC voTable = oQuery.GetDefaultSetting(oRemoteDB).SingleOrDefault();
                    if (voTable != null)
                    {
                        IList<CCTVHikvision_REC> voList = oQuery.GetList(oRemoteDB).ToList();
                        foreach (CCTVHikvision_REC voData in voList)
                        {
                            voData.RecordTimestamp = DateTime.Now;
                            voData.IPAddress = voTable.IPAddress;
                            voData.Username = voTable.Username;
                            voData.Password = voTable.Password;

                            oQuery.Update(oRemoteDB, voData);
                        }
                    }

                    oTransactions.Commit();

                    ResultData.Add("RecordID", voTable.RecordID.ToString());
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Submit to all devices setting Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }

            return Json(ResultData);
        }
    }
}