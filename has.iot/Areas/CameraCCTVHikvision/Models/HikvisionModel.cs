﻿using Camera.CCTV.Hikvision.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Camera.CCTV.Hikvision.Models
{
    public class CCTVHikvision_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string CategoryID { get; set; }
        public string IPAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string StatusDevice { get; set; }
        public string URLRtsp { get; set; }
        public string LocationID { get; set; }

        public string LocationName { get; set; }
    }
    public class CCTVHikvisionDefault_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string CategoryID { get; set; }
        public string URLRtsp { get; set; }
        public string URLService { get; set; }
        public string IPAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class CCTVModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public CCTVModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t70204fieldselect"
        private string t70204fieldselect = @"
            t70204r001 as RecordID,
            t70204r002 as RecordTimestamp,
            t70204r003 as RecordStatus,
            t70204f001 as DeviceID,
            t70204f002 as DeviceName,
            t70204f003 as CategoryID,
            t70204f004 as IPAddress,
            t70204f005 as Username,
            t70204f006 as Password,
            t70204f007 as StatusDevice,
            t70204f008 as URLRtsp,
            t70204f009 as LocationID";
        #endregion
        #region "t70104fieldselect"
        private string t70104fieldselect = @"
            t70104r001 as RecordID,
            t70104r002 as RecordTimestamp,
            t70104r003 as RecordStatus,
            t70104f001 as CategoryID,
            t70104f002 as URLRtsp,
            t70104f003 as URLService,
            t70104f004 as IPAddress,
            t70104f005 as Username,
            t70104f006 as Password";
        #endregion

        public DbRawSqlQuery<CCTVHikvision_REC> GetList(DatabaseContext oRemoteDB)
        {
            string sSQL = " SELECT "+t70204fieldselect+", t8030f002 as LocationName FROM dbo.t70204 LEFT JOIN t8030 on t8030f001 = t70204f009 ORDER BY t70204r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<CCTVHikvision_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<CCTVHikvision_REC> GetByGatewayID(DatabaseContext oRemoteDB, string sGatewayID)
        {
            string sSQL = " SELECT " + t70204fieldselect + ", t8030f002 as LocationName FROM dbo.t70204 LEFT JOIN t8030 on t8030f001 = t70204f009 where t70204f001 = '" + sGatewayID + "' ORDER BY t70204r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<CCTVHikvision_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<CCTVHikvisionDefault_REC> GetDefaultSetting(DatabaseContext oRemoteDB)
        {
            string sSQL = " SELECT TOP(1)" + t70104fieldselect + " FROM dbo.t70104 ORDER BY t70104r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<CCTVHikvisionDefault_REC>(sSQL);
            return vQuery;
        }

        public bool Insert(DatabaseContext oRemoteDB, CCTVHikvision_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                has.iot.Components.GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70204 " +
                    "      ( " +
                    "      t70204r002, t70204r003, " +
                    "      t70204f001, t70204f002, t70204f003, t70204f004, t70204f005, t70204f006, t70204f007, t70204f008, t70204f009" +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70204r002, @pt70204r003, " +
                    "      @pt70204f001, @pt70204f002, @pt70204f003, @pt70204f004, @pt70204f005, @pt70204f006, @pt70204f007, @pt70204f008, @pt70204f009" +
                    "      )" +
                    ";" +
                    "SELECT @pt70204r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70204r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70204r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70204r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70204f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70204f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt70204f003", "CameraCCTV"));
                oParameters.Add(new SqlParameter("@pt70204f004", poRecord.IPAddress));
                oParameters.Add(new SqlParameter("@pt70204f005", poRecord.Username));
                oParameters.Add(new SqlParameter("@pt70204f006", poRecord.Password));
                oParameters.Add(new SqlParameter("@pt70204f007", poRecord.StatusDevice));
                oParameters.Add(new SqlParameter("@pt70204f008", poRecord.URLRtsp));
                oParameters.Add(new SqlParameter("@pt70204f009", poRecord.LocationID));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, CCTVHikvision_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            has.iot.Components.GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70204 " +
                "   SET " +
                "      t70204f002 = @pt70204f002, " +
                "      t70204f004 = @pt70204f004, " +
                "      t70204f005 = @pt70204f005, " +
                "      t70204f006 = @pt70204f006, " +
                "      t70204f007 = @pt70204f007, " +
                "      t70204f008 = @pt70204f008, " +
                "      t70204f009 = @pt70204f009 " +
                "   WHERE (t70204r001 = @pt70204r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70204r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70204f002", poRecord.DeviceName));
            oParameters.Add(new SqlParameter("@pt70204f004", poRecord.IPAddress));
            oParameters.Add(new SqlParameter("@pt70204f005", poRecord.Username));
            oParameters.Add(new SqlParameter("@pt70204f006", poRecord.Password));
            oParameters.Add(new SqlParameter("@pt70204f007", poRecord.StatusDevice));
            oParameters.Add(new SqlParameter("@pt70204f008", poRecord.URLRtsp));
            oParameters.Add(new SqlParameter("@pt70204f009", poRecord.LocationID));
            //oParameters.Add(new SqlParameter("@pt70204f011", poRecord.StatusDevice));
            //oParameters.Add(new SqlParameter("@pt70204f012", poRecord.StatusSwitch));
            //oParameters.Add(new SqlParameter("@pt70204f013", poRecord.Live));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateStatus(DatabaseContext oRemoteDB, CCTVHikvision_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            //GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70204 " +
                "   SET " +
                "      t70204f007 = @t70204f007 " +
                "   WHERE (t70204r001 = @pt70204r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70204r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70204f007", poRecord.StatusDevice));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, CCTVHikvision_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t70204 " +
                "   WHERE (t70204r001 = @pt70204r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70204r001", poRecord.RecordID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }

        public long InsertDefault(DatabaseContext oRemoteDB, CCTVHikvisionDefault_REC poRecord)
        {
            long bReturn = 0;
            //------------------------------
            poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;

            //GlobalFunction.TrimNull(poRecord);
            //----------
            string sSQL = "" +
                "INSERT INTO t70104 " +
                "      ( " +
                "      t70104r002, t70104r003, " +
                "      t70104f001, t70104f002, t70104f003,t70104f004,t70104f005,t70104f006) " +
                "      VALUES " +
                "      (" +
                "      @pt70104r002, @pt70104r003, " +
                "      @pt70104f001, @pt70104f002, @pt70104f003,@pt70104f004,@pt70104f005,@pt70104f006)" +
                ";" +
                "SELECT @pt70104r001 = SCOPE_IDENTITY(); "
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter { ParameterName = "@pt70104r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
            oParameters.Add(new SqlParameter("@pt70104r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70104r003", poRecord.RecordStatus));

            oParameters.Add(new SqlParameter("@pt70104f001", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70104f002", poRecord.URLRtsp));
            oParameters.Add(new SqlParameter("@pt70104f003", poRecord.URLService));
            oParameters.Add(new SqlParameter("@pt70104f004", poRecord.IPAddress));
            oParameters.Add(new SqlParameter("@pt70104f005", poRecord.Username));
            oParameters.Add(new SqlParameter("@pt70104f006", poRecord.Password));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

            if (nReturn == 1)
            {
                bReturn = Convert.ToInt64(vSqlParameter[0].Value);
            }
            else
            {
                ErrorMessage = "Failed to insert record!";
                bReturn = 0;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateDefault(DatabaseContext oRemoteDB, CCTVHikvisionDefault_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            //poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            //GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70104 " +
                "   SET " +
                "      t70104f002 = @pt70104f002, " +
                "      t70104f003 = @pt70104f003, " +
                "      t70104f004 = @pt70104f004, " +
                "      t70104f005 = @pt70104f005, " +
                "      t70104f006 = @pt70104f006 " +
                "   WHERE (t70104r001 = @pt70104r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));

            oParameters.Add(new SqlParameter("@pt70104r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70104f001", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70104f002", poRecord.URLRtsp));
            oParameters.Add(new SqlParameter("@pt70104f003", poRecord.URLService));
            oParameters.Add(new SqlParameter("@pt70104f004", poRecord.IPAddress));
            oParameters.Add(new SqlParameter("@pt70104f005", poRecord.Username));
            oParameters.Add(new SqlParameter("@pt70104f006", poRecord.Password));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {
                bReturn = true;
            }
            else
            {
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
    }
}