﻿using has.iot.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
namespace MVCPluggableDemo
{
    public class CameraCCTVHikvisionAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CameraCCTVHikvision";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CameraCCTVHikvision_default",
                "CameraCCTVHikvision/{controller}/{action}/{id}",
                new {controller= "CCTVHikvision", action = "Index", id = UrlParameter.Optional },
                new string[] { "Camera.CCTV.Hikvision.Controllers" }
            );   
        }
    }
}