﻿using has.iot.Components;
using SmartPlug.Tasmota.Models.System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SmartPlug.Tasmota.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using uPLibrary.Networking.M2Mqtt.Messages;
using Kendo.Mvc.Export;
using Telerik.Documents.SpreadsheetStreaming;
using System.IO;
using System.Text;
using SmartPlug.Tasmota.Components;
using MongoDB.Driver;
using MongoDB.Bson;
using Kendo.Mvc;
using System.Text.RegularExpressions;

namespace SmartPlug.Tasmota.Controllers
{
    public class TasmotaController : Controller
    {
        // GET: SmartPlug
        public ActionResult Index()
        {
            has.iot.Models.Brands oModel = new has.iot.Models.Brands();
            IList<has.iot.Models.Brands_REC> oTable = oModel.GetList("SmartPlug").ToList();
            ViewData["DataList"] = oTable;
            return View();
        }

        public ActionResult Log_Read([DataSourceRequest]DataSourceRequest poRequest, string DeviceID)
        {
            TasmotaModel oClass = new TasmotaModel();
            var vResult = oClass.GetLogList(DeviceID).ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        #region CRUD SQL
        public ActionResult ReadSql([DataSourceRequest]DataSourceRequest poRequest)
        {
            TasmotaModel oClass = new TasmotaModel();
            var vResult = oClass.GetList().ToList();
            // begin write to json file
            string targetPathView = Server.MapPath("~/Components/Plugins/WidgetRealtime/");
            if (!Directory.Exists(targetPathView))
            {
                Directory.CreateDirectory(targetPathView);
            }
            else
            {
                string strValue = Convert.ToString(JsonConvert.SerializeObject(vResult));
                System.IO.File.WriteAllText(Server.MapPath("~/Components/Plugins/WidgetRealtime/smartplug.json"), strValue);
            }
            // end wriet to json file
            return Json(vResult.ToDataSourceResult(poRequest));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateSql([DataSourceRequest]DataSourceRequest poRequest, Tasmota_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    TasmotaModel oClass = new TasmotaModel();
                    if (!oClass.Insert(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSql([DataSourceRequest]DataSourceRequest poRequest, Tasmota_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    TasmotaModel oClass = new TasmotaModel();
                    if (!oClass.Update(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroySql([DataSourceRequest]DataSourceRequest poRequest, Tasmota_REC poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    TasmotaModel oClass = new TasmotaModel();
                    if (!oClass.Delete(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        #endregion

        #region MongoDB CRUD
        public ActionResult Read([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<SmartDevices_REC> Collections = database.GetCollection<SmartDevices_REC>("SmartDevices");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<SmartDevices_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                    { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<SmartDevices_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable.Where(x => x.Category == "Smartplug").ToList();
            ds.Total = Convert.ToInt32(Collections.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest]DataSourceRequest poRequest, SmartDevices_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();

                IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                IMongoCollection<SmartDevices_REC> Collections = database.GetCollection<SmartDevices_REC>("SmartDevices");
                try
                {
                    SmartDevices_REC voData = new SmartDevices_REC();
                    voData.RecordTimestamp = DateTime.Now;
                    voData.DeviceID = poRecord.DeviceID;
                    voData.DeviceName = poRecord.DeviceName;
                    voData.IPAddress = poRecord.IPAddress;
                    voData.ObjectName = poRecord.ObjectName;
                    voData.Location = poRecord.Location;
                    voData.SwitchNo = poRecord.SwitchNo;
                    voData.LightController = poRecord.LightController;
                    voData.Category = "Smartplug";
                    voData.Relay = 0;
                    Collections.InsertOne(voData);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        public ActionResult Update([DataSourceRequest]DataSourceRequest poRequest, SmartDevices_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();

                IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                IMongoCollection<SmartDevices_REC> Collections = database.GetCollection<SmartDevices_REC>("SmartDevices");
                try
                {
                    var builder = Builders<SmartDevices_REC>.Filter;
                    var filter = builder.Eq<string>("DeviceID", poRecord.DeviceID);
                    var update = Builders<SmartDevices_REC>.Update.Set("DeviceName", poRecord.DeviceName)
                    .Set("IPAddress", poRecord.IPAddress).Set("ObjectName", poRecord.ObjectName).Set("Location", poRecord.Location)
                    .Set("SwitchNo", poRecord.SwitchNo).Set("LightController", poRecord.LightController);
                    Collections.UpdateOne(filter, update);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest poRequest, SmartDevices_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                IMongoCollection<SmartDevices_REC> Collections = database.GetCollection<SmartDevices_REC>("SmartDevices");
                try
                {
                    var builder = Builders<SmartDevices_REC>.Filter;
                    var filter = builder.Eq<string>("DeviceID", poRecord.DeviceID);
                    Collections.DeleteOne(filter);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        #endregion

        public ActionResult Command(string cmd, string value)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();

            if (value != "All")
            {
                string strValue = Convert.ToString(cmd);
                Constants.MqttClient.Publish("Smartplug/Tasmota/"+value + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            else
            {
                TasmotaModel oClass = new TasmotaModel();
                var vResult = oClass.GetList().ToList();
                foreach (Tasmota_REC oData in vResult)
                {
                    string strValue = Convert.ToString(cmd);
                    Constants.MqttClient.Publish("Smartplug/Tasmota/" + oData.IPAddress + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult checkRelay(string DeviceID)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();

            string strValue = Convert.ToString("");
            Constants.MqttClient.Publish("Smartplug/Tasmota/" + DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        public static void Tasmota_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            string[] topic = e.Topic.Split('/');
            string Category = topic[0];
            if (topic.Length > 2)
            {
                string Brand = topic[1];
                string hostDevice = topic[2];
                string prefix = topic[3];
                var message = System.Text.Encoding.Default.GetString(e.Message);

                if (GlobalFunction.IsValidJson(message))
                {
                    var RT = GlobalHost.ConnectionManager.GetHubContext<RealTimeHub>();

                    DatabaseContext oRemoteDB = new DatabaseContext();

                    object result = JsonConvert.DeserializeObject(message);
                    JObject voobj = JObject.Parse(result.ToString());
                    if (prefix == "RESULT")
                    {
                        if (voobj.ContainsKey("POWER1"))
                        {
                            // begin switch relay
                            TasmotaModel oClass = new TasmotaModel();
                            Tasmota_REC oRecord = new Tasmota_REC();
                            string Power = voobj["POWER1"].ToString();
                            oRecord.DeviceID = hostDevice;
                            oRecord.StatusSwitch = Power;
                            oRecord.StatusDevice = "Connected";
                            oClass.UpdateStatus(oRemoteDB, oRecord);
                            var json = new JavaScriptSerializer().Serialize(oRecord);
                            RT.Clients.All.Tasmota("Check", json);
                            TasmotaLog_REC poRecords = new TasmotaLog_REC();
                            poRecords.DeviceID = hostDevice;
                            poRecords.ChartGauge = "chartgauge" + poRecords.DeviceID;
                            poRecords.ChartBar = "chartBar" + poRecords.DeviceID;
                            var jsons = new JavaScriptSerializer().Serialize(poRecords);
                            RT.Clients.All.ButtonSwitch(voobj["POWER1"].ToString(), jsons);
                            // end switch relay
                        }
                    }
                    else if (prefix == "SENSOR")
                    {
                        if (voobj.ContainsKey("ENERGY"))
                        {
                            // Begin insert data log
                            object resultEnergi = JsonConvert.DeserializeObject(voobj["ENERGY"].ToString());
                            JObject obj = JObject.Parse(resultEnergi.ToString());

                            TasmotaModel oQuery = new TasmotaModel();

                            TasmotaDataLog_REC poRecord = new TasmotaDataLog_REC();
                            poRecord.RecordTimestamp = DateTime.Now;
                            poRecord.DeviceID = hostDevice;
                            poRecord.Payload = message;
                            poRecord.Current = Convert.ToDecimal(obj["Power"].ToString());
                            //oQuery.DataLogInsert(oRemoteDB, poRecord);

                            RT.Clients.All.Tasmota("DataLog", message);
                            // end insert data log

                            //get the Json filepath  
                            //string pathroot = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                            //pathroot = pathroot + "\\App_Data";
                            //JObject objHp = null;
                            //JObject objlp = null;
                            //JObject objunk = null;
                            //if (System.IO.Directory.Exists(pathroot))
                            //{
                            //    string path = System.IO.Directory.GetCurrentDirectory();
                            //    string file = pathroot + "\\power_consumption_gadget.json";
                            //    //deserialize JSON from file  
                            //    string Json = System.IO.File.ReadAllText(file);
                            //    object rootresult = JsonConvert.DeserializeObject(Json);
                            //    JObject rootobj = JObject.Parse(rootresult.ToString());

                            //    object resulthp = JsonConvert.DeserializeObject(rootobj["handphone"].ToString());
                            //    objHp = JObject.Parse(resulthp.ToString());

                            //    object resultlp = JsonConvert.DeserializeObject(rootobj["laptop"].ToString());
                            //    objlp = JObject.Parse(resultlp.ToString());

                            //    object resultunk = JsonConvert.DeserializeObject(rootobj["unknown"].ToString());
                            //    objunk = JObject.Parse(resultunk.ToString());
                            //}

                            //string voltage = (obj["Voltage"] == null) ? "0" : obj["Voltage"].ToString().ToString();
                            //string current = (obj["Current"] == null) ? "0" : obj["Current"].ToString();
                            //string power = (obj["Power"] == null) ? "0" : obj["Power"].ToString();
                            //string apparent = (obj["ApparentPower"] == null) ? "0" : obj["ApparentPower"].ToString();
                            //string factor = (obj["Factors"] == null) ? "0" : obj["Factors"].ToString();
                            //string energy = (obj["energy"] == null) ? "0" : obj["energy"].ToString();
                            //string host = (obj["host"] == null) ? "" : obj["host"].ToString();
                            //string ip = (obj["ip"] == null) ? "" : obj["ip"].ToString();
                            //string mac = (obj["mac"] == null) ? "" : obj["mac"].ToString();

                            //TasmotaLog_REC poRecords = new TasmotaLog_REC();
                            //poRecords.DeviceID = hostDevice;
                            //poRecords.DeviceName = "";
                            //poRecords.Voltage = Convert.ToInt32(voltage);
                            //poRecords.PowerFactor = Convert.ToInt32(factor);
                            //poRecords.Energy = Convert.ToInt32(energy);
                            //poRecords.Current = Convert.ToDecimal(current);
                            //poRecords.ActivePower = Convert.ToDecimal(power);
                            //poRecords.ApparentPower = Convert.ToDecimal(apparent);
                            //TasmotaLogs(poRecords);

                            //if (Convert.ToDecimal(power) <= Convert.ToInt32(objunk["max_oncharging"].ToString()) && Convert.ToDecimal(power) >= Convert.ToInt32(objunk["min_oncharging"].ToString()))
                            //{
                            //    if (Convert.ToDecimal(power) <= Convert.ToInt32(objunk["max_fullcharging"].ToString()) && Convert.ToDecimal(power) >= Convert.ToInt32(objunk["min_fullcharging"].ToString()))
                            //    {
                            //        poRecords.fullcharger = "yes";
                            //    }
                            //    else
                            //    {
                            //        poRecords.fullcharger = "no";
                            //    }
                            //    poRecords.GadGet = "unknown";
                            //}
                            //else if (Convert.ToDecimal(power) <= Convert.ToInt32(objlp["max_oncharging"].ToString()) && Convert.ToDecimal(power) >= Convert.ToInt32(objlp["min_oncharging"].ToString()))
                            //{
                            //    if (Convert.ToDecimal(power) <= Convert.ToInt32(objlp["max_fullcharging"].ToString()) && Convert.ToDecimal(power) >= Convert.ToInt32(objlp["min_fullcharging"].ToString()))
                            //    {
                            //        poRecords.fullcharger = "yes";
                            //    }
                            //    else
                            //    {
                            //        poRecords.fullcharger = "no";
                            //    }
                            //    poRecords.GadGet = "laptop";
                            //}
                            //else if (Convert.ToDecimal(power) <= Convert.ToInt32(objHp["max_oncharging"].ToString()) && Convert.ToDecimal(power) >= Convert.ToInt32(objHp["min_oncharging"].ToString()))
                            //{
                            //    if (Convert.ToDecimal(power) <= Convert.ToInt32(objHp["max_fullcharging"].ToString()) && Convert.ToDecimal(power) >= Convert.ToInt32(objHp["min_fullcharging"].ToString()))
                            //    {
                            //        poRecords.fullcharger = "yes";
                            //    }
                            //    else
                            //    {
                            //        poRecords.fullcharger = "no";
                            //    }
                            //    poRecords.GadGet = "handphone";
                            //}
                            //else if (Convert.ToDecimal(power) == 0)
                            //{
                            //    poRecords.fullcharger = "poweroff";
                            //    poRecords.GadGet = "poweroff";
                            //}

                            //poRecords.ChartGauge = "chartgauge" + poRecord.DeviceID;
                            //poRecords.ChartBar = "chartBar" + poRecord.DeviceID;
                            //var json = new JavaScriptSerializer().Serialize(poRecord);
                            //RT.Clients.All.Device("chart", json);
                        }
                    }
                }
            }
        }

        private static void TasmotaLogs(TasmotaLog_REC poRecord)
        {
            Dictionary<string, string> vsLogMessage = new Dictionary<string, string>();
            Models.System.DatabaseContext oRemoteDB = new Models.System.DatabaseContext();
            var oTransaction = oRemoteDB.Database.BeginTransaction();
            try
            {
                TasmotaModel oClass = new TasmotaModel();
                if (!oClass.InsertLog(oRemoteDB, poRecord))
                {

                }
                vsLogMessage.Add("errorcode", "0");
                vsLogMessage.Add("title", "Success");
                vsLogMessage.Add("msg", "success");
                oTransaction.Commit();
                oRemoteDB.Database.Connection.Close();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                vsLogMessage.Add("errorcode", "100");
                vsLogMessage.Add("title", "warning");
                vsLogMessage.Add("msg", ex.Message);
            }
        }

        public ActionResult getLocationDLL(string DeviceID, string Type, string text)
        {
            has.iot.Models.LocationModel oClass = new has.iot.Models.LocationModel();
            has.iot.Models.System.DatabaseContext oRemoteDB = new has.iot.Models.System.DatabaseContext();
            var vResult = oClass.GetList(oRemoteDB).ToList();
            return Json(vResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DataLog()
        {
            return View();
        }
        public ActionResult GetListDataLog([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            /*TasmotaModel oModel = new TasmotaModel();
            IList<TasmotaDataLog_REC> oTable = oModel.GetListDataLog(poRequest).ToList();
            var ds = oTable.ToDataSourceResult(poRequest);

            if (poRequest.Filters.Count > 0)
                ds.Total = oTable.Count;
            else
            {
                ds.Total = oModel.GetCountData("t70301").SingleOrDefault().count;
                ds.Data = oTable;
            }

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;*/
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<TasmotaDataLog_REC> Collections = database.GetCollection<TasmotaDataLog_REC>("SmartplugDataLogs");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<TasmotaDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                    { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<TasmotaDataLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable;
            ds.Total = Convert.ToInt32(Collections.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public IEnumerable<TasmotaDataLog_REC> GetListDataLogReport([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<TasmotaDataLog_REC> Collections = database.GetCollection<TasmotaDataLog_REC>("SmartplugDataLogs");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<TasmotaDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();
            if (filter_date != null)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) },
                    { "$lt", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            IEnumerable<TasmotaDataLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).ToList();
            return oTable;
        }
        [HttpPost]
        public ActionResult ExportServer([DataSourceRequest]DataSourceRequest poRequest, ParameterExport data)
        {
            var columnsData = data.columns.ToArray();
            SpreadDocumentFormat exportFormat = data.options.format.ToString() == "csv" ? exportFormat = SpreadDocumentFormat.Csv : exportFormat = SpreadDocumentFormat.Xlsx;
            Action<ExportCellStyle> cellStyle = new Action<ExportCellStyle>(ChangeCellStyle);
            Action<ExportRowStyle> rowStyle = new Action<ExportRowStyle>(ChangeRowStyle);
            Action<ExportColumnStyle> columnStyle = new Action<ExportColumnStyle>(ChangeColumnStyle);

            string fileName = string.Format("{0}.{1}", data.options.title, data.options.format);
            string mimeType = Helpers.GetMimeType(exportFormat);

            Stream exportStream = exportFormat == SpreadDocumentFormat.Xlsx ?
                GetListDataLogReport(poRequest, data.transport).ToXlsxStream(columnsData) :
                GetListDataLogReport(poRequest, data.transport).ToCsvStream(columnsData);

            var fileStreamResult = new FileStreamResult(exportStream, mimeType);
            fileStreamResult.FileDownloadName = fileName;
            fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);
            using (var fileStream = System.IO.File.Create(Server.MapPath("~" + Constants.ExportDirectory + fileName)))
            {
                fileStreamResult.FileStream.CopyTo(fileStream);
            }

            return Json(new { filename = fileName });
        }

        private void ChangeCellStyle(ExportCellStyle e)
        {
            bool isHeader = e.Row == 0;
            SpreadBorder border = new SpreadBorder(SpreadBorderStyle.Thick, new SpreadThemableColor(new SpreadColor(0, 0, 0)));
            SpreadCellFormat format = new SpreadCellFormat
            {
                //TopBorder = border,
                //BottomBorder = border,
                //LeftBorder = border,
                //RightBorder = border,
                //Fill = SpreadPatternFill.CreateSolidFill(new SpreadColor(255, 255, 255)),
                FontSize = 11,
                ForeColor = new SpreadThemableColor(new SpreadColor(0, 0, 0)),
                WrapText = false
            };
            e.Cell.SetFormat(format);
        }
        private void ChangeRowStyle(ExportRowStyle e)
        {
            e.Row.SetHeightInPixels(e.Index == 0 ? 80 : 30);
        }
        private void ChangeColumnStyle(ExportColumnStyle e)
        {
            double width = e.Name == "Item ID" || e.Name == "Barcode" ? 200 : 100;
            e.Column.SetWidthInPixels(width);
        }
    }
}