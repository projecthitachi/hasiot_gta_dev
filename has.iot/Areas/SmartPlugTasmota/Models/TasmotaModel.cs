﻿using has.iot.Components;
using Kendo.Mvc;
using Kendo.Mvc.Export;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using SmartPlug.Tasmota.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SmartPlug.Tasmota.Models
{
    public class Tasmota_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string CategoryID { get; set; }
        public string IPAddress { get; set; }
        public string MacAddress { get; set; }
        public string SSID { get; set; }
        public string Password { get; set; }
        public string MQTTHost { get; set; }
        public string MQTTUsername { get; set; }
        public string MQTTPassword { get; set; }
        public string StatusDevice { get; set; }
        public string StatusSwitch { get; set; }
        public string LocationID { get; set; }
        public string LocationName { get; set; }
        public int Live { get; set; }
    }
    public class TasmotaDataLog_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        private long t70301r001;
        [Display(Name = "RecordID")]
        public virtual long RecordID { get { return t70301r001; } set { t70301r001 = value; } }

        private DateTime t70301r002;
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public virtual DateTime RecordTimestamp { get { return t70301r002; } set { t70301r002 = value; } }

        private Int32 t70301r003;
        [Display(Name = "Record Status")]
        public virtual Int32 RecordStatus { get { return t70301r003; } set { t70301r003 = value; } }

        private string t70301f001;
        [Display(Name = "BaseStation ID")]
        public virtual string BaseStationID { get { return t70301f001; } set { t70301f001 = value; } }

        private string t70301f002;
        [Display(Name = "Device ID")]
        public virtual string DeviceID { get { return t70301f002; } set { t70301f002 = value; } }

        private string t70301f003;
        [Display(Name = "Payload")]
        public virtual string Payload { get { return t70301f003; } set { t70301f003 = value; } }

        private decimal t70301f004;
        [Display(Name = "Current")]
        public virtual decimal Current { get { return t70301f004; } set { t70301f004 = value; } }

        public virtual string RecordTimestampString { get; set; }        
        public virtual string DeviceName { get; set; }        
        public virtual string IPAddress { get; set; }        
        public virtual int Voltage { get; set; }
        public virtual int PowerFactor { get; set; }
        public virtual string Energy { get; set; }
        public virtual string ActivePower { get; set; }
        public virtual string ApparentPower { get; set; }
    }
    public class TasmotaLog_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public int Energy { get; set; }
        public decimal Current { get; set; }
        public decimal ActivePower { get; set; }
        public decimal ApparentPower { get; set; }
        public string ChartGauge { get; set; }
        public string ChartBar { get; set; }
        public string GadGet { get; set; }
        public string fullcharger { get; set; }

    }
    public class SmartDevices_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string ObjectName { get; set; }
        public string Location { get; set; }
        public int SwitchNo { get; set; }
        public int LightController { get; set; }
        public string Category { get; set; }
        public int Relay { get; set; }
    }
    public class CountData
    {
        public int count { get; set; }
    }
    public class FilterDate
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
    public class ParameterExport
    {
        public IList<ExportColumnSettings> columns { get; set; }
        public ParameterExportOptions options { get; set; }
        public FilterDate transport { get; set; }
    }
    public class ParameterExportOptions
    {
        public string format { get; set; }
        public string title { get; set; }
    }
    public class TasmotaModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public TasmotaModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }

        #region "t70301fieldselect"
        private string t70301fieldselect = @"
            t70301r001 as RecordID,
            t70301r002 as RecordTimestamp,
            t70301r003 as RecordStatus,
            t70301f001 as BaseStationID,
            t70301f002 as DeviceID,
            t70301f003 as Payload,
            t70301f004 as 'Current' ";
        #endregion
        public DbRawSqlQuery<CountData> GetCountData(string table)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT COUNT(*) as count from " + table;
            var vQuery = oRemoteDB.Database.SqlQuery<CountData>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<TasmotaDataLog_REC> GetListDataLog([DataSourceRequest]DataSourceRequest poRequest)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            int start = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int end = poRequest.Page * poRequest.PageSize;

            string WhereClause = "";
            foreach (FilterDescriptor filter in poRequest.Filters)
            {
                WhereClause += filter.Member + " LIKE '%" + filter.Value + "%' AND ";
            }

            string Offset = "";
            string Limit = "";
            if (WhereClause.Length == 0)
            {
                Offset = "([table].RowNum BETWEEN " + start + " AND " + end + ")";
                Limit = "TOP(" + poRequest.PageSize + ")";
            }
            else
                WhereClause = WhereClause.Remove(WhereClause.LastIndexOf("AND"), 3);

            string sSQL = "SELECT " + Limit + "* " +
                "FROM(SELECT " + t70301fieldselect + ", ROW_NUMBER() OVER(ORDER BY t70301r001 DESC) AS RowNum " +
                "FROM t70301) AS [table] " +
                "WHERE " + WhereClause + Offset;

            var vQuery = oRemoteDB.Database.SqlQuery<TasmotaDataLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<TasmotaDataLog_REC> GetListDataLogReport()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            //string sSQL = "SELECT " + t70301fieldselect + " FROM t70301 ORDER BY t70301r001 DESC";
            string sSQL = "select t70301r001 as RecordID, FORMAT(t70301r002, 'MM/dd/yyyy HH:mm:ss') as RecordTimestampString, t70301r003 as RecordStatus, t70301f001 as BaseStationID, t70301f002 as DeviceID, t70301f003 as Payload," +
                "CAST([Current] as decimal(8, 2)) as [Current] FROM(select *, SUBSTRING(SUBSTRING(t70301f003, (CHARINDEX('Power', t70301f003) + 7), 5), 0, CHARINDEX(',', SUBSTRING(t70301f003, (CHARINDEX('Power', t70301f003) + 7), 5))) as [Current] from t70301_201907_15_26) as tab";
            var vQuery = oRemoteDB.Database.SqlQuery<TasmotaDataLog_REC>(sSQL);
            return vQuery;
        }
        public bool DataLogInsert(DatabaseContext oRemoteDB, TasmotaDataLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70301 " +
                    "      ( " +
                    "      t70301r002, t70301r003, " +
                    "      t70301f002, t70301f003, t70301f004 " +
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70301r002, @pt70301r003, " +
                    "      @pt70301f002, @pt70301f003, @pt70301f004 " +
                    "      )" +
                    ";" +
                    "SELECT @pt70301r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70301r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70301r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70301r003", poRecord.RecordStatus));

                //oParameters.Add(new SqlParameter("@pt70301f001", poRecord.BaseStationID));
                oParameters.Add(new SqlParameter("@pt70301f002", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70301f003", poRecord.Payload));
                oParameters.Add(new SqlParameter("@pt70301f004", poRecord.Current));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public DbRawSqlQuery<Tasmota_REC> GetSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT tb.t70200f005 AS DeviceID FROM t70100 ta JOIN t70200 tb ON ta.t70100f001 = tb.t70200f001 WHERE ta.t70100f001 = 'SmartPlug';";

            var vQuery = oRemoteDB.Database.SqlQuery<Tasmota_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Tasmota_REC> GetLastSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT TOP 1 t70201f001 AS DeviceID FROM t70201 ORDER BY t70201r001 DESC;";

            var vQuery = oRemoteDB.Database.SqlQuery<Tasmota_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Tasmota_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT a1.t70201r001 AS RecordID, a1.t70201r002 AS RecordTimestamp, a1.t70201r003 AS RecordStatus, a1.t70201f001 AS DeviceID, a1.t70201f002 AS DeviceName, a1.t70201f003 AS CategoryID, a1.t70201f004 AS IPAddress, a1.t70201f005 AS MacAddress, a1.t70201f006 AS SSID, a1.t70201f007 AS Password, a1.t70201f008 AS MQTTHost, a1.t70201f009 AS MQTTUsername, a1.t70201f010 AS MQTTPassword, a1.t70201f011 AS StatusDevice, a1.t70201f012 AS StatusSwitch, a1.t70201f013 AS Live, a1.t70201f014 AS LocationID, a2.t8030f002 AS LocationName " +
                " FROM t70201 a1 LEFT JOIN t8030 a2 ON a2.t8030f001 = a1.t70201f014 ORDER BY a1.t70201r001 DESC;";

            var vQuery = oRemoteDB.Database.SqlQuery<Tasmota_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<TasmotaLog_REC> GetLogList(string DeviceID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT t9002r001 AS RecordID, t9002r002 AS RecordTimestamp, t9002r003 AS RecordStatus, t9002f001 AS DeviceID, t9002f002 AS DeviceName, t9002f003 AS Voltage, t9002f004 AS PowerFactor, t9002f005 AS Energy, t9002f006 AS 'Current', t9002f007 AS ActivePower, t9002f008 AS ApparentPower FROM dbo.t9002 " +
                " WHERE t9002f001 = '"+ DeviceID + "' ORDER BY t9002r001 DESC;";

            var vQuery = oRemoteDB.Database.SqlQuery<TasmotaLog_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Tasmota_REC> GetOne(long RecordID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT a1.t70201r001 AS RecordID, a1.t70201r002 AS RecordTimestamp, a1.t70201r003 AS RecordStatus, a1.t70201f001 AS DeviceID, a1.t70201f002 AS DeviceName, a1.t70201f003 AS CategoryID, a1.t70201f004 AS IPAddress, a1.t70201f005 AS MacAddress, a1.t70201f006 AS SSID, a1.t70201f007 AS Password, a1.t70201f008 AS MQTTHost, a1.t70201f009 AS MQTTUsername, a1.t70201f010 AS MQTTPassword, a1.t70201f011 AS StatusDevice, a1.t70201f012 AS StatusSwitch, a1.t70201f013 AS Live, a1.t70201f014 AS LocationID, a2.t8030f002 AS LocationName " +
                " FROM t70201 a1  WHERE a1.t70201r001 = " + RecordID +  ";";

            var vQuery = oRemoteDB.Database.SqlQuery<Tasmota_REC>(sSQL);

            return vQuery;
        }
        public bool InsertLog(DatabaseContext oRemoteDB, TasmotaLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                //GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t9002 " +
                    "      ( " +
                    "      t9002r002, t9002r003, " +
                    "      t9002f001, t9002f002, t9002f003, t9002f004, t9002f005, t9002f006, t9002f007, t9002f008 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt9002r002, @pt9002r003, " +
                    "      @pt9002f001, @pt9002f002, @pt9002f003, @pt9002f004, @pt9002f005, @pt9002f006, @pt9002f007, @pt9002f008 " +
                    "      )" +
                    ";" +
                    "SELECT @pt9002r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt9002r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt9002r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt9002r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt9002f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt9002f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt9002f003", poRecord.Voltage));
                oParameters.Add(new SqlParameter("@pt9002f004", poRecord.PowerFactor));
                oParameters.Add(new SqlParameter("@pt9002f005", poRecord.Energy));
                oParameters.Add(new SqlParameter("@pt9002f006", poRecord.Current));
                oParameters.Add(new SqlParameter("@pt9002f007", poRecord.ActivePower));
                oParameters.Add(new SqlParameter("@pt9002f008", poRecord.ApparentPower));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Insert(DatabaseContext oRemoteDB, Tasmota_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;
                if (GetLastSerialNo().ToList().Count == 0)
                {
                    poRecord.DeviceID = GetSerialNo().SingleOrDefault().DeviceID + ".0000.0001";
                }
                else
                {
                    string[] serialNo = GetLastSerialNo().SingleOrDefault().DeviceID.Split('.');
                    int lastNo = Convert.ToInt32(serialNo[3]) + 1;
                    poRecord.DeviceID = serialNo[0] + "." + serialNo[1] + "." + serialNo[2] + "." + lastNo.ToString("D4");

                }
                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70201 " +
                    "      ( " +
                    "      t70201r002, t70201r003, " +
                    "      t70201f001, t70201f002, t70201f003, t70201f004, t70201f005, t70201f006, t70201f007, t70201f008, t70201f009, t70201f010, t70201f011, t70201f012, t70201f013, t70201f014 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70201r002, @pt70201r003, " +
                    "      @pt70201f001, @pt70201f002, @pt70201f003, @pt70201f004, @pt70201f005, @pt70201f006, @pt70201f007, @pt70201f008, @pt70201f009, @pt70201f010, @pt70201f011, @pt70201f012, @pt70201f013, @pt70201f014 " +
                    "      )" +
                    ";" +
                    "SELECT @pt70201r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70201r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70201r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70201r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70201f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70201f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt70201f003", poRecord.CategoryID));
                oParameters.Add(new SqlParameter("@pt70201f004", poRecord.IPAddress));
                oParameters.Add(new SqlParameter("@pt70201f005", poRecord.MacAddress));
                oParameters.Add(new SqlParameter("@pt70201f006", poRecord.SSID));
                oParameters.Add(new SqlParameter("@pt70201f007", poRecord.Password));
                oParameters.Add(new SqlParameter("@pt70201f008", poRecord.MQTTHost));
                oParameters.Add(new SqlParameter("@pt70201f009", poRecord.MQTTUsername));
                oParameters.Add(new SqlParameter("@pt70201f010", poRecord.MQTTPassword));
                oParameters.Add(new SqlParameter("@pt70201f011", poRecord.StatusDevice));
                oParameters.Add(new SqlParameter("@pt70201f012", poRecord.StatusSwitch));
                oParameters.Add(new SqlParameter("@pt70201f013", poRecord.Live));
                oParameters.Add(new SqlParameter("@pt70201f014", poRecord.LocationID));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, Tasmota_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70201 " +
                "   SET " +
                "      t70201f002 = @pt70201f002, " +
                "      t70201f003 = @pt70201f003, " +
                "      t70201f004 = @pt70201f004, " +
                "      t70201f005 = @pt70201f005, " +
                "      t70201f006 = @pt70201f006, " +
                "      t70201f007 = @pt70201f007, " +
                "      t70201f008 = @pt70201f008, " +
                "      t70201f009 = @pt70201f009, " +
                "      t70201f010 = @pt70201f010, " +
                "      t70201f013 = @pt70201f013, " +
                "      t70201f014 = @pt70201f014 " +
                "   WHERE (t70201f001 = @pt70201f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70201f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70201f002", poRecord.DeviceName));
            oParameters.Add(new SqlParameter("@pt70201f003", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70201f004", poRecord.IPAddress));
            oParameters.Add(new SqlParameter("@pt70201f005", poRecord.MacAddress));
            oParameters.Add(new SqlParameter("@pt70201f006", poRecord.SSID));
            oParameters.Add(new SqlParameter("@pt70201f007", poRecord.Password));
            oParameters.Add(new SqlParameter("@pt70201f008", poRecord.MQTTHost));
            oParameters.Add(new SqlParameter("@pt70201f009", poRecord.MQTTUsername));
            oParameters.Add(new SqlParameter("@pt70201f010", poRecord.MQTTPassword));
            //oParameters.Add(new SqlParameter("@pt70201f011", poRecord.StatusDevice));
            //oParameters.Add(new SqlParameter("@pt70201f012", poRecord.StatusSwitch));
            oParameters.Add(new SqlParameter("@pt70201f013", poRecord.Live));
            oParameters.Add(new SqlParameter("@pt70201f014", poRecord.LocationID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateStatus(DatabaseContext oRemoteDB, Tasmota_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70201 " +
                "   SET " +
                "      t70201f011 = @pt70201f011, " +
                "      t70201f012 = @pt70201f012 " +
                "   WHERE (t70201f004 = @pt70201f004) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70201f004", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70201f011", poRecord.StatusDevice));
            oParameters.Add(new SqlParameter("@pt70201f012", poRecord.StatusSwitch));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, Tasmota_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t70201 " +
                "   WHERE (t70201f001 = @pt70201f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70201f001", poRecord.DeviceID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
    }
}