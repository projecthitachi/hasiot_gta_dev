﻿using has.iot.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using SmartPlug.Tasmota.Components;

namespace MVCPluggableDemo
{
    public class SmartPlugTasmotaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SmartPlugTasmota";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SmartPlugTasmota_default",
                "SmartPlugTasmota/{controller}/{action}/{id}",
                new {controller= "Tasmota", action = "Index", id = UrlParameter.Optional },
                new string[] { "SmartPlug.Tasmota.Controllers" }
            );
            Constants.MqttClient = new MqttClient(GlobalFunction.mqttServer);
            Constants.MqttClient.Subscribe(new string[] { "Smartplug/Tasmota/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            Constants.MqttClient.MqttMsgPublishReceived += SmartPlug.Tasmota.Controllers.TasmotaController.Tasmota_MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            Constants.MqttClient.Connect(clientId);
        }
    }
}