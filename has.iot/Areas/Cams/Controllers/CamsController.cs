﻿using has.iot.Components;
using has.iot.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Cams.Models;
using Cams.Models.System;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt.Messages;
using Cams.Components;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Collections.Generic;
using Kendo.Mvc;
using Telerik.Documents.SpreadsheetStreaming;
using Kendo.Mvc.Export;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;

namespace Cams.Controllers
{
    public class CamsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DataLog()
        {
            return View();
        }

        public static void _MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            string[] topic = e.Topic.Split('/');
            string Category = topic[0];
            var message = System.Text.Encoding.Default.GetString(e.Message);

            if (GlobalFunction.IsValidJson(message))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();

                IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                IMongoCollection<Cams_REC> Collections = database.GetCollection<Cams_REC>("CamsDatalog");

                object result = JsonConvert.DeserializeObject(message);
                JObject voobj = JObject.Parse(result.ToString());

                if (topic[2] == "Book")
                {
                    Cams_REC voData = new Cams_REC();
                    voData.RecordTimestamp = DateTime.Now;
                    voData.ID = voobj["ID"].ToString();
                    voData.Room = voobj["Room"].ToString();
                    voData.Date = Convert.ToDateTime(voobj["Date"].ToString());
                    voData.Start = voobj["Start"].ToString();
                    voData.End = voobj["End"].ToString();
                    voData.Attendees = Convert.ToInt32(voobj["Attendees"].ToString());
                    voData.Guests = Convert.ToInt32(voobj["Guests"].ToString());
                    voData.BookBy = voobj["Book-by"].ToString();
                    voData.Type = "Book";
                    Collections.InsertOne(voData);
                }
                else
                {
                    Cams_REC voData = new Cams_REC();
                    voData.RecordTimestamp = DateTime.Now;
                    voData.ID = voobj["ID"].ToString();
                    voData.Start = voobj["Start"].ToString();
                    voData.End = voobj["End"].ToString();
                    voData.Type = "Cancel";
                    Collections.InsertOne(voData);
                }
            }
        }

        public ActionResult SendCommand(string ID, string Start, string End)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            ResultData.Add("ID", ID);
            ResultData.Add("Start", Start);
            ResultData.Add("End", End);

            string strValue = Convert.ToString(JsonConvert.SerializeObject(ResultData));
            Constants.MqttClient.Publish("/GW2CAMS/Cancel", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetListDataLog([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<Cams_REC> Collections = database.GetCollection<Cams_REC>("CamsDatalog");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<Cams_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                    { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<Cams_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable;
            ds.Total = Convert.ToInt32(Collections.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public IEnumerable<Cams_REC> GetListDataLogReport([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<Cams_REC> Collections = database.GetCollection<Cams_REC>("SmartswitchDataLogs");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<Cams_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();
            if (filter_date != null)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) },
                    { "$lt", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            IEnumerable<Cams_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).ToList();
            return oTable;
        }
        [HttpPost]
        public ActionResult ExportServer([DataSourceRequest]DataSourceRequest poRequest, ParameterExport data)
        {
            var columnsData = data.columns.ToArray();
            SpreadDocumentFormat exportFormat = data.options.format.ToString() == "csv" ? exportFormat = SpreadDocumentFormat.Csv : exportFormat = SpreadDocumentFormat.Xlsx;
            Action<ExportCellStyle> cellStyle = new Action<ExportCellStyle>(ChangeCellStyle);
            Action<ExportRowStyle> rowStyle = new Action<ExportRowStyle>(ChangeRowStyle);
            Action<ExportColumnStyle> columnStyle = new Action<ExportColumnStyle>(ChangeColumnStyle);

            string fileName = string.Format("{0}.{1}", data.options.title, data.options.format);
            string mimeType = Helpers.GetMimeType(exportFormat);

            Stream exportStream = exportFormat == SpreadDocumentFormat.Xlsx ?
                GetListDataLogReport(poRequest, data.transport).ToXlsxStream(columnsData) :
                GetListDataLogReport(poRequest, data.transport).ToCsvStream(columnsData);

            var fileStreamResult = new FileStreamResult(exportStream, mimeType);
            fileStreamResult.FileDownloadName = fileName;
            fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);
            using (var fileStream = System.IO.File.Create(Server.MapPath("~" + Constants.ExportDirectory + fileName)))
            {
                fileStreamResult.FileStream.CopyTo(fileStream);
            }

            return Json(new { filename = fileName });
        }
        private void ChangeCellStyle(ExportCellStyle e)
        {
            bool isHeader = e.Row == 0;
            SpreadBorder border = new SpreadBorder(SpreadBorderStyle.Thick, new SpreadThemableColor(new SpreadColor(0, 0, 0)));
            SpreadCellFormat format = new SpreadCellFormat
            {
                //TopBorder = border,
                //BottomBorder = border,
                //LeftBorder = border,
                //RightBorder = border,
                //Fill = SpreadPatternFill.CreateSolidFill(new SpreadColor(255, 255, 255)),
                FontSize = 11,
                ForeColor = new SpreadThemableColor(new SpreadColor(0, 0, 0)),
                WrapText = false
            };
            e.Cell.SetFormat(format);
        }
        private void ChangeRowStyle(ExportRowStyle e)
        {
            e.Row.SetHeightInPixels(e.Index == 0 ? 80 : 30);
        }
        private void ChangeColumnStyle(ExportColumnStyle e)
        {
            double width = e.Name == "Item ID" || e.Name == "Barcode" ? 200 : 100;
            e.Column.SetWidthInPixels(width);
        }

    }
}