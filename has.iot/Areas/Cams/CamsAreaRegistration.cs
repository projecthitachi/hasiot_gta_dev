﻿using has.iot.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using Cams.Components;

namespace MVCPluggableDemo
{
    public class CamsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Cams";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Cams_default",
                "Cams/{controller}/{action}/{id}",
                new {controller= "Cams", action = "Index", id = UrlParameter.Optional },
                new string[] { "Cams.Controllers" }
            );
            Constants.MqttClient = new MqttClient(GlobalFunction.mqttServer);
            Constants.MqttClient.Subscribe(new string[] { "/CAMS2GW/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            Constants.MqttClient.MqttMsgPublishReceived += Cams.Controllers.CamsController._MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            Constants.MqttClient.Connect(clientId);
        }
    }
}