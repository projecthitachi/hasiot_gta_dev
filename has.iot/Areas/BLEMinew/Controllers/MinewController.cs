﻿using has.iot.Components;
using has.iot.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BLE.Minew.Models;
using BLE.Minew.Models.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt.Messages;
using Kendo.Mvc.Export;
using Telerik.Documents.SpreadsheetStreaming;
using System.IO;
using Kendo.Mvc;
using MongoDB.Driver;
using BLE.Minew.Components;
using MongoDB.Bson;
using System.Text.RegularExpressions;

namespace BLE.Minew.Controllers
{
    public class MinewController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Read([DataSourceRequest]DataSourceRequest poRequest)
        {
            //BLEModel oClass = new BLEModel();
            //var vResult = oClass.GetList().ToList();
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<BLEMinew_REC> Collection = database.GetCollection<BLEMinew_REC>("BLEDevices");
            IList<BLEMinew_REC> vResult = Collection.Find(new BsonDocument()).ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest]DataSourceRequest poRequest, BLEMinew_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                //DatabaseContext oRemoteDB = new DatabaseContext();
                //var oTransaction = oRemoteDB.Database.BeginTransaction();
                //try
                //{
                //    BLEModel oClass = new BLEModel();
                //    if (!oClass.Insert(oRemoteDB, poRecord))
                //    {
                //        ModelState.AddModelError("Error", oClass.ErrorMessage);
                //    }
                //    oTransaction.Commit();
                //}
                //catch (Exception ex)
                //{
                //    oTransaction.Rollback();
                //    ModelState.AddModelError("Error", ex.Message);
                //}
                try
                {
                    IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                    IMongoCollection<BLEMinew_REC> Collection = database.GetCollection<BLEMinew_REC>("BLEDevices");

                    Collection.InsertOne(poRecord);
                }catch(Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest]DataSourceRequest poRequest, BLEMinew_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                //DatabaseContext oRemoteDB = new DatabaseContext();
                //var oTransaction = oRemoteDB.Database.BeginTransaction();
                //try
                //{
                //    BLEModel oClass = new BLEModel();
                //    if (!oClass.Update(oRemoteDB, poRecord))
                //    {
                //        ModelState.AddModelError("Error", oClass.ErrorMessage);
                //    }
                //    oTransaction.Commit();
                //}
                //catch (Exception ex)
                //{
                //    oTransaction.Rollback();
                //    ModelState.AddModelError("Error", ex.Message);
                //}
                try
                {
                    IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                    IMongoCollection<BLEMinew_REC> Collection = database.GetCollection<BLEMinew_REC>("BLEDevices");

                    FilterDefinitionBuilder<BLEMinew_REC> whereclause_builder = Builders<BLEMinew_REC>.Filter;
                    FilterDefinition<BLEMinew_REC> where_clause = whereclause_builder.Eq<string>("DeviceID", poRecord.DeviceID);
                    var update = Builders<BLEMinew_REC>.Update
                        .Set(p => p.RecordTimestamp, DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")))
                        .Set(p => p.RecordStatus, poRecord.RecordStatus)
                        .Set(p => p.DeviceID, poRecord.DeviceID)
                        .Set(p => p.DeviceName, poRecord.DeviceName)
                        .Set(p => p.CategoryID, poRecord.CategoryID)
                        .Set(p => p.IPAddress, poRecord.IPAddress)
                        .Set(p => p.MacAddress, poRecord.MacAddress)
                        .Set(p => p.SSID, poRecord.SSID)
                        .Set(p => p.Password, poRecord.Password)
                        .Set(p => p.MQTTHost, poRecord.MQTTHost)
                        .Set(p => p.MQTTUsername, poRecord.MQTTUsername)
                        .Set(p => p.MQTTPassword, poRecord.MQTTPassword)
                        .Set(p => p.StatusDevice, poRecord.StatusDevice)
                        .Set(p => p.Live, poRecord.Live)
                        .Set(p => p.IsPresentRSSI, poRecord.IsPresentRSSI)
                        .Set(p => p.LocationID, poRecord.LocationID);

                    Collection.UpdateOne(where_clause, update);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest poRequest, BLEMinew_REC poRecord)
        {
            if (poRecord != null)
            {
                //DatabaseContext oRemoteDB = new DatabaseContext();
                //var oTransaction = oRemoteDB.Database.BeginTransaction();
                //try
                //{
                //    BLEModel oClass = new BLEModel();
                //    if (!oClass.Delete(oRemoteDB, poRecord))
                //    {
                //        ModelState.AddModelError("Error", oClass.ErrorMessage);
                //    }
                //    oTransaction.Commit();
                //}
                //catch (Exception ex)
                //{
                //    oTransaction.Rollback();
                //    ModelState.AddModelError("Error", ex.Message);
                //}
                try
                {
                    IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
                    IMongoCollection<BLEMinew_REC> Collection = database.GetCollection<BLEMinew_REC>("BLEDevices");

                    FilterDefinitionBuilder<BLEMinew_REC> whereclause_builder = Builders<BLEMinew_REC>.Filter;
                    FilterDefinition<BLEMinew_REC> where_clause = whereclause_builder.Eq<string>("DeviceID", poRecord.DeviceID);

                    Collection.DeleteOne(where_clause);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }
        public ActionResult DataLog()
        {
            return View();
        }
        public ActionResult GetListDataLog([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            /*DatabaseContext oRemoteDB = new DatabaseContext();
            BLEModel oModel = new BLEModel();
            IList<BLEDataLog_REC> oTable = oModel.GetListDataLog(oRemoteDB, poRequest).ToList();
            var ds = oTable.ToDataSourceResult(poRequest);

            if (poRequest.Filters.Count > 0)
                ds.Total = oTable.Count;
            else
            {
                ds.Total = oModel.GetCountData(oRemoteDB, "t70308").SingleOrDefault().count;
                ds.Data = oTable;
            }

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;*/

            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<BLEDataLog_REC> Collections = database.GetCollection<BLEDataLog_REC>("BLEDataLog");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<BLEDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                    { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<BLEDataLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable;
            ds.Total = Convert.ToInt32(Collections.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public IEnumerable<BLEDataLog_REC> GetListDataLogReport([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            /*ElevatorModel oClass = new ElevatorModel();
            IEnumerable<ElevatorDataLog_REC> vResult = oClass.GetListDataLog().ToList();
            return vResult;*/
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<BLEDataLog_REC> Collections = database.GetCollection<BLEDataLog_REC>("BLEDataLog");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<BLEDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();
            if (filter_date != null)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) },
                    { "$lt", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            IEnumerable<BLEDataLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).ToList();
            return oTable;
        }
        [HttpPost]
        public ActionResult ExportServer([DataSourceRequest]DataSourceRequest poRequest, ParameterExport data)
        {
            var columnsData = data.columns.ToArray();
            SpreadDocumentFormat exportFormat = data.options.format.ToString() == "csv" ? exportFormat = SpreadDocumentFormat.Csv : exportFormat = SpreadDocumentFormat.Xlsx;
            Action<ExportCellStyle> cellStyle = new Action<ExportCellStyle>(ChangeCellStyle);
            Action<ExportRowStyle> rowStyle = new Action<ExportRowStyle>(ChangeRowStyle);
            Action<ExportColumnStyle> columnStyle = new Action<ExportColumnStyle>(ChangeColumnStyle);

            string fileName = string.Format("{0}.{1}", data.options.title, data.options.format);
            string mimeType = Helpers.GetMimeType(exportFormat);

            Stream exportStream = exportFormat == SpreadDocumentFormat.Xlsx ?
                GetListDataLogReport(poRequest, data.transport).ToXlsxStream(columnsData) :
                GetListDataLogReport(poRequest, data.transport).ToCsvStream(columnsData);

            var fileStreamResult = new FileStreamResult(exportStream, mimeType);
            fileStreamResult.FileDownloadName = fileName;
            fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);
            using (var fileStream = System.IO.File.Create(Server.MapPath("~" + Constants.ExportDirectory + fileName)))
            {
                fileStreamResult.FileStream.CopyTo(fileStream);
            }

            return Json(new { filename = fileName });
        }
        private void ChangeCellStyle(ExportCellStyle e)
        {
            bool isHeader = e.Row == 0;
            SpreadBorder border = new SpreadBorder(SpreadBorderStyle.Thick, new SpreadThemableColor(new SpreadColor(0, 0, 0)));
            SpreadCellFormat format = new SpreadCellFormat
            {
                //TopBorder = border,
                //BottomBorder = border,
                //LeftBorder = border,
                //RightBorder = border,
                //Fill = SpreadPatternFill.CreateSolidFill(new SpreadColor(255, 255, 255)),
                FontSize = 11,
                ForeColor = new SpreadThemableColor(new SpreadColor(0, 0, 0)),
                WrapText = false
            };
            e.Cell.SetFormat(format);
        }
        private void ChangeRowStyle(ExportRowStyle e)
        {
            e.Row.SetHeightInPixels(e.Index == 0 ? 80 : 30);
        }
        private void ChangeColumnStyle(ExportColumnStyle e)
        {
            double width = e.Name == "Item ID" || e.Name == "Barcode" ? 200 : 100;
            e.Column.SetWidthInPixels(width);
        }
    }
}