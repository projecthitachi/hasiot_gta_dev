﻿using BLE.Minew.Models.System;
using Kendo.Mvc;
using Kendo.Mvc.Export;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BLE.Minew.Models
{
    public class BLEDataLog_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        private long t70308r001;
        [Display(Name = "RecordID")]
        public virtual long RecordID { get { return t70308r001; } set { t70308r001 = value; } }

        private DateTime t70308r002;
        [Display(Name = "RecordTimestamp")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public virtual DateTime RecordTimestamp { get { return t70308r002; } set { t70308r002 = value; } }

        private Int32 t70308r003;
        [Display(Name = "Record Status")]
        public virtual Int32 RecordStatus { get { return t70308r003; } set { t70308r003 = value; } }

        private string t70308f001;
        [Display(Name = "Gateway ID")]
        public virtual string GatewayID { get { return t70308f001; } set { t70308f001 = value; } }

        private string t70308f002;
        [Display(Name = "BLE Tag ID")]
        public virtual string BLETagID { get { return t70308f002; } set { t70308f002 = value; } }

        private string t70308f003;
        [Display(Name = "Payload")]
        public virtual string Payload { get { return t70308f003; } set { t70308f003 = value; } }

        private string t70308f004;
        [Display(Name = "Value (RSSI)")]
        public virtual string Value { get { return t70308f004; } set { t70308f004 = value; } }
        public virtual string RSSI { get; set; }
        public virtual string Location { get; set; }
        public virtual string Person { get; set; }

    }
    public class BLEMinew_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string CategoryID { get; set; }
        public string IPAddress { get; set; }
        public string MacAddress { get; set; }
        public string SSID { get; set; }
        public string Password { get; set; }
        public string MQTTHost { get; set; }
        public string MQTTUsername { get; set; }
        public string MQTTPassword { get; set; }
        public string StatusDevice { get; set; }
        public int Live { get; set; }
        public string LocationID { get; set; }

        public int IsPresentRSSI { get; set; }
    }
    public class CountData
    {
        public int count { get; set; }
    }
    public class ParameterExport
    {
        public IList<ExportColumnSettings> columns { get; set; }
        public ParameterExportOptions options { get; set; }
        public FilterDate transport { get; set; }
    }
    public class FilterDate
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
    public class ParameterExportOptions
    {
        public string format { get; set; }
        public string title { get; set; }
    }
    public class BLEModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public BLEModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t70208fieldselect"
        private string t70208fieldselect = @"
            t70208r001 as RecordID,
            t70208r002 as RecordTimestamp,
            t70208r003 as RecordStatus,
            t70208f001 as DeviceID,
            t70208f002 as DeviceName,
            t70208f003 as CategoryID,
            t70208f004 as IPAddress,
            t70208f005 as MacAddress,
            t70208f006 as SSID,
            t70208f007 as Password,
            t70208f008 as MQTTHost,
            t70208f009 as MQTTUsername,
            t70208f010 as MQTTPassword,
            t70208f011 as StatusDevice,
            t70208f012 as Live,
            t70208f013 as IsPresentRSSI,
            t70208f014 as LocationID";
        #endregion

        #region "t70308fieldselect"
        private string t70308fieldselect = @"
            t70308r001 as RecordID,
            t70308r002 as RecordTimestamp,
            t70308r003 as RecordStatus,
            t70308f001 as GatewayID,
            t70308f002 as BLETagID,
            t70308f003 as Payload,
            t70308f004 as Value";
        #endregion

        public DbRawSqlQuery<BLEMinew_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " SELECT "+t70208fieldselect+" FROM dbo.t70208 ORDER BY t70208r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<BLEMinew_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<BLEMinew_REC> GetByGatewayID(DatabaseContext oRemoteDB, string sGatewayID)
        {
            string sSQL = " SELECT " + t70208fieldselect + " FROM dbo.t70208 where t70208f001 = '"+ sGatewayID + "' ORDER BY t70208r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<BLEMinew_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<BLEDataLog_REC> GetListDataLog(DatabaseContext oRemoteDB)
        {
            string sSQL = "SELECT " + t70308fieldselect + " FROM t70308 ORDER BY t70308r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<BLEDataLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<BLEDataLog_REC> GetListDataLog(DatabaseContext oRemoteDB, [DataSourceRequest]DataSourceRequest poRequest)
        {
            int start = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int end = poRequest.Page * poRequest.PageSize;

            string WhereClause = "";
            foreach (FilterDescriptor filter in poRequest.Filters)
            {
                WhereClause += filter.Member + " LIKE '%"+ filter.Value +"%' AND ";
            }

            string Offset = "";
            string Limit = "";
            if (WhereClause.Length == 0)
            {
                Offset = "([table].RowNum BETWEEN " + start + " AND " + end + ")";
                Limit = "TOP(" + poRequest.PageSize + ")";
            }
            else
                WhereClause = WhereClause.Remove(WhereClause.LastIndexOf("AND"), 3);

            string sSQL = "SELECT "+Limit+"* " +
                "FROM(SELECT " + t70308fieldselect + ", ROW_NUMBER() OVER(ORDER BY t70308r001 DESC) AS RowNum " +
                "FROM t70308) AS [table] " +
                "WHERE "+ WhereClause + Offset;

            var vQuery = oRemoteDB.Database.SqlQuery<BLEDataLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<CountData> GetCountData(DatabaseContext oRemoteDB, string table)
        {
            string sSQL = "SELECT COUNT(*) as count from "+ table;
            var vQuery = oRemoteDB.Database.SqlQuery<CountData>(sSQL);
            return vQuery;
        }
        public bool BLEDataLogInsert(DatabaseContext oRemoteDB, BLEDataLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                has.iot.Components.GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70308 " +
                    "      ( " +
                    "      t70308r002, t70308r003, " +
                    "      t70308f001, t70308f002, t70308f003, t70308f004" +
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70308r002, @pt70308r003, " +
                    "      @pt70308f001, @pt70308f002, @pt70308f003, @pt70308f004" +
                    "      )" +
                    ";" +
                    "SELECT @pt70308r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70308r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70308r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70308r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70308f001", poRecord.GatewayID));
                oParameters.Add(new SqlParameter("@pt70308f002", poRecord.BLETagID));
                oParameters.Add(new SqlParameter("@pt70308f003", poRecord.Payload));
                oParameters.Add(new SqlParameter("@pt70308f004", poRecord.Value));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        public bool Insert(DatabaseContext oRemoteDB, BLEMinew_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                has.iot.Components.GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70208 " +
                    "      ( " +
                    "      t70208r002, t70208r003, " +
                    "      t70208f001, t70208f002, t70208f003, t70208f004, t70208f005, t70208f006, t70208f007, t70208f008, t70208f009, t70208f010, t70208f011, t70208f012, t70208f013" +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70208r002, @pt70208r003, " +
                    "      @pt70208f001, @pt70208f002, @pt70208f003, @pt70208f004, @pt70208f005, @pt70208f006, @pt70208f007, @pt70208f008, @pt70208f009, @pt70208f010, @pt70208f011, @pt70208f012, @pt70208f013" +
                    "      )" +
                    ";" +
                    "SELECT @pt70208r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70208r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70208r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70208r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70208f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70208f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt70208f003", poRecord.CategoryID));
                oParameters.Add(new SqlParameter("@pt70208f004", poRecord.IPAddress));
                oParameters.Add(new SqlParameter("@pt70208f005", poRecord.MacAddress));
                oParameters.Add(new SqlParameter("@pt70208f006", poRecord.SSID));
                oParameters.Add(new SqlParameter("@pt70208f007", poRecord.Password));
                oParameters.Add(new SqlParameter("@pt70208f008", poRecord.MQTTHost));
                oParameters.Add(new SqlParameter("@pt70208f009", poRecord.MQTTUsername));
                oParameters.Add(new SqlParameter("@pt70208f010", poRecord.MQTTPassword));
                oParameters.Add(new SqlParameter("@pt70208f011", poRecord.StatusDevice));
                oParameters.Add(new SqlParameter("@pt70208f012", poRecord.Live));
                oParameters.Add(new SqlParameter("@pt70208f013", poRecord.IsPresentRSSI));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, BLEMinew_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            has.iot.Components.GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70208 " +
                "   SET " +
                "      t70208f002 = @pt70208f002, " +
                "      t70208f003 = @pt70208f003, " +
                "      t70208f004 = @pt70208f004, " +
                "      t70208f005 = @pt70208f005, " +
                "      t70208f006 = @pt70208f006, " +
                "      t70208f007 = @pt70208f007, " +
                "      t70208f008 = @pt70208f008, " +
                "      t70208f009 = @pt70208f009, " +
                "      t70208f010 = @pt70208f010, " +
                "      t70208f013 = @pt70208f013 " +
                "   WHERE (t70208r001 = @pt70208r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70208r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70208f002", poRecord.DeviceName));
            oParameters.Add(new SqlParameter("@pt70208f003", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70208f004", poRecord.IPAddress));
            oParameters.Add(new SqlParameter("@pt70208f005", poRecord.MacAddress));
            oParameters.Add(new SqlParameter("@pt70208f006", poRecord.SSID));
            oParameters.Add(new SqlParameter("@pt70208f007", poRecord.Password));
            oParameters.Add(new SqlParameter("@pt70208f008", poRecord.MQTTHost));
            oParameters.Add(new SqlParameter("@pt70208f009", poRecord.MQTTUsername));
            oParameters.Add(new SqlParameter("@pt70208f010", poRecord.MQTTPassword));
            oParameters.Add(new SqlParameter("@pt70208f013", poRecord.IsPresentRSSI));
            //oParameters.Add(new SqlParameter("@pt70208f011", poRecord.StatusDevice));
            //oParameters.Add(new SqlParameter("@pt70208f012", poRecord.StatusSwitch));
            //oParameters.Add(new SqlParameter("@pt70208f013", poRecord.Live));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateStatus(DatabaseContext oRemoteDB, BLEMinew_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            //GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70208 " +
                "   SET " +
                "      t70208f011 = @pt70208f011 " +
                "   WHERE (t70208f001 = @pt70208f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70208f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70208f011", poRecord.StatusDevice));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, BLEMinew_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t70208 " +
                "   WHERE (t70208f001 = @pt70208f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70208f001", poRecord.DeviceID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }

    }
}