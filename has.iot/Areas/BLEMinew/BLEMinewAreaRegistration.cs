﻿using has.iot.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
namespace MVCPluggableDemo
{
    public class BLEAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BLEMinew";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "BLEMinew_default",
                "BLEMinew/{controller}/{action}/{id}",
                new {controller= "Minew", action = "Index", id = UrlParameter.Optional },
                new string[] { "BLE.Minew.Controllers" }
            );          
        }
    }
}