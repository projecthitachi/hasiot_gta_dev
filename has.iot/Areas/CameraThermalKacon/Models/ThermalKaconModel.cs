﻿using Camera.Thermal.Kacon.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Camera.Thermal.Kacon.Models
{
    public class ThermalKacon_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string CategoryID { get; set; }
        public string IPAddress { get; set; }
        public string Resolution { get; set; }
        public string StatusDevice { get; set; }
        public string LocationID { get; set; }

        public string LocationName { get; set; }
    }
    public class ThermalKaconDefault_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string CategoryID { get; set; }
        public decimal MaxTemperature { get; set; }
        public decimal MinTemperature { get; set; }
        public string Resolution { get; set; }
        public string URLService { get; set; }
    }
    public class ThermalModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public ThermalModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t70203fieldselect"
        private string t70203fieldselect = @"
            t70203r001 as RecordID,
            t70203r002 as RecordTimestamp,
            t70203r003 as RecordStatus,
            t70203f001 as DeviceID,
            t70203f002 as DeviceName,
            t70203f003 as CategoryID,
            t70203f004 as IPAddress,
            t70203f005 as Resolution,
            t70203f006 as LocationID";
        #endregion
        
        #region "t70103fieldselect"
        private string t70103fieldselect = @"
            t70103r001 as RecordID,
            t70103r002 as RecordTimestamp,
            t70103r003 as RecordStatus,
            t70103f001 as CategoryID,
            t70103f002 as MaxTemperature,
            t70103f003 as MinTemperature,
            t70103f004 as Resolution,
            t70103f005 as URLService";
        #endregion
        public DbRawSqlQuery<ThermalKacon_REC> GetList(DatabaseContext oRemoteDB)
        {
            string sSQL = " SELECT "+t70203fieldselect+ ", t8030f002 as LocationName  FROM dbo.t70203 LEFT JOIN t8030 on t8030f001 = t70203f006 ORDER BY t70203r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ThermalKacon_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ThermalKacon_REC> GetByGatewayID(DatabaseContext oRemoteDB, string sGatewayID)
        {
            string sSQL = " SELECT " + t70203fieldselect + ", t8030f002 as LocationName  FROM dbo.t70203 LEFT JOIN t8030 on t8030f001 = t70203f006 where t70203f001 = '" + sGatewayID + "' ORDER BY t70203r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ThermalKacon_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ThermalKaconDefault_REC> GetDefaultSetting(DatabaseContext oRemoteDB)
        {
            string sSQL = " SELECT TOP(1)" + t70103fieldselect + " FROM dbo.t70103 ORDER BY t70103r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ThermalKaconDefault_REC>(sSQL);
            return vQuery;
        }
        public bool Insert(DatabaseContext oRemoteDB, ThermalKacon_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                has.iot.Components.GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70203 " +
                    "      ( " +
                    "      t70203r002, t70203r003, " +
                    "      t70203f001, t70203f002, t70203f003, t70203f004, t70203f005, t70203f006" +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70203r002, @pt70203r003, " +
                    "      @pt70203f001, @pt70203f002, @pt70203f003, @pt70203f004, @pt70203f005, @pt70203f006" +
                    "      )" +
                    ";" +
                    "SELECT @pt70203r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70203r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70203r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70203r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70203f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70203f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt70203f003", "CameraThermal"));
                oParameters.Add(new SqlParameter("@pt70203f004", poRecord.IPAddress));
                oParameters.Add(new SqlParameter("@pt70203f005", poRecord.Resolution));
                oParameters.Add(new SqlParameter("@pt70203f006", poRecord.LocationID));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, ThermalKacon_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            has.iot.Components.GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70203 " +
                "   SET " +
                "      t70203f001 = @pt70203f001, " +
                "      t70203f002 = @pt70203f002, " +
                "      t70203f003 = @pt70203f003, " +
                "      t70203f004 = @pt70203f004, " +
                "      t70203f005 = @pt70203f005, " +
                "      t70203f006 = @pt70203f006 " +
                "   WHERE (t70203r001 = @pt70203r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70203r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70203f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70203f002", poRecord.DeviceName));
            oParameters.Add(new SqlParameter("@pt70203f003", "CameraThermal"));
            oParameters.Add(new SqlParameter("@pt70203f004", poRecord.IPAddress));
            oParameters.Add(new SqlParameter("@pt70203f005", poRecord.Resolution));
            oParameters.Add(new SqlParameter("@pt70203f006", poRecord.LocationID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, ThermalKacon_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t70203 " +
                "   WHERE (t70203r001 = @pt70203r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70203r001", poRecord.RecordID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }


        public long InsertDefault(DatabaseContext oRemoteDB, ThermalKaconDefault_REC poRecord)
        {
            long bReturn = 0;
            //------------------------------
            poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;

            has.iot.Components.GlobalFunction.TrimNull(poRecord);
            //----------
            string sSQL = "" +
                "INSERT INTO t70103 " +
                "      ( " +
                "      t70103r002, t70103r003, " +
                "      t70103f001, t70103f002, t70103f003,t70103f004,t70103f005) " +
                "      VALUES " +
                "      (" +
                "      @pt70103r002, @pt70103r003, " +
                "      @pt70103f001, @pt70103f002, @pt70103f003,@pt70103f004,@pt70103f005)" +
                ";" +
                "SELECT @pt70103r001 = SCOPE_IDENTITY(); "
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter { ParameterName = "@pt70103r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
            oParameters.Add(new SqlParameter("@pt70103r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70103r003", poRecord.RecordStatus));

            oParameters.Add(new SqlParameter("@pt70103f001", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70103f002", poRecord.MaxTemperature));
            oParameters.Add(new SqlParameter("@pt70103f003", poRecord.MinTemperature));
            oParameters.Add(new SqlParameter("@pt70103f004", poRecord.Resolution));
            oParameters.Add(new SqlParameter("@pt70103f005", poRecord.URLService));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

            if (nReturn == 1)
            {
                bReturn = Convert.ToInt64(vSqlParameter[0].Value);
            }
            else
            {
                ErrorMessage = "Failed to insert record!";
                bReturn = 0;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateDefault(DatabaseContext oRemoteDB, ThermalKaconDefault_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            //poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            has.iot.Components.GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70103 " +
                "   SET " +
                "      t70103f002 = @pt70103f002, " +
                "      t70103f003 = @pt70103f003, " +
                "      t70103f004 = @pt70103f004, " +
                "      t70103f005 = @pt70103f005 " +
                "   WHERE (t70103r001 = @pt70103r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));

            oParameters.Add(new SqlParameter("@pt70103r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70103f001", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70103f002", poRecord.MaxTemperature));
            oParameters.Add(new SqlParameter("@pt70103f003", poRecord.MinTemperature));
            oParameters.Add(new SqlParameter("@pt70103f004", poRecord.Resolution));
            oParameters.Add(new SqlParameter("@pt70103f005", poRecord.URLService));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {
                bReturn = true;
            }
            else
            {
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
    }
}