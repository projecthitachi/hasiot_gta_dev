﻿using has.iot.Components;
using has.iot.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Camera.Thermal.Kacon.Models;
using Camera.Thermal.Kacon.Models.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt.Messages;
using Kendo.Mvc.Export;
using Telerik.Documents.SpreadsheetStreaming;
using System.IO;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace Camera.Thermal.Kacon.Controllers
{
    public class ThermalKaconController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DataLog()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest]DataSourceRequest poRequest)
        {
            ThermalModel oClass = new ThermalModel();
            DatabaseContext oRemoteDB = new DatabaseContext();
            var vResult = oClass.GetList(oRemoteDB).ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest]DataSourceRequest poRequest, ThermalKacon_REC poRecord)
        {
            if ((poRecord != null))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    ThermalModel oClass = new ThermalModel();
                    if (!oClass.Insert(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest]DataSourceRequest poRequest, ThermalKacon_REC poRecord)
        {
            if ((poRecord != null))
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    ThermalModel oClass = new ThermalModel();
                    if (!oClass.Update(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest poRequest, ThermalKacon_REC poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    ThermalModel oClass = new ThermalModel();
                    if (!oClass.Delete(oRemoteDB, poRecord))
                    {
                        ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        public ActionResult SubmitDefault(ThermalKaconDefault_REC poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    ThermalModel oQuery = new ThermalModel();
                    ThermalKaconDefault_REC voTable = oQuery.GetDefaultSetting(oRemoteDB).SingleOrDefault();
                    long PRI = 0;
                    if (voTable == null)
                        PRI = oQuery.InsertDefault(oRemoteDB, poData);
                    else
                        oQuery.UpdateDefault(oRemoteDB, poData);

                    oTransactions.Commit();

                    ResultData.Add("RecordID", PRI.ToString());
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Insert Default Settings " + poData.CategoryID + " Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }

            return Json(ResultData);
        }
        public ActionResult SubmitAllDefault(ThermalKaconDefault_REC poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    ThermalModel oQuery = new ThermalModel();
                    ThermalKaconDefault_REC voTable = oQuery.GetDefaultSetting(oRemoteDB).SingleOrDefault();
                    if (voTable != null)
                    {
                        IList<ThermalKacon_REC> voList = oQuery.GetList(oRemoteDB).ToList();
                        foreach (ThermalKacon_REC voData in voList)
                        {
                            voData.RecordTimestamp = DateTime.Now;
                            voData.Resolution = voTable.Resolution;

                            oQuery.Update(oRemoteDB, voData);
                        }
                    }

                    oTransactions.Commit();

                    ResultData.Add("RecordID", voTable.RecordID.ToString());
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Submit to all devices setting Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }

            return Json(ResultData);
        }
    }
}