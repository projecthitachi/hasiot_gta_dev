﻿using Camera.Thermal.Kacon.Models;
using Camera.Thermal.Kacon.Models.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Camera.Thermal.Kacon.Components
{
    public class Constants
    {
        public static ThermalKaconDefault_REC GetDefaultSetting()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            ThermalModel oQuery = new ThermalModel();
            ThermalKaconDefault_REC voData = oQuery.GetDefaultSetting(oRemoteDB).SingleOrDefault();
            return voData;
        }
    }
}