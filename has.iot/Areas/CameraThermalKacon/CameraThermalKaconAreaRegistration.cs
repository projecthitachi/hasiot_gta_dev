﻿using has.iot.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
namespace MVCPluggableDemo
{
    public class CameraThermalKaconRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CameraThermalKacon";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CameraThermalKacon_default",
                "CameraThermalKacon/{controller}/{action}/{id}",
                new {controller= "CameraThermalKacon", action = "Index", id = UrlParameter.Optional },
                new string[] { "Camera.Thermal.Kacon.Controllers" }
            );
        }
    }
}