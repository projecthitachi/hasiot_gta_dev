﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using RFID.Impinj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using has.iot.Components;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.ServiceProcess;
using Newtonsoft.Json;
using Kendo.Mvc.Export;
using Telerik.Documents.SpreadsheetStreaming;
using System.IO;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.SignalR;
using RFID.Impinj.Components;
using System.ComponentModel;
using System.Web.Configuration;
using MongoDB.Driver;
using MongoDB.Bson;
using Kendo.Mvc;
using System.Text.RegularExpressions;

namespace RFID.Impinj.Controllers
{
    public class ImpinjController : Controller
    {
        public static int presencetime_delay = Convert.ToInt32(WebConfigurationManager.AppSettings["human_presence_clear_time"]);

        // GET: RFID
        public ActionResult Index()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            ImpinjModel oModel = new ImpinjModel();
            ImpinjDefault_REC oData = oModel.GetDefault(oRemoteDB).SingleOrDefault();
            IList<ImpinjAntennaDefault_REC> oDataAntenna = new List<ImpinjAntennaDefault_REC>();

            if (oData != null)
                oDataAntenna = oModel.GetAntennaDefaultByParentID(oRemoteDB, oData.RecordID).ToList();
            else
            {
                oData = new ImpinjDefault_REC();
                oData.ReaderMode = "";
                oData.SearchMode = "";
            }

            ViewData["Data"] = oData;
            ViewData["DataAntenna"] = oDataAntenna;
            return View();
        }
        public static IHubContext RT = null;
        public static void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ImpinjDataLog_REC> ImpinjDataLog = database.GetCollection<ImpinjDataLog_REC>("RFIDDataLog");
            IMongoCollection<ImpinjLocationLog_REC> ImpinjLocationLog = database.GetCollection<ImpinjLocationLog_REC>("RFIDLocationLog");
            IMongoCollection<ImpinjLocationLog_REC> ImpinjPresenceLocation = database.GetCollection<ImpinjLocationLog_REC>("RFIDPresenceLocation");
            //------------------
            string[] topic = e.Topic.Split('/');
            RT = GlobalHost.ConnectionManager.GetHubContext<RealTimeHub>();
            if(topic[3] == "data")
            {
                var message = System.Text.Encoding.Default.GetString(e.Message);
                if (GlobalFunction.IsValidJson(message))
                {
                    object result = JsonConvert.DeserializeObject(message);
                    JObject obj = JObject.Parse(result.ToString());
                    DatabaseContext oRemoteDB = new DatabaseContext();
                    ImpinjModel oQuery = new ImpinjModel();
                    
                    ImpinjDataLog_REC poRecord = new ImpinjDataLog_REC();
                    poRecord.RecordTimestamp = DateTime.Now;
                    poRecord.ReaderID = obj["Sender"]["Name"].ToString();
                    poRecord.RFIDTagID = obj["Epc"]["Data"].ToString();
                    poRecord.Payload = JsonConvert.SerializeObject(obj);
                    poRecord.Value = obj["PeakRssiInDbm"].ToString();
                    poRecord.Antenna = obj["AntennaPortNumber"].ToString();

                    var filter = new BsonDocument(){
                        {"RecordTimestamp", new BsonDocument(){
                            {"$gte", DateTime.Parse(DateTime.Now.AddSeconds(-1).ToString("yyyy-MM-ddTHH:mm:ss").Replace(".",":")) }
                        }},
                        {"RFIDTagID", new BsonDocument(){
                            {"$eq", poRecord.RFIDTagID}
                        }},
                        {"Antenna", new BsonDocument(){
                            {"$eq", poRecord.Antenna}
                        }}
                    };

                    long lastCount = ImpinjDataLog.Find(filter).Count();
                    if (lastCount == 0)
                    {
                        ImpinjDataLog.InsertOne(poRecord);
                        //oQuery.ImpinjDataLogInsert(oRemoteDB, poRecord);
                        ImpinjAntenna_REC voAntenna = oQuery.GetAntennaByReaderIDAndPort(oRemoteDB, poRecord.ReaderID, Convert.ToInt32(poRecord.Antenna)).SingleOrDefault();

                        has.iot.Models.PersonModel oQueryPerson = new has.iot.Models.PersonModel();
                        has.iot.Models.Person_REC voPerson = oQueryPerson.GetPersonByEPC(poRecord.RFIDTagID).SingleOrDefault();

                        if (voAntenna != null && voPerson != null)
                        {
                            //dont_allow_direct_l2(voLoc, voPerson, oQuery, oRemoteDB, poRecord);
                            one_second_unique(voAntenna, voPerson, poRecord, ImpinjLocationLog, ImpinjPresenceLocation);
                        }

                        RT.Clients.All.RFIDImpinj("DataLog", poRecord.Payload, "RFIDImpinj");
                    }
                    
                }
            }else if (topic[3] == "connectdata" || topic[3] == "disconnectdata" || topic[3] == "startdata" || topic[3] == "stopdata")
            {
                RT.Clients.All.RFIDImpinj("GridCommand", topic[3], "RFIDImpinj");
            }
            else if(topic[3] == "request_constring")
            {
                string value = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Constants.MqttClient.Publish("RFID/Impinj/service/set_constring", Encoding.UTF8.GetBytes(value), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
        }

        public static void one_second_unique(ImpinjAntenna_REC voAntenna, has.iot.Models.Person_REC voPerson, ImpinjDataLog_REC poRecord, IMongoCollection<ImpinjLocationLog_REC> ImpinjLocationLog, IMongoCollection<ImpinjLocationLog_REC> ImpinjPresenceLocation)
        {

            ImpinjLocationLog_REC poLocLog = new ImpinjLocationLog_REC();
            poLocLog.RecordTimestamp = DateTime.Now;
            poLocLog.RecordStatus = 0;
            poLocLog.EPC = poRecord.RFIDTagID;
            poLocLog.LocationID = voAntenna.LocationID;
            poLocLog.LocationName = voAntenna.LocationName;
            poLocLog.PersonID = voPerson.PersonID;
            poLocLog.PersonName = voPerson.PersonName;
            poLocLog.RSSI = Convert.ToDecimal(poRecord.Value);
            ImpinjLocationLog.InsertOne(poLocLog);

            var log = ImpinjPresenceLocation.Find(_ => true).ToList();
            if (log.Count > 0)
            {
                ImpinjLocationLog_REC documentBefore = ImpinjPresenceLocation.Find(w => w.EPC == poLocLog.EPC).SingleOrDefault();
                if (documentBefore != null)
                {
                    if (documentBefore.LocationID == voAntenna.LocationID)
                    {
                        FilterDefinitionBuilder<ImpinjLocationLog_REC> whereclause_builder = Builders<ImpinjLocationLog_REC>.Filter;
                        FilterDefinition<ImpinjLocationLog_REC> where_clause = whereclause_builder.Eq<string>("EPC", poLocLog.EPC);
                        var update = Builders<ImpinjLocationLog_REC>.Update
                            .Set(p => p.RecordTimestamp, DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")))
                            .Set(p => p.LocationID, voAntenna.LocationID)
                            .Set(p => p.RSSI, Convert.ToDecimal(poRecord.Value))
                            .Set(p => p.LocationName, voAntenna.LocationName)
                            .Set(p => p.PersonID, voPerson.PersonID);
                        ImpinjPresenceLocation.UpdateOne(where_clause, update);
                        // The document already existed and was updated.
                    }
                    else
                    {
                        if (documentBefore.RSSI < Convert.ToDecimal(poRecord.Value) || documentBefore.RecordTimestamp < DateTime.Now.AddSeconds(-3))
                        {
                            FilterDefinitionBuilder<ImpinjLocationLog_REC> whereclause_builder = Builders<ImpinjLocationLog_REC>.Filter;
                            FilterDefinition<ImpinjLocationLog_REC> where_clause = whereclause_builder.Eq<string>("EPC", poLocLog.EPC);
                            var update = Builders<ImpinjLocationLog_REC>.Update
                                .Set(p => p.RecordTimestamp, DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")))
                                .Set(p => p.LocationID, voAntenna.LocationID)
                                .Set(p => p.RSSI, Convert.ToDecimal(poRecord.Value))
                                .Set(p => p.LocationName, voAntenna.LocationName)
                                .Set(p => p.PersonID, voPerson.PersonID);
                            ImpinjPresenceLocation.UpdateOne(where_clause, update);
                        }
                    }
                }
                else
                    ImpinjPresenceLocation.InsertOne(poLocLog);
            }
            else
                ImpinjPresenceLocation.InsertOne(poLocLog);

            sendSignaltoView(ImpinjPresenceLocation, poRecord);
        }
        public static void sendSignaltoView(IMongoCollection<ImpinjLocationLog_REC> ImpinjPresenceLocation, ImpinjDataLog_REC poRecord)
        {
            var filter = new BsonDocument(){
                {"RecordTimestamp", new BsonDocument(){
                    {"$gte", DateTime.Parse(DateTime.Now.AddSeconds(presencetime_delay*-1).ToString("yyyy-MM-ddTHH:mm:ss").Replace(".",":")) }
                }}
            };

            List<ImpinjLocationLog_REC> items = ImpinjPresenceLocation.Find(filter).ToList();
            RT.Clients.All.RFIDImpinj("PresenceLocation", items, "RFIDImpinj");

            //publish to has.app
            var pipeline = new[]{
                new BsonDocument(){
                {
                    "$match", new BsonDocument(){
                        {
                            "RecordTimestamp", new BsonDocument(){
                                { "$gte", DateTime.Parse(DateTime.Now.AddSeconds(presencetime_delay*-1).ToString("yyyy-MM-ddTHH:mm:ss").Replace(".",":")) }
                            }
                        }
                    }
                }
            },
            new BsonDocument(){
                {"$group", new BsonDocument(){
                    {"_id", new BsonDocument(){
                        {"LocationID", "$LocationID"},
                        {"LocationName", "$LocationName"},
                        {"Count", "$EPC"}
                    }}
                }}
            },new BsonDocument(){
                {"$group", new BsonDocument(){
                    {"_id", new BsonDocument(){
                        {"LocationID", "$_id.LocationID"},
                        {"LocationName", "$_id.LocationName"}
                    }},
                    {"Count", new BsonDocument(){
                        {"$sum", 1}
                    }}
                }}
            },new BsonDocument(){
                {"$project", new BsonDocument(){
                    {"_id", 0},
                    {"LocationID", "$_id.LocationID"},
                    {"LocationName", "$_id.LocationName"},
                    {"Count", 1}
                }}
            }};

            List<ImpinjLocationLog_REC> result = ImpinjPresenceLocation.Aggregate<ImpinjLocationLog_REC>(pipeline).ToList();
            RT.Clients.All.RFIDImpinj("PresenceLocationCounting", result, "RFIDImpinj");
            Constants.MqttClient.Publish("RFID/Impinj/" + poRecord.ReaderID + "/AppDatas", Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        }
        public static void read_write_presence_location(ImpinjLocationLog_REC poLocLog)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~/Components/Plugins/RFIDImpinj/");
            string filename = "presence_location.json";
            string fullpath = path + filename;
            System.IO.Directory.CreateDirectory(path);

            if (!System.IO.File.Exists(fullpath))
                System.IO.File.Create(fullpath).Dispose();

            using (StreamReader r = new StreamReader(fullpath))
            {
                List<ImpinjLocationLog_REC> items = new List<ImpinjLocationLog_REC>();
                string json = r.ReadToEnd();
                r.Dispose();

                if (GlobalFunction.IsValidJson(json))
                {
                    items = JsonConvert.DeserializeObject<List<ImpinjLocationLog_REC>>(json);
                    //remove undetected item
                    items.RemoveAll(w => w.RecordTimestamp.Subtract(DateTime.Now).TotalMinutes * -1 > 1);

                    int idx = items.FindIndex(w => w.EPC == poLocLog.EPC);
                    if (idx > -1)
                        items[idx] = poLocLog;
                    else
                        items.Add(poLocLog);
                }
                else
                    items.Add(poLocLog);

                json = JsonConvert.SerializeObject(items.ToArray());

                //write string to file
                System.IO.File.WriteAllText(fullpath, json);
            }
        }
        public static void dont_allow_direct_l2(ImpinjAntenna_REC voAntenna, has.iot.Models.Person_REC voPerson, ImpinjModel oQuery, DatabaseContext oRemoteDB, ImpinjDataLog_REC poRecord)
        {
            if (voAntenna.LocationID == voAntenna.LocationID)
            {
                ImpinjLocationLog_REC GetLastDetected = oQuery.DataLocLogGetByEPC(oRemoteDB, poRecord.RFIDTagID).SingleOrDefault();
                if (GetLastDetected != null)
                {
                    ImpinjLocationLog_REC poLocLog = new ImpinjLocationLog_REC();
                    poLocLog.RecordTimestamp = DateTime.Now;
                    poLocLog.RecordStatus = 0;
                    poLocLog.EPC = poRecord.RFIDTagID;
                    poLocLog.LocationID = voAntenna.LocationID;
                    poLocLog.LocationName = voAntenna.LocationName;
                    poLocLog.PersonID = voPerson.PersonID;
                    poLocLog.PersonName = voPerson.PersonName;
                    poLocLog.RSSI = Convert.ToDecimal(poRecord.Value);
                    poLocLog.RecordID = oQuery.ImpinjLocationLogInsert(oRemoteDB, poLocLog);

                    string path = System.Web.Hosting.HostingEnvironment.MapPath("~/Components/Plugins/RFIDImpinj/");
                    string filename = "presence_location.json";
                    string fullpath = path + filename;
                    System.IO.Directory.CreateDirectory(path);

                    if (!System.IO.File.Exists(fullpath))
                        System.IO.File.Create(fullpath).Dispose();

                    using (StreamReader r = new StreamReader(fullpath))
                    {
                        List<ImpinjLocationLog_REC> items = new List<ImpinjLocationLog_REC>();
                        string json = r.ReadToEnd();
                        r.Dispose();

                        if (GlobalFunction.IsValidJson(json))
                        {
                            items = JsonConvert.DeserializeObject<List<ImpinjLocationLog_REC>>(json);
                            //remove undetected item
                            items.RemoveAll(w => w.RecordTimestamp.Subtract(DateTime.Now).TotalMinutes * -1 > 1);

                            int idx = items.FindIndex(w => w.EPC == poLocLog.EPC);
                            if (idx > -1)
                                items[idx] = poLocLog;
                            else
                                items.Add(poLocLog);
                        }
                        else
                            items.Add(poLocLog);

                        json = JsonConvert.SerializeObject(items.ToArray());

                        //write string to file
                        System.IO.File.WriteAllText(fullpath, json);
                    }
                }
            }
            else
            {
                ImpinjLocationLog_REC poLocLog = new ImpinjLocationLog_REC();
                poLocLog.RecordTimestamp = DateTime.Now;
                poLocLog.RecordStatus = 0;
                poLocLog.EPC = poRecord.RFIDTagID;
                poLocLog.LocationID = voAntenna.LocationID;
                poLocLog.LocationName = voAntenna.LocationName;
                poLocLog.PersonID = voPerson.PersonID;
                poLocLog.PersonName = voPerson.PersonName;
                poLocLog.RSSI = Convert.ToDecimal(poRecord.Value);
                poLocLog.RecordID = oQuery.ImpinjLocationLogInsert(oRemoteDB, poLocLog);

                string path = System.Web.Hosting.HostingEnvironment.MapPath("~/Components/Plugins/RFIDImpinj/");
                string filename = "presence_location.json";
                string fullpath = path + filename;
                System.IO.Directory.CreateDirectory(path);

                if (!System.IO.File.Exists(fullpath))
                    System.IO.File.Create(fullpath).Dispose();

                using (StreamReader r = new StreamReader(fullpath))
                {
                    List<ImpinjLocationLog_REC> items = new List<ImpinjLocationLog_REC>();
                    string json = r.ReadToEnd();
                    r.Dispose();

                    if (GlobalFunction.IsValidJson(json))
                    {
                        items = JsonConvert.DeserializeObject<List<ImpinjLocationLog_REC>>(json);
                        //remove undetected item
                        items.RemoveAll(w => w.RecordTimestamp.Subtract(DateTime.Now).TotalMinutes * -1 > 1);

                        int idx = items.FindIndex(w => w.EPC == poLocLog.EPC);
                        if (idx > -1)
                            items[idx] = poLocLog;
                        else
                            items.Add(poLocLog);
                    }
                    else
                        items.Add(poLocLog);

                    json = JsonConvert.SerializeObject(items.ToArray());

                    //write string to file
                    System.IO.File.WriteAllText(fullpath, json);
                }
            }
        }

        public ActionResult GetList([DataSourceRequest]DataSourceRequest poRequest)
        {
            ImpinjModel oModel = new ImpinjModel();
            IList<Impinj_REC> oTable = oModel.GetList().ToList();
            var jsonResult = Json(oTable.ToDataSourceResult(poRequest));
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult GetRegionByModel(string Model)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            ImpinjModel oModel = new ImpinjModel();
            IList<has.iot.Models.GeneralTables> oTable = oModel.GetRegionByModel(oRemoteDB,Model).ToList();
            return Json(oTable);
        }
        public ActionResult Command(string cmd, string value)
        {
            string msg = "success";
            try
            {
                ServiceController sc = new ServiceController("HASIOT - RFID.Impinj.Service");
                if (sc.Status == ServiceControllerStatus.Running)
                {
                    Constants.MqttClient.Publish("RFID/Impinj/"+value+"/"+cmd, Encoding.UTF8.GetBytes(value), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
                else
                    msg = "Command Failed. HASIOT - RFIDService is not running.";
            }
            catch(Exception ex)
            {
                msg = ex.Message;
            }

            return Json(new { msg = msg });
        }

        public ActionResult Form(long? RecordID)
        {
            string view = "Form";
            if(RecordID != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                ImpinjModel oModel = new ImpinjModel();
                Impinj_REC oData = oModel.GetOne(oRemoteDB,RecordID.Value).SingleOrDefault();
                IList<ImpinjAntenna_REC> oDataAntenna = oModel.GetAntennaByParentID(oRemoteDB, RecordID.Value).ToList();
                ViewData["Data"] = oData;
                ViewData["DataAntenna"] = oDataAntenna;
                view = "FormEdit";
            }
            
            return View(view);
        }
        public ActionResult DataLog()
        {
            return View();
        }
        public ActionResult PresenceLocation()
        {
            return View();
        }
        public ActionResult PresenceLocationPlayback()
        {
            return View();
        }

        public ActionResult GetListDataLog([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            /*
             // sql server datalog
            DatabaseContext oRemoteDB = new DatabaseContext();
            ImpinjModel oModel = new ImpinjModel();
            IList<ImpinjDataLog_REC> oTable = oModel.GetListDataLog(oRemoteDB, poRequest).ToList();
            var ds = oTable.ToDataSourceResult(poRequest);

            if (poRequest.Filters.Count > 0)
                ds.Total = oTable.Count;
            else
            {
                ds.Total = oModel.GetCountData(oRemoteDB, "t70307").SingleOrDefault().count;
                ds.Data = oTable;
            }

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            */

            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ImpinjDataLog_REC> Collections = database.GetCollection<ImpinjDataLog_REC>("RFIDDataLog");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<ImpinjDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                    { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<ImpinjDataLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable;
            ds.Total = Convert.ToInt32(Collections.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult GetListDataLocLog([DataSourceRequest]DataSourceRequest poRequest)
        {
            /*
             * // sql server datalog
            DatabaseContext oRemoteDB = new DatabaseContext();
            ImpinjModel oModel = new ImpinjModel();
            IList<ImpinjLocationLog_REC> oTable = oModel.GetListDataLocLog(oRemoteDB, poRequest).ToList();
            var ds = oTable.ToDataSourceResult(poRequest);
            if (poRequest.Filters.Count > 0)
                ds.Total = oTable.Count;
            else
            {
                ds.Total = oModel.GetCountData(oRemoteDB, "t70304").SingleOrDefault().count;
                ds.Data = oTable;
            }

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            */

            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ImpinjLocationLog_REC> ImpinjLocationLog = database.GetCollection<ImpinjLocationLog_REC>("RFIDLocationLog");
            List<ImpinjLocationLog_REC> ds = ImpinjLocationLog.Find(_ => true).ToList();
            var jsonResult = Json(ds.ToDataSourceResult(poRequest));
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult GetListPresenceLocation([DataSourceRequest]DataSourceRequest poRequest)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ImpinjLocationLog_REC> ImpinjPresenceLocation = database.GetCollection<ImpinjLocationLog_REC>("RFIDPresenceLocation");

            var filter = new BsonDocument(){
                        {"RecordTimestamp", new BsonDocument(){
                            {"$gte", DateTime.Parse(DateTime.Now.AddSeconds(presencetime_delay*-1).ToString("yyyy-MM-ddTHH:mm:ss").Replace(".",":")) }
                        }}
                    };

            List<ImpinjLocationLog_REC> oTable = ImpinjPresenceLocation.Find(filter).ToList();
            var jsonResult = Json(oTable.ToDataSourceResult(poRequest));
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult PresenceLocationPlaybackFilter([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            /*
             // sql server datalog
             * DatabaseContext oRemoteDB = new DatabaseContext();
            ImpinjModel oClass = new ImpinjModel();
            IEnumerable<ImpinjLocationLog_REC> vResult = oClass.PresenceLocationPlayback(oRemoteDB, Data).ToList();
            var jsonResult = Json(vResult.ToDataSourceResult(poRequest));
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            */
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ImpinjLocationLog_REC> Collections = database.GetCollection<ImpinjLocationLog_REC>("RFIDLocationLog");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<ImpinjLocationLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();

            if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
                    { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<ImpinjLocationLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable;
            ds.Total = Convert.ToInt32(Collections.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult Create(Impinj_REC Reader, List<ImpinjAntenna_REC> AntennaPortData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    ImpinjModel oQuery = new ImpinjModel();
                    long PRI = oQuery.Insert(oRemoteDB, Reader);
                    foreach(ImpinjAntenna_REC oAntennaPortData in AntennaPortData)
                    {
                        oAntennaPortData.ReaderID = PRI;
                        oQuery.InsertAntenna(oRemoteDB, oAntennaPortData);
                    }
                    oTransactions.Commit();

                    ResultData.Add("RecordID", PRI.ToString());
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Insert " + Reader.DeviceID + " Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }

            return Json(ResultData);
        }
        public ActionResult Update(Impinj_REC Reader, List<ImpinjAntenna_REC> AntennaPortData, List<ImpinjAntenna_REC> AntennaPortDataDeleted)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    ImpinjModel oQuery = new ImpinjModel();
                    oQuery.Update(oRemoteDB, Reader);

                    foreach (ImpinjAntenna_REC oAntennaPortData in AntennaPortData)
                    {
                        oAntennaPortData.ReaderID = Reader.RecordID;
                        if (oAntennaPortData.RecordID == null)
                            oQuery.InsertAntenna(oRemoteDB, oAntennaPortData);
                        else
                            oQuery.UpdateAntenna(oRemoteDB, oAntennaPortData);
                    }
                    if (AntennaPortDataDeleted != null)
                    {
                        foreach (ImpinjAntenna_REC oAntennaPortData in AntennaPortDataDeleted)
                        {
                            oQuery.DeleteAntenna(oRemoteDB, oAntennaPortData.RecordID.Value);
                        }
                    }
                    oTransactions.Commit();

                    ResultData.Add("errorcode", "0");
                    ResultData.Add("RecordID", Reader.RecordID.ToString());
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Update " + Reader.DeviceID + " Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }

            return Json(ResultData);
        }
        public ActionResult Delete(Impinj_REC Reader)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    ImpinjModel oQuery = new ImpinjModel();
                    oQuery.Delete(oRemoteDB, Reader);
                    oTransactions.Commit();

                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Delete " + Reader.DeviceID + " Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }

            return Json(ResultData);
        }

        public ActionResult SubmitDefault(ImpinjDefault_REC Reader, List<ImpinjAntennaDefault_REC> AntennaPortData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    ImpinjModel oQuery = new ImpinjModel();
                    ImpinjDefault_REC voTable = oQuery.GetDefault(oRemoteDB).SingleOrDefault();
                    long PRI = 0;
                    if (voTable == null)
                    {
                        PRI = oQuery.InsertDefault(oRemoteDB, Reader);
                        foreach (ImpinjAntennaDefault_REC oAntennaPortData in AntennaPortData)
                        {
                            oAntennaPortData.ReaderID = PRI;
                            oQuery.InsertDefaultAntenna(oRemoteDB, oAntennaPortData);
                        }
                    }
                    else
                    {
                        PRI = voTable.RecordID;
                        oQuery.UpdateDefault(oRemoteDB, Reader);

                        foreach (ImpinjAntennaDefault_REC oAntennaPortData in AntennaPortData)
                        {
                            oAntennaPortData.ReaderID = PRI;
                            if(oAntennaPortData.RecordID != null)
                                oQuery.UpdateDefaultAntenna(oRemoteDB, oAntennaPortData);
                            else
                                oQuery.InsertDefaultAntenna(oRemoteDB, oAntennaPortData);
                        }
                    }
                    
                    oTransactions.Commit();

                    ResultData.Add("RecordID", PRI.ToString());
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Insert " + Reader.DeviceID + " Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }

            return Json(ResultData);
        }
        public ActionResult SubmitAllDefault(ImpinjDefault_REC Reader, List<ImpinjAntennaDefault_REC> AntennaPortData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            using (var oTransactions = oRemoteDB.Database.BeginTransaction())
            {
                try
                {
                    ImpinjModel oQuery = new ImpinjModel();
                    ImpinjDefault_REC voTable = oQuery.GetDefault(oRemoteDB).SingleOrDefault();
                    if (voTable != null)
                    {
                        IList<Impinj_REC> voList = oQuery.GetList(oRemoteDB).ToList();
                        foreach(Impinj_REC voData in voList)
                        {
                            voData.RecordTimestamp = DateTime.Now;
                            voData.ModelName = voTable.ModelName;
                            voData.OperatingRegion = voTable.OperatingRegion;
                            voData.AntennaHubEnabled = voTable.AntennaHubEnabled;
                            voData.ReaderMode = voTable.ReaderMode;
                            voData.SearchMode = voTable.SearchMode;
                            voData.Session = voTable.Session;

                            oQuery.Update(oRemoteDB, voData);

                            IList<ImpinjAntenna_REC> oDataAntenna = oQuery.GetAntennaByParentID(oRemoteDB, voData.RecordID).ToList();
                            foreach (ImpinjAntenna_REC voTemp in oDataAntenna)
                            {
                                ImpinjAntennaDefault_REC oData = AntennaPortData.Where(w => w.AntennaPort == voTemp.AntennaPort && w.AntennaNo == voTemp.AntennaNo).SingleOrDefault();
                                if(oData != null)
                                {
                                    voTemp.RecordTimestamp = DateTime.Now;
                                    voTemp.RxSensitivity = oData.RxSensitivity;
                                    voTemp.TxPower = oData.TxPower;

                                    oQuery.UpdateAntenna(oRemoteDB, voTemp);
                                }
                            }
                        }
                    }

                    oTransactions.Commit();

                    ResultData.Add("RecordID", voTable.RecordID.ToString());
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                    ResultData.Add("msg", "Insert " + Reader.DeviceID + " Success");
                }
                catch (Exception ex)
                {
                    oTransactions.Rollback();
                    ResultData.Add("errorcode", "500");
                    ResultData.Add("title", "Failed");
                    ResultData.Add("msg", ex.Message);
                }
            }

            return Json(ResultData);
        }

        public IEnumerable<ImpinjDataLog_REC> GetListDataLogReport([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ImpinjDataLog_REC> Collections = database.GetCollection<ImpinjDataLog_REC>("RFIDDataLog");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<ImpinjDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();
            if (filter_date != null)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) },
                    { "$lt", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            IEnumerable<ImpinjDataLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).ToList();
            return oTable;
        }
        [HttpPost]
        public ActionResult ExportServer([DataSourceRequest]DataSourceRequest poRequest, ParameterExport data)
        {
            var columnsData = data.columns.ToArray();
            SpreadDocumentFormat exportFormat = data.options.format.ToString() == "csv" ? exportFormat = SpreadDocumentFormat.Csv : exportFormat = SpreadDocumentFormat.Xlsx;
            Action<ExportCellStyle> cellStyle = new Action<ExportCellStyle>(ChangeCellStyle);
            Action<ExportRowStyle> rowStyle = new Action<ExportRowStyle>(ChangeRowStyle);
            Action<ExportColumnStyle> columnStyle = new Action<ExportColumnStyle>(ChangeColumnStyle);

            string fileName = string.Format("{0}.{1}", data.options.title, data.options.format);
            string mimeType = Helpers.GetMimeType(exportFormat);

            Stream exportStream = exportFormat == SpreadDocumentFormat.Xlsx ?
                GetListDataLogReport(poRequest, data.transport).ToXlsxStream(columnsData) :
                GetListDataLogReport(poRequest, data.transport).ToCsvStream(columnsData);

            var fileStreamResult = new FileStreamResult(exportStream, mimeType);
            fileStreamResult.FileDownloadName = fileName;
            fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);
            using (var fileStream = System.IO.File.Create(Server.MapPath("~" + Constants.ExportDirectory + fileName)))
            {
                fileStreamResult.FileStream.CopyTo(fileStream);
            }

            return Json(new { filename = fileName });
        }
        public IEnumerable<ImpinjLocationLog_REC> GetListLocationLogReport([DataSourceRequest]DataSourceRequest poRequest, FilterDate filter_date)
        {
            IMongoDatabase database = Constants.MongoClient.GetDatabase("IoT");
            IMongoCollection<ImpinjLocationLog_REC> Collections = database.GetCollection<ImpinjLocationLog_REC>("RFIDLocationLog");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;

            var ds = new List<ImpinjDataLog_REC>().ToDataSourceResult(poRequest);

            var filter_mongo = new BsonDocument();
            if (filter_date != null)
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("RecordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) },
                    { "$lt", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":")) }
                });
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = GlobalFunction.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            IEnumerable<ImpinjLocationLog_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).ToList();
            return oTable;
        }
        [HttpPost]
        public ActionResult ExportServerLocationPlayback([DataSourceRequest]DataSourceRequest poRequest, ParameterExport data)
        {
            var columnsData = data.columns.ToArray();
            SpreadDocumentFormat exportFormat = data.options.format.ToString() == "csv" ? exportFormat = SpreadDocumentFormat.Csv : exportFormat = SpreadDocumentFormat.Xlsx;
            Action<ExportCellStyle> cellStyle = new Action<ExportCellStyle>(ChangeCellStyle);
            Action<ExportRowStyle> rowStyle = new Action<ExportRowStyle>(ChangeRowStyle);
            Action<ExportColumnStyle> columnStyle = new Action<ExportColumnStyle>(ChangeColumnStyle);

            string fileName = string.Format("{0}.{1}", data.options.title, data.options.format);
            string mimeType = Helpers.GetMimeType(exportFormat);

            Stream exportStream = exportFormat == SpreadDocumentFormat.Xlsx ?
                GetListLocationLogReport(poRequest, data.transport).ToXlsxStream(columnsData) :
                GetListLocationLogReport(poRequest, data.transport).ToCsvStream(columnsData);

            var fileStreamResult = new FileStreamResult(exportStream, mimeType);
            fileStreamResult.FileDownloadName = fileName;
            fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);
            using (var fileStream = System.IO.File.Create(Server.MapPath("~" + Constants.ExportDirectory + fileName)))
            {
                fileStreamResult.FileStream.CopyTo(fileStream);
            }

            return Json(new { filename = fileName });
        }
        private void ChangeCellStyle(ExportCellStyle e)
        {
            bool isHeader = e.Row == 0;
            SpreadBorder border = new SpreadBorder(SpreadBorderStyle.Thick, new SpreadThemableColor(new SpreadColor(0, 0, 0)));
            SpreadCellFormat format = new SpreadCellFormat
            {
                //TopBorder = border,
                //BottomBorder = border,
                //LeftBorder = border,
                //RightBorder = border,
                //Fill = SpreadPatternFill.CreateSolidFill(new SpreadColor(255, 255, 255)),
                FontSize = 11,
                ForeColor = new SpreadThemableColor(new SpreadColor(0, 0, 0)),
                WrapText = false
            };
            e.Cell.SetFormat(format);
        }
        private void ChangeRowStyle(ExportRowStyle e)
        {
            e.Row.SetHeightInPixels(e.Index == 0 ? 80 : 30);
        }
        private void ChangeColumnStyle(ExportColumnStyle e)
        {
            double width = e.Name == "Item ID" || e.Name == "Barcode" ? 200 : 100;
            e.Column.SetWidthInPixels(width);
        }
    }
}