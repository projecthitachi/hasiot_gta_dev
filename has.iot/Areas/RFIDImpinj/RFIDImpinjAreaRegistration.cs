﻿using has.iot.Components;
using RFID.Impinj.Components;
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace MVCPluggableDemo
{
    public class RFIDImpinjAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "RFIDImpinj";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "RFIDImpinj_default",
                "RFIDImpinj/{controller}/{action}/{id}",
                new {controller= "Impinj", action = "Index", id = UrlParameter.Optional },
                new string[] { "RFID.Impinj.Controllers" }
            );
            Constants.MqttClient = new MqttClient(GlobalFunction.mqttServer);
            Constants.MqttClient.Subscribe(new string[] { "RFID/Impinj/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            Constants.MqttClient.MqttMsgPublishReceived += RFID.Impinj.Controllers.ImpinjController.client_MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            Constants.MqttClient.Connect(clientId);
        }
    }
}