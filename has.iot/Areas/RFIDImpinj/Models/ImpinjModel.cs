﻿using Kendo.Mvc;
using Kendo.Mvc.Export;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RFID.Impinj.Models
{
    public class ImpinjDefault_REC
    {
        [Key]
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Display(Name = "Device ID")]
        public string DeviceID { get; set; }

        [Display(Name = "Model Name")]
        public string ModelName { get; set; }
        [Display(Name = "Category ID")]
        public string CategoryID { get; set; }
        [Display(Name = "Operating Region")]
        public string OperatingRegion { get; set; }
        [Display(Name = "Antenna Hub Enable?")]
        public int AntennaHubEnabled { get; set; }
        [Display(Name = "Reader Mode")]
        public string ReaderMode { get; set; }
        [Display(Name = "Search Mode")]
        public string SearchMode { get; set; }
        [Display(Name = "Session")]
        public Int32 Session { get; set; }
        [Display(Name = "IP Address")]
        public string IPAddress { get; set; }
    }
    public class ImpinjAntennaDefault_REC
    {
        [Key]
        public long? RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Display(Name = "Reader ID")]
        public long ReaderID { get; set; }
        [Display(Name = "Antenna Port")]
        public int AntennaPort { get; set; }
        [Display(Name = "Antenna No")]
        public int AntennaNo { get; set; }
        [Display(Name = "Tx Power")]
        public Decimal TxPower { get; set; }
        [Display(Name = "Rx Sensitivity")]
        public int RxSensitivity { get; set; }
    }
    public class Impinj_REC
    {
        [Key]
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Display(Name = "Device ID")]
        public string DeviceID { get; set; }

        [Display(Name = "Model Name")]
        public string ModelName { get; set; }
        [Display(Name = "Category ID")]
        public string CategoryID { get; set; }
        [Display(Name = "Operating Region")]
        public string OperatingRegion { get; set; }
        [Display(Name = "Antenna Hub Enable?")]
        public int AntennaHubEnabled { get; set; }
        [Display(Name = "Reader Mode")]
        public string ReaderMode { get; set; }
        [Display(Name = "Search Mode")]
        public string SearchMode { get; set; }
        [Display(Name = "Session")]
        public Int32 Session { get; set; }
        [Display(Name = "IP Address")]
        public string IPAddress { get; set; }

        public string StatusReader { get; set; }
    }
    public class ImpinjAntenna_REC
    {
        [Key]
        public long? RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Display(Name = "Reader ID")]
        public long ReaderID { get; set; }
        [Display(Name = "Antenna Port")]
        public int AntennaPort { get; set; }
        [Display(Name = "Antenna No")]
        public int AntennaNo { get; set; }
        [Display(Name = "Tx Power")]
        public Decimal TxPower { get; set; }
        [Display(Name = "Rx Sensitivity")]
        public int RxSensitivity { get; set; }
        [Display(Name = "Location ID")]
        public string LocationID { get; set; }

        [Display(Name = "Location Name")]
        public string LocationName { get; set; }
    }
    public class ImpinjTagLog_REC
    {
        private long t70305r001;
        [Display(Name = "RecordID")]
        public virtual long RecordID { get { return t70305r001; } set { t70305r001 = value; } }

        private DateTime t70305r002;
        [Display(Name = "RecordTimestamp")]
        public virtual DateTime RecordTimestamp { get { return t70305r002; } set { t70305r002 = value; } }

        private Int32 t70305r003;
        [Display(Name = "Record Status")]
        public virtual Int32 RecordStatus { get { return t70305r003; } set { t70305r003 = value; } }

        private string t70305f001;
        [Display(Name = "IPAddress")]
        public virtual string IPAddress { get { return t70305f001; } set { t70305f001 = value; } }

        private string t70305f002;
        [Display(Name = "ReaderIdentity")]
        public virtual string ReaderIdentity { get { return t70305f002; } set { t70305f002 = value; } }

        private string t70305f003;
        [Display(Name = "Epc")]
        public virtual string Epc { get { return t70305f003; } set { t70305f003 = value; } }

        private Int32 t70305f004;
        [Display(Name = "AntennaPort")]
        public virtual Int32 AntennaPortNumber { get { return t70305f004; } set { t70305f004 = value; } }

        private Decimal t70305f005;
        [Display(Name = "ChannelInMhz")]
        public virtual Decimal ChannelInMhz { get { return t70305f005; } set { t70305f005 = value; } }

        private DateTime t70305f006;
        [Display(Name = "FirstSeenTime")]
        public virtual DateTime FirstSeenTime { get { return t70305f006; } set { t70305f006 = value; } }

        private DateTime t70305f007;
        [Display(Name = "LastSeenTime")]
        public virtual DateTime LastSeenTime { get { return t70305f007; } set { t70305f007 = value; } }

        private Decimal t70305f008;
        [Display(Name = "PeakRssiInDbm")]
        public virtual Decimal PeakRssiInDbm { get { return t70305f008; } set { t70305f008 = value; } }

        private Int32 t70305f009;
        [Display(Name = "TagSeenCount")]
        public virtual Int32 TagSeenCount { get { return t70305f009; } set { t70305f009 = value; } }

        private string t70305f010;
        [Display(Name = "Tid")]
        public virtual string Tid { get { return t70305f010; } set { t70305f010 = value; } }

        private Decimal t70305f011;
        [Display(Name = "RfDopplerFrequency")]
        public virtual Decimal RfDopplerFrequency { get { return t70305f011; } set { t70305f011 = value; } }

        private Decimal t70305f012;
        [Display(Name = "PhaseAngleInRadians")]
        public virtual Decimal PhaseAngleInRadians { get { return t70305f012; } set { t70305f012 = value; } }

        private Int32 t70305f013;
        [Display(Name = "Crc")]
        public virtual Int32 Crc { get { return t70305f013; } set { t70305f013 = value; } }

        private Int32 t70305f014;
        [Display(Name = "PcBits")]
        public virtual Int32 PcBits { get { return t70305f014; } set { t70305f014 = value; } }

        private string t70305f015;
        [Display(Name = "SerializedTid")]
        public virtual string SerializedTid { get { return t70305f015; } set { t70305f015 = value; } }

        private string t70305f016;
        [Display(Name = "Latitude")]
        public virtual string Latitude { get { return t70305f016; } set { t70305f016 = value; } }

        private string t70305f017;
        [Display(Name = "Longitude")]
        public virtual string Longitude { get { return t70305f017; } set { t70305f017 = value; } }
        
        private Int32 t70305f018;
        [Display(Name = "IsAntennaPortNumberPresent")]
        public virtual Int32 IsAntennaPortNumberPresent { get { return t70305f018; } set { t70305f018 = value; } }

        private Int32 t70305f019;
        [Display(Name = "IsChannelInMhzPresent")]
        public virtual Int32 IsChannelInMhzPresent { get { return t70305f019; } set { t70305f019 = value; } }

        private Int32 t70305f020;
        [Display(Name = "IsFirstSeenTimePresent")]
        public virtual Int32 IsFirstSeenTimePresent { get { return t70305f020; } set { t70305f020 = value; } }

        private Int32 t70305f021;
        [Display(Name = "IsLastSeenTimePresent")]
        public virtual Int32 IsLastSeenTimePresent { get { return t70305f021; } set { t70305f021 = value; } }

        private Int32 t70305f022;
        [Display(Name = "IsPeakRssiInDbmPresent")]
        public virtual Int32 IsPeakRssiInDbmPresent { get { return t70305f022; } set { t70305f022 = value; } }

        private Int32 t70305f023;
        [Display(Name = "IsFastIdPresent")]
        public virtual Int32 IsFastIdPresent { get { return t70305f023; } set { t70305f023 = value; } }

        private Int32 t70305f024;
        [Display(Name = "IsRfPhaseAnglePresent")]
        public virtual Int32 IsRfPhaseAnglePresent { get { return t70305f024; } set { t70305f024 = value; } }

        private Int32 t70305f025;
        [Display(Name = "IsSeenCountPresent")]
        public virtual Int32 IsSeenCountPresent { get { return t70305f025; } set { t70305f025 = value; } }

        private Int32 t70305f026;
        [Display(Name = "IsCrcPresent")]
        public virtual Int32 IsCrcPresent { get { return t70305f026; } set { t70305f026 = value; } }

        private Int32 t70305f027;
        [Display(Name = "IsPcBitsPresent")]
        public virtual Int32 IsPcBitsPresent { get { return t70305f027; } set { t70305f027 = value; } }

        private Int32 t70305f028;
        [Display(Name = "IsRfDopplerFrequencyPresent")]
        public virtual Int32 IsRfDopplerFrequencyPresent { get { return t70305f028; } set { t70305f028 = value; } }

        private Int32 t70305f029;
        [Display(Name = "IsGpsCoordinatesPresent")]
        public virtual Int32 IsGpsCoordinatesPresent { get { return t70305f029; } set { t70305f029 = value; } }

        private Int32 t70305f030;
        [Display(Name = "IsPhaseAngleInRadiansPresent")]
        public virtual Int32 IsPhaseAngleInRadiansPresent { get { return t70305f030; } set { t70305f030 = value; } }

        private Int32 t70305f031;
        [Display(Name = "IsSerializedTidPresent")]
        public virtual Int32 IsSerializedTidPresent { get { return t70305f031; } set { t70305f031 = value; } }
    }
    public class ImpinjDataLog_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        private long t70307r001;
        [Display(Name = "RecordID")]
        public virtual long RecordID { get { return t70307r001; } set { t70307r001 = value; } }

        private DateTime t70307r002;
        [Display(Name = "RecordTimestamp")]
        public virtual DateTime RecordTimestamp { get { return t70307r002; } set { t70307r002 = value; } }

        private Int32 t70307r003;
        [Display(Name = "Record Status")]
        public virtual Int32 RecordStatus { get { return t70307r003; } set { t70307r003 = value; } }

        private string t70307f001;
        [Display(Name = "Reader ID")]
        public virtual string ReaderID { get { return t70307f001; } set { t70307f001 = value; } }

        private string t70307f002;
        [Display(Name = "RFID Tag ID")]
        public virtual string RFIDTagID { get { return t70307f002; } set { t70307f002 = value; } }

        private string t70307f003;
        [Display(Name = "Payload")]
        public virtual string Payload { get { return t70307f003; } set { t70307f003 = value; } }

        private string t70307f004;
        [Display(Name = "Value(RSSI)")]
        public virtual string Value { get { return t70307f004; } set { t70307f004 = value; } }

        private string t70307f005;
        [Display(Name = "Antenna")]
        public virtual string Antenna { get { return t70307f005; } set { t70307f005 = value; } }

    }
    public class ImpinjLocationLog_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        private long t70304r001;
        [Display(Name = "RecordID")]
        public virtual long RecordID { get { return t70304r001; } set { t70304r001 = value; } }

        private DateTime t70304r002;
        [BsonDateTimeOptions(Kind=DateTimeKind.Local)]
        public virtual DateTime RecordTimestamp { get { return t70304r002; } set { t70304r002 = value; } }

        private Int32 t70304r003;
        [Display(Name = "Record Status")]
        public virtual Int32 RecordStatus { get { return t70304r003; } set { t70304r003 = value; } }

        private string t70304f001;
        [Display(Name = "EPC")]
        public virtual string EPC { get { return t70304f001; } set { t70304f001 = value; } }

        private string t70304f002;
        [Display(Name = "Person ID")]
        public virtual string PersonID { get { return t70304f002; } set { t70304f002 = value; } }

        private string t70304f003;
        [Display(Name = "Location ID")]
        public virtual string LocationID { get { return t70304f003; } set { t70304f003 = value; } }

        private decimal t70304f004;
        [Display(Name = "RSSI")]
        public virtual decimal RSSI { get { return t70304f004; } set { t70304f004 = value; } }


        public virtual string LocationName { get; set; }
        public virtual string PersonName { get; set; }

        public int Count { get; set; }
    }
    public class ImpinjPresenceLocation_REC
    {
        private long t70311r001;
        [Display(Name = "RecordID")]
        public virtual long RecordID { get { return t70311r001; } set { t70311r001 = value; } }

        private DateTime t70311r002;
        [Display(Name = "RecordTimestamp")]
        public virtual DateTime RecordTimestamp { get { return t70311r002; } set { t70311r002 = value; } }

        private Int32 t70311r003;
        [Display(Name = "Record Status")]
        public virtual Int32 RecordStatus { get { return t70311r003; } set { t70311r003 = value; } }

        private string t70311f001;
        [Display(Name = "EPC")]
        public virtual string EPC { get { return t70311f001; } set { t70311f001 = value; } }

        private string t70311f002;
        [Display(Name = "Person ID")]
        public virtual string PersonID { get { return t70311f002; } set { t70311f002 = value; } }

        private string t70311f003;
        [Display(Name = "Location ID")]
        public virtual string LocationID { get { return t70311f003; } set { t70311f003 = value; } }

        private decimal t70311f004;
        [Display(Name = "RSSI")]
        public virtual decimal RSSI { get { return t70311f004; } set { t70311f004 = value; } }

        public virtual string LocationName { get; set; }
        public virtual string PersonName { get; set; }
    }
    public class CountData
    {
        public int count { get; set; }
    }
    public class FilterDate
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
    public class ParameterExport
    {
        public IList<ExportColumnSettings> columns { get; set; }
        public ParameterExportOptions options { get; set; }
        public FilterDate transport { get; set; }
    }
    public class ParameterExportOptions
    {
        public string format { get; set; }
        public string title { get; set; }
    }
    public class ImpinjModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public ImpinjModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t70105fieldselect"
        private string t70105fieldselect = @"
            t70105r001 as RecordID,
            t70105r002 as RecordTimestamp,
            t70105r003 as RecordStatus,
            t70105f001 as DeviceID,
            t70105f002 as ModelName,
            t70105f003 as CategoryID,
            t70105f004 as OperatingRegion,
            t70105f005 as AntennaHubEnabled,
            t70105f006 as ReaderMode,
            t70105f007 as SearchMode,
            t70105f008 as Session,
            t70105f009 as IPAddress";
        #endregion
        #region "t70106fieldselect"
        private string t70106fieldselect = @"
            t70106r001 as RecordID,
            t70106r002 as RecordTimestamp,
            t70106r003 as RecordStatus,
            t70106f001 as ReaderID,
            t70106f002 as AntennaPort,
            t70106f003 as AntennaNo,
            t70106f004 as TxPower,
            t70106f005 as RxSensitivity";
        #endregion
        #region "t70205fieldselect"
        private string t70205fieldselect = @"
            t70205r001 as RecordID,
            t70205r002 as RecordTimestamp,
            t70205r003 as RecordStatus,
            t70205f001 as DeviceID,
            t70205f002 as ModelName,
            t70205f003 as CategoryID,
            t70205f004 as OperatingRegion,
            t70205f005 as AntennaHubEnabled,
            t70205f006 as ReaderMode,
            t70205f007 as SearchMode,
            t70205f008 as Session,
            t70205f009 as IPAddress,
            t70205f010 as StatusReader";
        #endregion
        #region "t70206fieldselect"
        private string t70206fieldselect = @"
            t70206r001 as RecordID,
            t70206r002 as RecordTimestamp,
            t70206r003 as RecordStatus,
            t70206f001 as ReaderID,
            t70206f002 as AntennaPort,
            t70206f003 as AntennaNo,
            t70206f004 as TxPower,
            t70206f005 as RxSensitivity,
            t70206f006 as LocationID";
        #endregion

        #region "t8990fieldselect"
        private string t8990fieldselect = @"
            t8990r001 as RecordID,
            t8990r002 as RecordTimestamp,
            t8990r003 as RecordStatus,
            t8990f001 as ID,
            t8990f002 as Name,
            t8990f003 as ListType
        ";
        #endregion
        #region "t70304fieldselect"
        private string t70304fieldselect = @"
            t70304r001 as RecordID,
            t70304r002 as RecordTimestamp,
            t70304r003 as RecordStatus,
            t70304f001 as EPC,
            t70304f002 as PersonID,
            t70304f003 as LocationID,
            t70304f004 as RSSI";
        #endregion
        #region "t70307fieldselect"
        private string t70307fieldselect = @"
            t70307r001 as RecordID,
            t70307r002 as RecordTimestamp,
            t70307r003 as RecordStatus,
            t70307f001 as ReaderID,
            t70307f002 as RFIDTagID,
            t70307f003 as Payload,
            t70307f004 as Value,
            t70307f005 as Antenna";
        #endregion
        #region "t70311fieldselect"
        private string t70311fieldselect = @"
            t70311r001 as RecordID,
            t70311r002 as RecordTimestamp,
            t70311r003 as RecordStatus,
            t70311f001 as EPC,
            t70311f002 as PersonID,
            t70311f003 as LocationID,
            t70311f004 as RSSI";
        #endregion
        
        public DbRawSqlQuery<Impinj_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70205fieldselect + " FROM t70205 ORDER BY t70205r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<Impinj_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Impinj_REC> GetList(DatabaseContext oRemoteDB)
        {            
            string sSQL = "SELECT " + t70205fieldselect + " FROM t70205 ORDER BY t70205r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<Impinj_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Impinj_REC> GetOne(DatabaseContext oRemoteDB, long RecordID)
        {
            string sSQL = "SELECT " + t70205fieldselect + " FROM t70205 WHERE t70205r001 = " + RecordID;
            var vQuery = oRemoteDB.Database.SqlQuery<Impinj_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ImpinjAntenna_REC> GetAntennaByParentID(DatabaseContext oRemoteDB, long RecordID)
        {
            string sSQL = "SELECT " + t70206fieldselect + ", t8030f002 as LocationName FROM t70206 LEFT JOIN t8030 on t8030f001 = t70206f006 where t70206f001 = " + RecordID + " ORDER BY t70206f002 ASC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjAntenna_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ImpinjAntenna_REC> GetAntennaByReaderIDAndPort(DatabaseContext oRemoteDB, string ReaderID, int Port)
        {
            string sSQL = "SELECT " + t70206fieldselect + ", t8030f002 AS LocationName FROM t70206 LEFT JOIN t8030 ON t8030f001 = t70206f006 JOIN t70205 ON t70205r001 = t70206f001 AND t70205f001 = '"+ReaderID+"' WHERE t70206f002 = " + Port;
            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjAntenna_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ImpinjDefault_REC> GetDefault(DatabaseContext oRemoteDB)
        {
            string sSQL = "SELECT TOP(1)" + t70105fieldselect + " FROM t70105 ";
            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjDefault_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ImpinjAntennaDefault_REC> GetAntennaDefaultByParentID(DatabaseContext oRemoteDB, long RecordID)
        {
            string sSQL = "SELECT " + t70106fieldselect + " FROM t70106 where t70106f001 = " + RecordID + " ORDER BY t70106f002 ASC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjAntennaDefault_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<has.iot.Models.GeneralTables> GetRegionByModel(DatabaseContext oRemoteDB, string sModel)
        {
            string where_in = "";
            if (sModel == "SPEEDWAYR420")
            {
                where_in = "IN('FCC','AUS','BRA','CAN','CHN','ETSI','HKG','IND','IDN','ISR','JPN5','JPN1','KOR2','USA','MYS','NZL','PHL','SGP','ZAF','TWN','URY','VNM')";
            }
            else if (sModel == "SPEEDWAYR220")
            {
                where_in = "IN('FCC','AUS','BRA','CAN','CHN','ETSI','HKG','IND','IDN','ISR','JPN5','USA','KOR2','MYS','NZL','PHL','SGP','ZAF','TWN','URY','VNM')";
            }
            else if (sModel == "SPEEDWAYR640")
            {
                where_in = "IN('FCC','AUS','BRA','CAN','CHN','ETSI','HKG','IND','IDN','ISR','KOR2','USA','MYS','NZL','PHL','SGP','ZAF','TWN','TWN','URY','VNM')";
            }
            else if (sModel == "SPEEDWAYR1000")
            {
                where_in = "IN('FCC','CHN','ETSI','HKG','JPN4','JPN2','JPN3','KOR1','MYS','ZAF','TWN')";
            }
            else if (sModel == "MICRONETICSRFIDFORKLIFTSYSTEM")
            {
                where_in = "IN ('FCC')";
            }
            else
            {
                where_in = " LIKE '%%' ";
            }

            string sSQL = "SELECT " + t8990fieldselect + " FROM t8990 " +
                "WHERE t8990f001 "+ where_in + " AND t8990f003 = 5";
            var vQuery = oRemoteDB.Database.SqlQuery<has.iot.Models.GeneralTables>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ImpinjDataLog_REC> GetListDataLog(DatabaseContext oRemoteDB, [DataSourceRequest]DataSourceRequest poRequest)
        {
            int start = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int end = poRequest.Page * poRequest.PageSize;

            string WhereClause = "";
            foreach (FilterDescriptor filter in poRequest.Filters)
            {
                WhereClause += filter.Member + " LIKE '%" + filter.Value + "%' AND ";
            }

            string Offset = "";
            string Limit = "";
            if (WhereClause.Length == 0)
            {
                Offset = "([table].RowNum BETWEEN " + start + " AND " + end + ")";
                Limit = "TOP(" + poRequest.PageSize + ")";
            }
            else
                WhereClause = WhereClause.Remove(WhereClause.LastIndexOf("AND"), 3);

            string sSQL = "SELECT " + Limit + "* " +
                "FROM(SELECT " + t70307fieldselect + ", ROW_NUMBER() OVER(ORDER BY t70307r001 DESC) AS RowNum " +
                "FROM t70307) AS [table] " +
                "WHERE " + WhereClause + Offset;

            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjDataLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<CountData> GetCountData(DatabaseContext oRemoteDB, string table)
        {
            string sSQL = "SELECT COUNT(*) as count from " + table;
            var vQuery = oRemoteDB.Database.SqlQuery<CountData>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ImpinjDataLog_REC> GetListDataLog(DatabaseContext oRemoteDB)
        {
            string sSQL = "SELECT " + t70307fieldselect + " FROM t70307 ORDER BY t70307r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjDataLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ImpinjLocationLog_REC> GetListDataLocLog(DatabaseContext oRemoteDB, [DataSourceRequest]DataSourceRequest poRequest)
        {
            int start = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int end = poRequest.Page * poRequest.PageSize;
            string WhereClause = "";
            foreach (FilterDescriptor filter in poRequest.Filters)
            {
                WhereClause += filter.Member + " LIKE '%" + filter.Value + "%' AND ";
            }

            string Offset = "";
            string Limit = "";
            if (WhereClause.Length == 0)
            {
                Offset = "([table].RowNum BETWEEN " + start + " AND " + end + ")";
                Limit = "TOP(" + poRequest.PageSize + ")";
            }
            else
                WhereClause = WhereClause.Remove(WhereClause.LastIndexOf("AND"), 3);

            string sSQL = "SELECT " + Limit + "* " +
                "FROM(SELECT " + t70304fieldselect + ",t8030f002 as LocationName, t8040f002 as PersonName, ROW_NUMBER() OVER(ORDER BY t70304r001 DESC) AS RowNum " +
                "FROM t70304 LEFT JOIN t8030 on t8030f001 = t70304f003 LEFT JOIN t8040 on t8040f001 = t70304f002) AS [table] " +
                "WHERE " + WhereClause + Offset;

            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjLocationLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ImpinjLocationLog_REC> DataLocLogGetByEPC(DatabaseContext oRemoteDB, string sEPC)
        {
            string oneminute = DateTime.Now.AddMinutes(-1).ToString("yyyy-MM-dd HH:mm:ss").Replace(".",":");
            string sSQL = "SELECT TOP(1)" +t70304fieldselect+ " from t70304 where t70304f001 = '" + sEPC + "' and t70304r002 >= '"+ oneminute + "' order by t70304r001 desc";
            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjLocationLog_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<ImpinjLocationLog_REC> PresenceLocationPlayback(DatabaseContext oRemoteDB, FilterDate poData)
        {
            string starttime = poData.StartTime.ToString("yyyy-MM-dd HH:mm").Replace(".",":");
            string endtime = poData.EndTime.ToString("yyyy-MM-dd HH:mm").Replace(".", ":");
            string sSQL = "SELECT " + t70304fieldselect + ",t8030f002 as LocationName, t8040f002 as PersonName from t70304 LEFT JOIN t8030 on t8030f001 = t70304f003 LEFT JOIN t8040 on t8040f001 = t70304f002 where t70304r002 >= '" + starttime + "' AND t70304r002 <= '"+ endtime +"' order by t70304r001 asc";
            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjLocationLog_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ImpinjPresenceLocation_REC> GetListPresenceLocation()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70311fieldselect + ", t8030f002 as LocationName, t8040f002 as PersonName FROM t70311 LEFT JOIN t8030 on t8030f001 = t70311f003 LEFT JOIN t8040 on t8040f001 = t70311f002 ORDER BY t70304r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjPresenceLocation_REC>(sSQL);
            return vQuery;
        }

        public DbRawSqlQuery<ImpinjPresenceLocation_REC> GetPresenceLocationByEPC(DatabaseContext oRemoteDB, string sEPC)
        {
            string sSQL = "SELECT " + t70311fieldselect + ", t8030f002 as LocationName, t8040f002 as PersonName FROM t70311 LEFT JOIN t8030 on t8030f001 = t70311f003 LEFT JOIN t8040 on t8040f001 = t70311f002 WHERE t70311f001 = '" + sEPC + "' ORDER BY t70304r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<ImpinjPresenceLocation_REC>(sSQL);
            return vQuery;
        }

        public long Insert(DatabaseContext oRemoteDB, Impinj_REC poRecord)
        {
            long bReturn = 0;
            //------------------------------
            poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;

            //GlobalFunction.TrimNull(poRecord);
            //----------
            string sSQL = "" +
                "INSERT INTO t70205 " +
                "      ( " +
                "      t70205r002, t70205r003, " +
                "      t70205f001, t70205f002, t70205f003,t70205f004,t70205f005,t70205f006,t70205f007,t70205f008,t70205f009) " +
                "      VALUES " +
                "      (" +
                "      @pt70205r002, @pt70205r003, " +
                "      @pt70205f001, @pt70205f002, @pt70205f003,@pt70205f004,@pt70205f005,@pt70205f006,@pt70205f007,@pt70205f008,@pt70205f009)" +
                ";" +
                "SELECT @pt70205r001 = SCOPE_IDENTITY(); "
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter { ParameterName = "@pt70205r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
            oParameters.Add(new SqlParameter("@pt70205r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70205r003", poRecord.RecordStatus));

            oParameters.Add(new SqlParameter("@pt70205f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70205f002", poRecord.ModelName));
            oParameters.Add(new SqlParameter("@pt70205f003", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70205f004", poRecord.OperatingRegion));
            oParameters.Add(new SqlParameter("@pt70205f005", poRecord.AntennaHubEnabled));
            oParameters.Add(new SqlParameter("@pt70205f006", poRecord.ReaderMode));
            oParameters.Add(new SqlParameter("@pt70205f007", poRecord.SearchMode));
            oParameters.Add(new SqlParameter("@pt70205f008", poRecord.Session));
            oParameters.Add(new SqlParameter("@pt70205f009", poRecord.IPAddress));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

            if (nReturn == 1)
            {
                bReturn = Convert.ToInt64(vSqlParameter[0].Value);
            }
            else
            {
                ErrorMessage = "Failed to insert record!";
                bReturn = 0;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, Impinj_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            //poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            //GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70205 " +
                "   SET " +
                "      t70205f002 = @pt70205f002, " +
                "      t70205f003 = @pt70205f003, " +
                "      t70205f004 = @pt70205f004, " +
                "      t70205f005 = @pt70205f005, " +
                "      t70205f006 = @pt70205f006, " +
                "      t70205f007 = @pt70205f007, " +
                "      t70205f008 = @pt70205f008, " +
                "      t70205f009 = @pt70205f009 " +
                "   WHERE (t70205r001 = @pt70205r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            
            oParameters.Add(new SqlParameter("@pt70205r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70205f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70205f002", poRecord.ModelName));
            oParameters.Add(new SqlParameter("@pt70205f003", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70205f004", poRecord.OperatingRegion));
            oParameters.Add(new SqlParameter("@pt70205f005", poRecord.AntennaHubEnabled));
            oParameters.Add(new SqlParameter("@pt70205f006", poRecord.ReaderMode));
            oParameters.Add(new SqlParameter("@pt70205f007", poRecord.SearchMode));
            oParameters.Add(new SqlParameter("@pt70205f008", poRecord.Session));
            oParameters.Add(new SqlParameter("@pt70205f009", poRecord.IPAddress));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {
                bReturn = true;
            }
            else
            {
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, Impinj_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordID = 0;
            //poRecord.RecordTimestamp = DateTime.Now;
            //poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            string sSQL = "" +
                "DELETE t70205 WHERE (t70205r001 = @pt70205r001); " +
                "DELETE t70206 WHERE (t70206f001 = @pt70205r001);"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70205r001", poRecord.RecordID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn != 0)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }

        public long InsertAntenna(DatabaseContext oRemoteDB, ImpinjAntenna_REC poRecord)
        {
            long bReturn = 0;
            //------------------------------
            poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;

            //GlobalFunction.TrimNull(poRecord);
            //----------
            string sSQL = "" +
                "INSERT INTO t70206 " +
                "      ( " +
                "      t70206r002, t70206r003, " +
                "      t70206f001, t70206f002, t70206f003,t70206f004,t70206f005, t70206f006) " +
                "      VALUES " +
                "      (" +
                "      @pt70206r002, @pt70206r003, " +
                "      @pt70206f001, @pt70206f002, @pt70206f003,@pt70206f004,@pt70206f005,@pt70206f006)" +
                ";" +
                "SELECT @pt70206r001 = SCOPE_IDENTITY(); "
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter { ParameterName = "@pt70206r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
            oParameters.Add(new SqlParameter("@pt70206r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70206r003", poRecord.RecordStatus));

            oParameters.Add(new SqlParameter("@pt70206f001", poRecord.ReaderID));
            oParameters.Add(new SqlParameter("@pt70206f002", poRecord.AntennaPort));
            oParameters.Add(new SqlParameter("@pt70206f003", poRecord.AntennaNo));
            oParameters.Add(new SqlParameter("@pt70206f004", poRecord.TxPower));
            oParameters.Add(new SqlParameter("@pt70206f005", poRecord.RxSensitivity));
            oParameters.Add(new SqlParameter("@pt70206f006", String.IsNullOrEmpty(poRecord.LocationID) ? "" : poRecord.LocationID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

            if (nReturn == 1)
            {
                bReturn = Convert.ToInt64(vSqlParameter[0].Value);
            }
            else
            {
                ErrorMessage = "Failed to insert record!";
                bReturn = 0;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateAntenna(DatabaseContext oRemoteDB, ImpinjAntenna_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            //poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            //GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70206 " +
                "   SET " +
                "      t70206r002 = @pt70206r002, " +
                "      t70206f002 = @pt70206f002, " +
                "      t70206f003 = @pt70206f003, " +
                "      t70206f004 = @pt70206f004, " +
                "      t70206f005 = @pt70206f005, " +
                "      t70206f006 = @pt70206f006 " +
                "   WHERE (t70206r001 = @pt70206r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));

            oParameters.Add(new SqlParameter("@pt70206r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70206r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70206f001", poRecord.ReaderID));
            oParameters.Add(new SqlParameter("@pt70206f002", poRecord.AntennaPort));
            oParameters.Add(new SqlParameter("@pt70206f003", poRecord.AntennaNo));
            oParameters.Add(new SqlParameter("@pt70206f004", poRecord.TxPower));
            oParameters.Add(new SqlParameter("@pt70206f005", poRecord.RxSensitivity));
            oParameters.Add(new SqlParameter("@pt70206f006", String.IsNullOrEmpty(poRecord.LocationID) ? "" : poRecord.LocationID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {
                    
            }
            else
            {
                    
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool DeleteAntenna(DatabaseContext oRemoteDB, long RecordID)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordID = 0;
            //poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            string sSQL = "DELETE t70206 WHERE (t70206r001 = "+ RecordID + ");";
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL);
            if (nReturn == 1)
            {
                bReturn = true;
            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another user.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }

        public bool ImpinjTagLogInsert(DatabaseContext oRemoteDB, ImpinjTagLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70305 " +
                    "      ( " +
                    "      t70305r002, t70305r003, " +
                    "      t70305f001, t70305f002, t70305f003, t70305f004, t70305f005, t70305f006, t70305f007, t70305f008, t70305f009, t70305f010, " +
                    "      t70305f011, t70305f012, t70305f013, t70305f014, t70305f015, t70305f016, t70305f017, t70305f018, t70305f019, t70305f020, " +
                    "      t70305f021, t70305f022, t70305f023, t70305f024, t70305f025, t70305f026, t70305f027, t70305f028, t70305f029, t70305f030, " +
                    "      t70305f031" +
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70305r002, @pt70305r003, " +
                    "      @pt70305f001, @pt70305f002, @pt70305f003, @pt70305f004, @pt70305f005, @pt70305f006, @pt70305f007, @pt70305f008, @pt70305f009, @pt70305f010, " +
                    "      @pt70305f011, @pt70305f012, @pt70305f013, @pt70305f014, @pt70305f015, @pt70305f016, @pt70305f017, @pt70305f018, @pt70305f019, @pt70305f020, " +
                    "      @pt70305f021, @pt70305f022, @pt70305f023, @pt70305f024, @pt70305f025, @pt70305f026, @pt70305f027, @pt70305f028, @pt70305f029, @pt70305f030, " +
                    "      @pt70305f031" +
                    "      )" +
                    ";" +
                    "SELECT @pt70305r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70305r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70305r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70305r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70305f001", poRecord.IPAddress));
                oParameters.Add(new SqlParameter("@pt70305f002", poRecord.ReaderIdentity));
                oParameters.Add(new SqlParameter("@pt70305f003", poRecord.Epc));
                oParameters.Add(new SqlParameter("@pt70305f004", poRecord.AntennaPortNumber));
                oParameters.Add(new SqlParameter("@pt70305f005", poRecord.ChannelInMhz));
                oParameters.Add(new SqlParameter("@pt70305f006", poRecord.FirstSeenTime));
                oParameters.Add(new SqlParameter("@pt70305f007", poRecord.LastSeenTime));
                oParameters.Add(new SqlParameter("@pt70305f008", poRecord.PeakRssiInDbm));
                oParameters.Add(new SqlParameter("@pt70305f009", poRecord.TagSeenCount));
                oParameters.Add(new SqlParameter("@pt70305f010", poRecord.Tid));
                oParameters.Add(new SqlParameter("@pt70305f011", poRecord.RfDopplerFrequency));
                oParameters.Add(new SqlParameter("@pt70305f012", poRecord.PhaseAngleInRadians));
                oParameters.Add(new SqlParameter("@pt70305f013", poRecord.Crc));
                oParameters.Add(new SqlParameter("@pt70305f014", poRecord.PcBits));
                oParameters.Add(new SqlParameter("@pt70305f015", poRecord.SerializedTid));
                oParameters.Add(new SqlParameter("@pt70305f016", poRecord.Latitude));
                oParameters.Add(new SqlParameter("@pt70305f017", poRecord.Longitude));
                oParameters.Add(new SqlParameter("@pt70305f018", poRecord.IsAntennaPortNumberPresent));
                oParameters.Add(new SqlParameter("@pt70305f019", poRecord.IsChannelInMhzPresent));
                oParameters.Add(new SqlParameter("@pt70305f020", poRecord.IsFirstSeenTimePresent));
                oParameters.Add(new SqlParameter("@pt70305f021", poRecord.IsLastSeenTimePresent));
                oParameters.Add(new SqlParameter("@pt70305f022", poRecord.IsPeakRssiInDbmPresent));
                oParameters.Add(new SqlParameter("@pt70305f023", poRecord.IsFastIdPresent));
                oParameters.Add(new SqlParameter("@pt70305f024", poRecord.IsRfPhaseAnglePresent));
                oParameters.Add(new SqlParameter("@pt70305f025", poRecord.IsSeenCountPresent));
                oParameters.Add(new SqlParameter("@pt70305f026", poRecord.IsCrcPresent));
                oParameters.Add(new SqlParameter("@pt70305f027", poRecord.IsPcBitsPresent));
                oParameters.Add(new SqlParameter("@pt70305f028", poRecord.IsRfDopplerFrequencyPresent));
                oParameters.Add(new SqlParameter("@pt70305f029", poRecord.IsGpsCoordinatesPresent));
                oParameters.Add(new SqlParameter("@pt70305f030", poRecord.IsPhaseAngleInRadiansPresent));
                oParameters.Add(new SqlParameter("@pt70305f031", poRecord.IsSerializedTidPresent));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool ImpinjDataLogInsert(DatabaseContext oRemoteDB, ImpinjDataLog_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70307 " +
                    "      ( " +
                    "      t70307r002, t70307r003, " +
                    "      t70307f001, t70307f002, t70307f003, t70307f004, t70307f005" +
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70307r002, @pt70307r003, " +
                    "      @pt70307f001, @pt70307f002, @pt70307f003, @pt70307f004, @pt70307f005" +
                    "      )" +
                    ";" +
                    "SELECT @pt70307r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70307r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70307r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70307r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70307f001", poRecord.ReaderID));
                oParameters.Add(new SqlParameter("@pt70307f002", poRecord.RFIDTagID));
                oParameters.Add(new SqlParameter("@pt70307f003", poRecord.Payload));
                oParameters.Add(new SqlParameter("@pt70307f004", poRecord.Value));
                oParameters.Add(new SqlParameter("@pt70307f005", poRecord.Antenna));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public long InsertDefault(DatabaseContext oRemoteDB, ImpinjDefault_REC poRecord)
        {
            long bReturn = 0;
            //------------------------------
            poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;

            //GlobalFunction.TrimNull(poRecord);
            //----------
            string sSQL = "" +
                "INSERT INTO t70105 " +
                "      ( " +
                "      t70105r002, t70105r003, " +
                "      t70105f001, t70105f002, t70105f003,t70105f004,t70105f005,t70105f006,t70105f007,t70105f008,t70105f009) " +
                "      VALUES " +
                "      (" +
                "      @pt70105r002, @pt70105r003, " +
                "      @pt70105f001, @pt70105f002, @pt70105f003,@pt70105f004,@pt70105f005,@pt70105f006,@pt70105f007,@pt70105f008,@pt70105f009)" +
                ";" +
                "SELECT @pt70105r001 = SCOPE_IDENTITY(); "
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter { ParameterName = "@pt70105r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
            oParameters.Add(new SqlParameter("@pt70105r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70105r003", poRecord.RecordStatus));

            oParameters.Add(new SqlParameter("@pt70105f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70105f002", poRecord.ModelName));
            oParameters.Add(new SqlParameter("@pt70105f003", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70105f004", poRecord.OperatingRegion));
            oParameters.Add(new SqlParameter("@pt70105f005", poRecord.AntennaHubEnabled));
            oParameters.Add(new SqlParameter("@pt70105f006", poRecord.ReaderMode));
            oParameters.Add(new SqlParameter("@pt70105f007", poRecord.SearchMode));
            oParameters.Add(new SqlParameter("@pt70105f008", poRecord.Session));
            oParameters.Add(new SqlParameter("@pt70105f009", poRecord.IPAddress));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

            if (nReturn == 1)
            {
                bReturn = Convert.ToInt64(vSqlParameter[0].Value);
            }
            else
            {
                ErrorMessage = "Failed to insert record!";
                bReturn = 0;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateDefault(DatabaseContext oRemoteDB, ImpinjDefault_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            //poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            //GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70105 " +
                "   SET " +
                "      t70105f002 = @pt70105f002, " +
                "      t70105f003 = @pt70105f003, " +
                "      t70105f004 = @pt70105f004, " +
                "      t70105f005 = @pt70105f005, " +
                "      t70105f006 = @pt70105f006, " +
                "      t70105f007 = @pt70105f007, " +
                "      t70105f008 = @pt70105f008, " +
                "      t70105f009 = @pt70105f009 " +
                "   WHERE (t70105r001 = @pt70105r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));

            oParameters.Add(new SqlParameter("@pt70105r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70105f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70105f002", poRecord.ModelName));
            oParameters.Add(new SqlParameter("@pt70105f003", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70105f004", poRecord.OperatingRegion));
            oParameters.Add(new SqlParameter("@pt70105f005", poRecord.AntennaHubEnabled));
            oParameters.Add(new SqlParameter("@pt70105f006", poRecord.ReaderMode));
            oParameters.Add(new SqlParameter("@pt70105f007", poRecord.SearchMode));
            oParameters.Add(new SqlParameter("@pt70105f008", poRecord.Session));
            oParameters.Add(new SqlParameter("@pt70105f009", poRecord.IPAddress));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {
                bReturn = true;
            }
            else
            {
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        
        public long ImpinjLocationLogInsert(DatabaseContext oRemoteDB, ImpinjLocationLog_REC poRecord)
        {
            long bReturn = 0;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70304 " +
                    "      ( " +
                    "      t70304r002, t70304r003, " +
                    "      t70304f001, t70304f002, t70304f003, t70304f004 " +
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70304r002, @pt70304r003, " +
                    "      @pt70304f001, @pt70304f002, @pt70304f003, @pt70304f004 " +
                    "      )" +
                    ";" +
                    "SELECT @pt70304r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70304r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70304r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70304r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70304f001", poRecord.EPC));
                oParameters.Add(new SqlParameter("@pt70304f002", poRecord.PersonID));
                oParameters.Add(new SqlParameter("@pt70304f003", poRecord.LocationID));
                oParameters.Add(new SqlParameter("@pt70304f004", poRecord.RSSI));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    bReturn = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
            //------------------------------
            return bReturn;
        }
        public bool ImpinjPresenceLocationInsert(DatabaseContext oRemoteDB, ImpinjPresenceLocation_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                //oRemoteDB.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t70311 " +
                    "      ( " +
                    "      t70311r002, t70311r003, " +
                    "      t70311f001, t70311f002, t70311f003, t70311f004 " +
                    ") " +
                    "      VALUES " +
                    "      (" +
                    "      @pt70311r002, @pt70311r003, " +
                    "      @pt70311f001, @pt70311f002, @pt70311f003, @pt70311f004 " +
                    "      )" +
                    ";" +
                    "SELECT @pt70311r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt70311r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt70311r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70311r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt70311f001", poRecord.EPC));
                oParameters.Add(new SqlParameter("@pt70311f002", poRecord.PersonID));
                oParameters.Add(new SqlParameter("@pt70311f003", poRecord.LocationID));
                oParameters.Add(new SqlParameter("@pt70311f004", poRecord.RSSI));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool ImpinjPresenceLocationUpdate(DatabaseContext oRemoteDB, ImpinjPresenceLocation_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            //poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            //GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70311 " +
                "   SET " +
                "      t70311r002 = @pt70311r002, " +
                "      t70311r003 = @pt70311r003, " +
                "      t70311f001 = @pt70311f001, " +
                "      t70311f002 = @pt70311f002, " +
                "      t70311f003 = @pt70311f003, " +
                "      t70311f004 = @pt70311f004 " +
                "   WHERE (t70311r001 = @pt70311r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70311r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70311r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70311r003", poRecord.RecordStatus));
            oParameters.Add(new SqlParameter("@pt70311f001", poRecord.EPC));
            oParameters.Add(new SqlParameter("@pt70311f002", poRecord.PersonID));
            oParameters.Add(new SqlParameter("@pt70311f003", poRecord.LocationID));
            oParameters.Add(new SqlParameter("@pt70311f004", poRecord.RSSI));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {
                bReturn = true;
            }
            else
            {
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }

        public long InsertDefaultAntenna(DatabaseContext oRemoteDB, ImpinjAntennaDefault_REC poRecord)
        {
            long bReturn = 0;
            //------------------------------
            poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;

            //GlobalFunction.TrimNull(poRecord);
            //----------
            string sSQL = "" +
                "INSERT INTO t70106 " +
                "      ( " +
                "      t70106r002, t70106r003, " +
                "      t70106f001, t70106f002, t70106f003,t70106f004,t70106f005) " +
                "      VALUES " +
                "      (" +
                "      @pt70106r002, @pt70106r003, " +
                "      @pt70106f001, @pt70106f002, @pt70106f003,@pt70106f004,@pt70106f005)" +
                ";" +
                "SELECT @pt70106r001 = SCOPE_IDENTITY(); "
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter { ParameterName = "@pt70106r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
            oParameters.Add(new SqlParameter("@pt70106r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70106r003", poRecord.RecordStatus));

            oParameters.Add(new SqlParameter("@pt70106f001", poRecord.ReaderID));
            oParameters.Add(new SqlParameter("@pt70106f002", poRecord.AntennaPort));
            oParameters.Add(new SqlParameter("@pt70106f003", poRecord.AntennaNo));
            oParameters.Add(new SqlParameter("@pt70106f004", poRecord.TxPower));
            oParameters.Add(new SqlParameter("@pt70106f005", poRecord.RxSensitivity));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

            if (nReturn == 1)
            {
                bReturn = Convert.ToInt64(vSqlParameter[0].Value);
            }
            else
            {
                ErrorMessage = "Failed to insert record!";
                bReturn = 0;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool UpdateDefaultAntenna(DatabaseContext oRemoteDB, ImpinjAntennaDefault_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            //poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            //GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t70106 " +
                "   SET " +
                "      t70106r002 = @pt70106r002, " +
                "      t70106f002 = @pt70106f002, " +
                "      t70106f003 = @pt70106f003, " +
                "      t70106f004 = @pt70106f004, " +
                "      t70106f005 = @pt70106f005 " +
                "   WHERE (t70106r001 = @pt70106r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));

            oParameters.Add(new SqlParameter("@pt70106r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt70106r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70106f001", poRecord.ReaderID));
            oParameters.Add(new SqlParameter("@pt70106f002", poRecord.AntennaPort));
            oParameters.Add(new SqlParameter("@pt70106f003", poRecord.AntennaNo));
            oParameters.Add(new SqlParameter("@pt70106f004", poRecord.TxPower));
            oParameters.Add(new SqlParameter("@pt70106f005", poRecord.RxSensitivity));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {

                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
    }
}