﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace RFID.Impinj.Components
{
    public class Constants
    {
        public static MqttClient MqttClient = null;
        public static MongoClient MongoClient = new MongoClient(WebConfigurationManager.AppSettings["mongodb_server"].ToString());
        public static string ExportDirectory = "/Components/Export/RFID/";
    }
}